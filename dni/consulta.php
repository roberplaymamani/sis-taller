<?php
	header('Content-Type: text/json');
	require ("autoload.php");

	$dni 		= ( isset($_REQUEST["ndni"]))? $_REQUEST["ndni"] : false;
	$person = new \Dnis\Dnis();
	$search 	= $person->search( $dni );
	echo $search->json();
?>
