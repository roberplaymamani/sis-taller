<?php
	namespace Dnis;
	class Dnis{
		var $cc;
		var $list_error = array();
		function __construct(){
			$this->cc = new \CURL\cURL();
			$this->cc->setReferer("http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano");
		}
		function getCode( $dni ){
			if ($dni!="" || strlen($dni) == 8){
				$suma = 0;
				$hash = array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
				$suma = 5;
				for( $i=2; $i<10; $i++ ){
					$suma += ( $dni[$i-2] * $hash[$i] );
				}
				$entero = (int)($suma/11);
				$digito = 11 - ( $suma - $entero*11);
				if ($digito == 10){
					$digito = 0;
				}
				else if ($digito == 11){
					$digito = 1;
				}
				return $digito;
			}
			return "";
		}

		function search( $dni ){
			if( strlen($dni)!=8 ){
				$response = new \response\obj(array(
					'success' => false,
					'message' => 'DNI tiene 8 digitos.'
				));
				return $response;
			}
			
			$data = array(
				"DNI" 		=> $dni
			);

			$url = "http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano";			
			$code = $this->cc->send( $url, $data );
			if( $this->cc->getHttpStatus() == 200 && $code != ""){
				if($code != ""){
					$response = new \response\obj();
					$response->success = true;
					$response->result->nombre = str_replace("|", " ", $code);
					return $response;
				}
				else{
					$response = new \response\obj(array(
						'success' => false,
						'message' => 'Datos no encontrados.'
					));
					return $response;
				}
			}
			$response = new \response\obj(array(
				'success' => false,
				'message' => 'Coneccion fallida.'
			));
			return $response;
		}
	}


//http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI=

?>