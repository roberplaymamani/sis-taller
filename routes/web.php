<?php

Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});

Route::get('downloadpdf', function () {
    return \view('client.exportPdf');
});
Route::get('downloadpd2', function () {

     //return view('entryinventory.test');
    //$pdf = App::make('dompdf.wrapper');
    //$pdf->loadHTML('<h1>Test</h1>');
    //return 'Hol';
    //$pdf = PDF::loadView('entryinventory.test');
    $pdf = PDF::loadView('client.exportPdf');
    return $pdf->stream();
});



Route::get('downloadpdf1', 'ClientController@Donwloadpdf');


Route::get('/home1','controllerUbigeos@lista_departamentos');

Route::get('/departamentos','controllerUbigeos@lista_departamentos');
Route::get('/provincias','controllerUbigeos@lista_provincia_x_departamentos');
Route::get('/distritos','controllerUbigeos@lista_distritos_x_provincia');

Route::get('/home','HomeController@index', function () {
    return view('home');
})->middleware('auth');

require_once('rutas/comprobante.php');

Route::group(['middleware' => 'auth'], function () {
    // Users
    Route::resource('user', 'UsersController');
    Route::resource('listUser', 'UsersController');
    Route::post('listUser/store', 'UsersController@store');
    Route::get('user/listUser/export', 'UsersController@export');
    Route::get('listUser/delete/{id}', 'UsersController@destroy');
    Route::get('profile_user', 'UsersController@profileuser');
    Route::post('save_profile_user', 'UsersController@save_profile_user');

    // Vehicles
    Route::resource('vehicles', 'VehiclesController');
    Route::resource('listVehicles', 'VehiclesController');
    Route::post('listVehicles/store', 'VehiclesController@store');
    Route::get('listVehicles/delete/{id}', 'VehiclesController@destroy');
    Route::get('vehicles/listVehicles/export', 'VehiclesController@export');
    Route::post('vehicles/listVehicles/models', 'VehiclesController@models');
    Route::get('downloadpdfVehicles', 'VehiclesController@Donwloadpdf');

    // Clients
    Route::resource('client', 'ClientController');
    Route::resource('listClient', 'ClientController');
    Route::get('listProviders', 'ClientController@redireccionaraProveedores');
    Route::post('listClient/store', 'ClientController@store');
    Route::get('listClient/delete/{id}', 'ClientController@destroy');
    Route::get('client/listClient/export', 'ClientController@export');
    Route::get('client/listClient/export_providers', 'ClientController@export_providers');
    Route::post('client/listClient/search', 'ClientController@search');
    Route::get('client_search_vehicle', 'ClientController@filterClientsForVehicles');
    Route::get('downloadpdfCliente/{typeid}', 'ClientController@Donwloadpdf');
    // System
    Route::resource('system', 'SystemController');
    Route::resource('listSystem', 'SystemController');
    Route::post('listSystem/store', 'SystemController@store');
    Route::get('listSystem/delete/{id}', 'SystemController@destroy');
    Route::get('system/listSystem/export', 'SystemController@export');

    Route::get('list_tables_system', 'SystemController@list_tables');

    // Coins
    Route::resource('coins', 'CoinsController');
    Route::resource('listCoins', 'CoinsController');
    Route::post('listCoins/store', 'CoinsController@store');
    Route::get('listCoins/delete/{id}', 'CoinsController@destroy');
    Route::get('coins/listCoins/export', 'CoinsController@export');

    // Advisors
    Route::resource('advisors', 'AdvisorsController');
    Route::resource('listAdvisors', 'AdvisorsController');
    Route::post('listAdvisors/store', 'AdvisorsController@store');
    Route::get('listAdvisors/delete/{id}', 'AdvisorsController@destroy');
    Route::get('advisors/listAdvisors/export', 'AdvisorsController@export');

    // Items
    Route::resource('items', 'ItemsController');
    Route::resource('listItems', 'ItemsController');
    Route::post('listItems/store', 'ItemsController@store');
    Route::get('listItems/delete/{id}', 'ItemsController@destroy');
    Route::get('items/listItems/export', 'ItemsController@export');

    // Mechanics
    Route::resource('mechanics', 'MechanicsController');
    Route::resource('listMechanics', 'MechanicsController');
    Route::post('listMechanics/store', 'MechanicsController@store');
    Route::get('listMechanics/delete/{id}', 'MechanicsController@destroy');
    Route::get('mechanics/listMechanics/export', 'MechanicsController@export');


    // Movements Cash
    Route::resource('MovementsCash', 'MovementsCashController');
    Route::resource('listMovementsCash', 'MovementsCashController');
    Route::post('listMovementsCash/store', 'MovementsCashController@store');
    Route::get('listMovementsCash/delete/{id}', 'MovementsCashController@destroy');
    Route::get('MovementsCash/listMovementsCash/export', 'MovementsCashController@export');

    // SubProducts
    Route::resource('SubProducts', 'SubProductsController');
    Route::resource('listSubProducts', 'SubProductsController');
    Route::post('listSubProducts/store', 'SubProductsController@store');
    Route::get('listSubProducts/delete/{id}', 'SubProductsController@destroy');
    Route::get('SubProducts/listSubProducts/export', 'SubProductsController@export');

    // models
    Route::resource('models', 'ModelsController');
    Route::resource('listModels', 'ModelsController');
    Route::post('listModels/store', 'ModelsController@store');
    Route::get('listModels/delete/{id}', 'ModelsController@destroy');
    Route::get('models/listModels/export', 'ModelsController@export');

    // products
    Route::resource('products', 'ProductsController');
    Route::resource('listProducts', 'ProductsController');
    Route::post('listProducts/store', 'ProductsController@store');
    Route::get('listProducts/delete/{id}', 'ProductsController@destroy');
    Route::post('products/listProducts/coins', 'ProductsController@coins');
    Route::get('products/listProducts/export', 'ProductsController@export');
    Route::post('products/listProducts/subline', 'ProductsController@subline');
    Route::get('downloadpdfproducts', 'ProductsController@Donwloadpdf');
    // Inventory
    Route::resource('entryinventory', 'EntryInventoryController');
    Route::resource('listEntryInventory', 'EntryInventoryController');
    Route::post('listEntryInventory/store', 'EntryInventoryController@store');
    Route::get('listEntryInventory/delete/{id}', 'EntryInventoryController@destroy');
    Route::post('/entryinventory/listEntryInventory/sum', 'EntryInventoryController@sum');
    Route::get('entryinventory/listEntryInventory/export', 'EntryInventoryController@export');
    Route::post('entryinventory/listEntryInventory/search', 'EntryInventoryController@search');
    Route::post('entryinventory/listEntryInventory/newitems', 'EntryInventoryController@newitems');
    Route::post('entryinventory/listEntryInventory/products', 'EntryInventoryController@products');

    Route::get('download_pdf_reporte/{search}/{date}', 'EntryInventoryController@Donwloadpdf');

    //company - parametros

    Route::resource('company_parametros', 'controllerCompany_parametros');
    Route::post('company_parametros_store', 'controllerCompany_parametros@store');
    //caja

    Route::get('cierre_caja', 'controllerCaja@cierreCaja');

});