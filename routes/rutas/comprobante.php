<?php 

//Route::middleware(['auth', 'admin'])->group(function () {
Route::middleware(['auth'])->group(function () {
	Route::post('/sfactura', 'Control\CComprobante@sfactura');
	Route::post('/sboleta', 'Control\CComprobante@sboleta');
	Route::post('/sncredito', 'Control\CComprobante@sncredito');
	Route::post('/sndevito', 'Control\CComprobante@sndevito');

	Route::get('/sfactura', 'Control\CComprobante@vfactura');
	Route::get('/sboleta', 'Control\CComprobante@vboleta');
	Route::get('/sncredito', 'Control\CComprobante@vncredito');
	Route::get('/sndevito', 'Control\CComprobante@vndevito');

	Route::get('/lcomprobante', 'Control\CComprobante@lcomprobante');
	Route::post('/consulta','Control\CComprobante@consulta');
	Route::get('/kodo/{ruc}','Control\CComprobante@padron');
	Route::get('/bsproducto/{cop}','Control\CComprobante@bsproducto');
});