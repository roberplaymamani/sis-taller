@php($item=1)
@foreach($listVehiculos as $valueVehiculos)
<tr>
  <td><span class="badge badge-primary">{{$item++}}</span></td>
  <td>{{$valueVehiculos->plate}}</td>
  <td>{{$valueVehiculos->category}}</td>
  <td>{{$valueVehiculos->mark}}</td>
  <td>{{$valueVehiculos->model}}</td>
  <td>{{$valueVehiculos->year}}</td>
  <td>{{$valueVehiculos->name}}</td>
  <td><span class="label label-success">{{$valueVehiculos->description}}</span></td>
  <td>
    <center>
        <a href="#" rel="modal:open" data-id="{{$valueVehiculos->id}}/{{$valueVehiculos->plate}}"  title="Editar" class="edit edit-vehicle">
          <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
        </a>
        &nbsp;
        <a  href="#" id="delete-vehicle" title="Eliminar" data-id="{{$valueVehiculos->id}}/{{$valueVehiculos->plate}}" class="delete">
          <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
        </a>
    </center>
  </td>
</tr>
@endforeach 