<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div id="content_sistema">
           <div class="container">
            <div class="row">
              <div class="col-md-12">
                <h3 class="page-title">Vehículos</h3>
                <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                  <li>
                    <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#ignore">Dashboard</a>
                  </li>
                  <li>
                    <a href="#ignore">Mantenimiento de vehículos</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <div class="row">
                  <button type="button" class="btn btn-primary add add-vehicle" id="btnAdd" style="margin-left: 5px;" onclick="AddVehiculoDIV(this.value)" value="divListaVehiculo">
                    <span class="fa fa-pencil"></span>&nbsp;Agregar Vehiculo
                  </button>
                  <a href="{{url('vehicles/listVehicles/export')}}" class="btn btn-success" style="margin-left: 5px;">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar Excel
                  </a>
                  <button type="button" class="btn btn-danger" style="margin-left: 5px;" onclick="window.open('/downloadpdfVehicles')">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar PDF
                  </button>
                  <input type="text" name="searchListVehiculo" id="searchListVehiculo" class="form-control pull-right" placeholder="Buscar" style="width: 200px; margin-right: 10px;">
                  </div>
                </div>
                <div class="panel-body" id="divListaVehiculo" >
                  <div class="table-responsive">
                     <table id="listSystem12" class="table table-striped table-bordered table-hover table-responsive">
                        <thead>
                          <tr class="bg-primary">
                            <th>No</th>
                            <th>Placa</th>
                            <th>Categoria</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Propietario</th>
                            <th>Estado</th>
                            <th><center>Opciones</center></th>
                          </tr>
                        </thead>
                        <tbody id="listPaginatesvehi">
                          
                          @php($item=1)
                          @foreach($listVehiculos as $valueVehiculos)
                          <tr>
                            <td><span class="badge badge-primary">{{$item++}}</span></td>
                            <td>{{$valueVehiculos->plate}}</td>
                            <td>{{$valueVehiculos->category}}</td>
                            <td>{{$valueVehiculos->mark}}</td>
                            <td>{{$valueVehiculos->model}}</td>
                            <td>{{$valueVehiculos->year}}</td>
                            <td>{{$valueVehiculos->name}}</td>
                            <td><span class="label label-success">{{$valueVehiculos->description}}</span></td>
                            <td>
                              <center>
                                  <a href="#" rel="modal:open" data-id="{{$valueVehiculos->id}}/{{$valueVehiculos->plate}}"  title="Editar" class="edit edit-vehicle">
                                    <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
                                  </a>
                                  &nbsp;
                                  <a  href="#" id="delete-vehicle" title="Eliminar" data-id="{{$valueVehiculos->id}}/{{$valueVehiculos->plate}}" class="delete">
                                    <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
                                  </a>
                              </center>
                            </td>
                          </tr>
                          @endforeach 
                        </tbody>
                      </table>
                    </div>
                  <div class="row">
                    <div class="pull-left" id="mostraradd" style="margin-left: 15px;   margin-top: 20px;">
                        <select id="cant_mostrar" class="form-control" onchange="filterVehiculos();">
                          <option value="10">Paginacion de 10</option>
                          <option value="20">Paginacion de 20</option>
                          <option value="50">Paginacion de 50</option>
                          <option value="100">Paginacion de 100</option>
                        </select>
                    </div>
                    <div class="pull-right" id="paginatesvehi" style="margin-right: 10px;">
                        {{$listVehiculos->links()}}
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="divRegistrarVehiculo" hidden="">
                  <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">Formulario de registro</h3>
                        </div>
                        <div class="panel-body">
                          <form id="vehicles-submit" enctype="multipart/form-data">
                            @csrf
                            <h4 class="box-title">Datos Generales </h4>
                            <div class="col-md-12 "><hr></div>
                            <input type="hidden" name="Vehid" id="Vehid">
                            <input type="hidden" name="Clientid" id="Clientid">
                            <input type="hidden" name="ClientidNew" id="ClientidNew">
                            <div class="col-md-2">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Placa:</label>
                                  <input type="text" class="form-control mayusc" name="Vehplate" id="Vehplate" placeholder="Ingresar placa" required>
                                </div>
                            </div>

                             <div class="col-md-2">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Categoria:</label>
                                  <select class="form-control mayusc" name="Vehcategory" id="Vehcategory" required="">
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Marca:</label>
                                  <select class="form-control mayusc" name="Vehmark" id="Vehmark" required="">
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Modelo:</label>
                                  <select class="form-control mayusc" name="Vehmodel" id="Vehmodel" required="" >
                                    <!--onchange="models('Vehmodel','Vehmark');"-->
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Color:</label>
                                  <input type="text" class="form-control mayusc" name="Vehcolour" id="Vehcolour" placeholder="Ingresar color" required>
                                </div>
                            </div>

                            <div class="col-md-2">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Año:</label>
                                  <input type="number" class="form-control mayusc" name="Vehyear" id="Vehyear" placeholder="Ingresar año" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">No. Motor:</label>
                                   <input type="text" class="form-control mayusc" name="Vehmotor" id="Vehmotor" placeholder="Ingresar nro. motor" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">No. Serie:</label>
                                  <input type="text" class="form-control mayusc" name="Vehserie" id="Vehserie" placeholder="Ingresar nro. serie" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Esatdo:</label>
                                  <select class="form-control mayusc" name="Vehstatus" id="Vehstatus" required></select>  
                                </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Observacion:</label>
                                  <input name="Vehobservations" id="Vehobservations" type="text" class="form-control"  placeholder="Ingresar Observación">
                                </div>
                            </div>
                            <h4 class="box-title">Datos Del Cliente </h4>
                            <div class="col-md-12 "><hr></div>

                            
                            
                            


                            <div class="col-md-3">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Tipo Documento:</label>
                                  <select class="form-control mayusc" id="Clienttype" readonly>
                                      <option value="1">D.N.I</option>
                                      <option value="2">RUC</option>
                                  </select> 
                                </div>
                            </div>

                             <div class="col-md-3">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Nro. de Documento :</label>
                                  <input type="text" class="form-control mayusc ghs" id="Clientdocument" placeholder="Ingresar documento" readonly="">
                                </div>
                            </div>

                             <div class="col-md-3">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Nombre(s):</label>
                                  <input type="text" class="form-control mayusc ghs" id="Clientname" placeholder="Ingresar nombre" required readonly="">
                                </div>
                            </div>

                            <div class="col-md-3">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">
                                    <font color="white">
                                      -------------------------------------------------------:
                                    </font>
                                  </label>
                                  <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-default">
                                    <li class="fa fa-search"></li> Buscar Cliente
                                  </button>
                                </div>
                            </div>
                          
                            <br>
                            <label style="color:red">(*) Todos los campos son requeridos.</label>
                            <div class="col-md-12">
                                <center>
                                    <button type="submit" class="btn btn-primary" id="btn-vehicle" style="margin: 5px;" value="add">
                                      <li class="fa fa-save"></li> Registrar veh.
                                    </button>
                                    <button type="button" class="btn btn-danger" style="margin: 5px;" onclick="getClearVehicles();">
                                      <li class="fa fa-close"></li> Cancelar reg.
                                    </button>
                                </center>
                            </div>
                          </form>


                           <div tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bs-example-modal-lg" id="ModalClients">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
                                  <h4 id="myModalLabel2" class="modal-title">Buscar Cliente</h4>
                                </div>
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-heading">
                                         <div class="row">
                                          
                                          <input type="text" name="searchListClient" id="searchListClient" class="form-control pull-right" placeholder="Buscar" style="width: 200px; margin-right: 10px;">
                                          </div>
                                        </div>
                                        <div class="panel-body" id="divListaCliente" >
                                          <div class="table-responsive">
                                            <table id="ListClientSearch" class="table">
                                              <thead>
                                                <tr class="bg-primary">
                                                    <th></th>
                                                    <th>Nombre</th>
                                                    <th>Tipo</th>
                                                    <th>Documento</th>
                                                    <th>No. Teléfonico</th>
                                                    <th>Estatus</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                  <tbody id="cv_listPaginates">
                                                          @foreach($listaClientes as $valueCl)
                                                          <tr role="row" onclick="SelectCleint('{{$valueCl->id}}','{{$valueCl->idtype}}','{{$valueCl->name}}','{{$valueCl->document}}')">
                                                              <td><li class="fa fa-hand-o-up"></li></td>
                                                              <td>{{$valueCl->name}}</td>
                                                              <td>{{$valueCl->type}}</td>
                                                              <td>{{$valueCl->document}}</td>
                                                              <td>{{$valueCl->phone}}</td>
                                                              <td><span class="label label-success">{{$valueCl->description}}</span></td>
                                                          </tr>
                                                          @endforeach
                                                    </tbody>
                                              </tbody>
                                            </table>
                                          </div>
                                          
                                          <div class="row">
                                            <div class="pull-right" id="cv_paginates" style="margin-right: 10px;">
                                                {{$listaClientes->links()}}
                                            </div>
                                          </div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" data-dismiss="modal" class="btn btn-default buttonCerrarCleintes">Cerrar</button>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
  <script src="/js/client/client.js?v={{ time() }}"></script>
  <script src="/js/vehicle/vehicle.js?v={{ time() }}"></script>
</html>