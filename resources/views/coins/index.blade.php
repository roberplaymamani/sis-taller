@extends('layouts.index')
@extends('coins.edit')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
 <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Listado  de Monedas</h1>
        <div style="margin-top:10px" align='right'><a href="#editCoins" rel="modal:open" data-id="0" data-toggle="tooltip" title="Agregar" class="add add-coins"><img src="img/add.svg" width="30px" height="30px"></a>&nbsp;&nbsp;<a href="{{url('coins/listCoins/export')}}" data-id="0" data-toggle="tooltip" title="Exportar a Excel" onmouseout="this.style.opacity=1;this.filters.alpha.opacity='100';" onmouseover="this.style.opacity=0.8;this.filters.alpha.opacity='20';"><img src="img/excel.svg" width="35px" height="35px"></a></div>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list-ul"></i>Monedas</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>    

    <section class="content">
        <div class="row">
           <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        @include('common.notification')
                        <table width='100%' class="table table-bordered table-striped table-resposive mayuscInit listCoinsRefres" id="listCoins" align="center">
                                <thead>
                                <tr style="background:#222d32;color:#fff">
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Abreviación</th>
                                    <th>Símbolo</th>
                                    <th>Estatus</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection