<div id="editModels" class="modal" style="max-width: 700px;margin-top:50px;">
    <section class="content-header">
        <h1><text id="tittleModels"></text> Sub Productos</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list-ul"></i>Sub Productos</a></li>
            <li class="active"><text id="tittleModelsHr"></text></li>
        </ol>
    </section>   
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h3 class="box-title">Datos Generales Asesores De Servicio</h3></div>
                    <form id="models-submit" enctype="multipart/form-data">
                        <div>
                            <input type="hidden" name="Modelsid" id="Modelsid">
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="form-group col-md-4">
                                        <label for="categoria">Marca<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="Modelsidmark" id="Modelsidmark" required></select>   
                                    </div>
                                    
                                    <div class="form-group col-md-4">
                                        <label for="placa">Modelo<label style="color:red">*</label></label>
                                        <input type="text" class="form-control mayusc" name="Modelsdescription" id="Modelsdescription" placeholder="Ingresar sub producto" required>
                                    </div>
        
                                    <div class="form-group col-md-4">
                                        <label for="placa">Abreviación<label style="color:red">*</label></label>
                                        <input type="text" class="form-control mayusc" name="Modelsabreviation" id="Modelsabreviation" placeholder="Ingresar Abreviación" required>
                                    </div>
                                </div> 
                                
                                <div class="col-md-12">
                                    <div class="form-group col-md-4">
                                        <label for="categoria">Estatus<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="Modelsstatus" id="Modelsstatus" required></select>   
                                    </div>
                                </div> 
                            </div>
        
                            <div class="box-footer">
                                <label style="color:red">(*) Todos los campos son requeridos.</label>
                                
                                <center>
                                    <button type="submit" class="btn btn-primary" id='btn-models' value="edit">Editar</button>
                                    <button type="reset" class="btn btn-default" onclick="getClearModels()">Cerrar</button>
                                </center>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </section>
</div>