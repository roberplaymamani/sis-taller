@foreach($listainventario as $valueInvent)
<tr>
    <td>{{$valueInvent->description}}</td> 
    <td>{{$valueInvent->motive}}</td>
    <td style="text-align: right;">{{$valueInvent->value_sale}}</td>
    <td style="text-align: right;">{{$valueInvent->taxt}}</td>
    <td style="text-align: right;">{{$valueInvent->total_net}}</td>
    <td style="text-align: center;">{{$valueInvent->date_registration}}</td>
    <td style="text-align: center;">
      @if($valueInvent->status=='Activo')
        <span class="label label-success">{{$valueInvent->status}}</span>
      @else
        <span class="label label-danger">{{$valueInvent->status}}</span>
      @endif
    </td>
</tr>
@endforeach 