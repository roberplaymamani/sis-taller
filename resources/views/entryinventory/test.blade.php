<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
  <head>
    <meta charset="utf-8">
    <title>Admin | Social - Premium Responsive Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/template/css/social.core.css" rel="stylesheet">
    <link href="/template/css/social.admin.css" rel="stylesheet">
    <!-- Chose your icons set-->
    <link href="/template/css/glyphicons_free/glyphicons.css" rel="stylesheet">
    <link href="/template/css/glyphicons_pro/glyphicons.css" rel="stylesheet">
    <link href="/template/css/glyphicons_pro/glyphicons.halflings.css" rel="stylesheet">
    <link href="/template/css/font-awesome/font-awesome.css" rel="stylesheet">
    <style>
      .wrapper .main {
        margin-top: 40px;
      }
      @media screen and (max-width: 480px) {
        .wrapper .main {
          margin-top: 80px;
        }
      }
    </style>
    <link href="/template/css/jquery-ui/social/jquery.ui.css" rel="stylesheet">
    <!-- Current theme-->
    <link id="current-theme" href="/template/css/themes/admin/facebook.css" rel="stylesheet">
    <!-- BEGIN CURRENT PAGE STYLES-->
    <link href="/template/css/plugins/x-editable/x-editable.css" rel="stylesheet">
    <link href="/template/css/plugins/select2/select2.css" rel="stylesheet">
    <link href="/template/css/plugins/select2/select2-default.css" rel="stylesheet">
    <link href="/template/css/plugins/datatables/datatables-rtl.css" rel="stylesheet">
    <!-- END CURRENT PAGE STYLES-->
    <!-- BEGIN DEMO FILES-->
    <link href="/template/css/demo.css" rel="stylesheet">
    <link href="/template/js/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet">
    <!-- END DEMO FILES-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--[if lte IE 8]>
    <script src="/template/js/html5shiv/html5shiv.js"></script>
    <script src="/template/js/plugins/respond/respond.min.js"></script>
    <script src="/template/js/plugins/flot/excanvas.min.js"></script> 
    <![endif]-->
  </head>
  <body>

    <!-- BEGIN THEME SWITCHER-->
    <div class="theme-switcher">
      <a href="#"><i class="fa fa-cogs"></i>
      </a>
      <div class="content"><strong>Color Style</strong>
        <select name="colorpicker" class="styles">
          <option value="#fff" data-theme="/template/css/themes/admin/default.css">Default</option>
          <option value="#647AAB" data-theme="/template/css/themes/admin/facebook.css">Facebook</option>
          <option value="#242424" data-theme="/template/css/themes/admin/inverse.css">Inverse</option>
          <option value="#62c462" data-theme="/template/css/themes/admin/green.css">Green</option>
          <option value="#394263" data-theme="/template/css/themes/admin/blue-sidebar.css">Blue Sidebar</option>
        </select>
        <hr>
        <a href="../admin-rtl/index.html"><strong>RTL Version</strong>
        </a>
      </div>
    </div>
    <!-- END THEME SWITCHER-->
    <!-- jQuery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
      window.jQuery || document.write('<script src="/template/js/jquery/jquery.min.js"><\/script>')
    </script>
    <!-- Bootstrap JS-->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script>
      $.fn.modal || document.write('<script src="/template/js/plugins/bootstrap/bootstrap.min.js"><\/script>')
       // Prevent jQueryUI Conflicts
       var bootstrapTooltip = $.fn.tooltip.noConflict()
       $.fn.bootstrapTooltip = bootstrapTooltip
    </script>
    <!-- jQueryUI-->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
      window.jQuery.ui || document.write('<script src="/template/js/jquery-ui/jquery-ui.min.js"><\/script>')
    </script>
    <!-- Bootstrap Hover Dropdown-->
    <script src="/template/js/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <!-- jQuery slimScroll-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.1/jquery.slimscroll.min.js"></script>
    <script>
      window.jQuery.ui || document.write('<script src="/template/js/plugins/jquery.slimscroll/jquery.slimscroll.min.js"><\/script>')
    </script>
    <!-- BEGIN THEME SWITCHER SCRIPTS-->
    <script>
      var assets_dir = '/template/'
    </script>
    <script src="/template/js/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
    <script src="/template/js/demo/theme-switcher-admin.js"></script>
    <!-- END THEME SWITCHER SCRIPTS-->
    <script src="/template/js/sidebar.js"></script>
    <script src="/template/js/panels.js"></script>
    <!-- BEGIN GENERAL SCRIPTS-->
    <script>
      /*<![CDATA[*/
      $(function() {
        $(".social-sidebar").socialSidebar();
        $('.main').panels();
        $(".main a[href='#ignore']").click(function(e) {
          e.stopPropagation()
        });
      });
      $(document).on('click', '.navbar-super .navbar-super-fw', function(e) {
        e.stopPropagation()
      });
      /*]]>*/
    </script>
    <!-- END GENERAL SCRIPTS-->
    <!-- BEGIN CURRENT PAGE SCRIPTS-->
    <script src="/template/js/plugins/select2/select2.js"></script>
    <script src="/template/js/plugins/moment.js/moment.min.js"></script>
    <script src="/template/js/plugins/x-editable/bootstrap-editable.min.js"></script>
    <script src="/template/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/template/js/demo/dataTables.bootstrap.js"></script>
    <script src="/template/js/demo/tables.js"></script>
    <script>
      $(function() {
        Tables.initDynamicTables();
      });
    </script>
    <!-- END CURRENT PAGE SCRIPTS-->
  </body>
</html>