<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div id="content_sistema">
          <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Inventarios</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Ingreso y salida de inventario</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <div class="row">

                  <button type="button" class="btn btn-primary left_ovalado"  id="list"  onclick="AddInventarioDIV('list')" disabled="">
                    <span class="fa fa-list fa-lg"></span> Lista ingresos y salidas
                  </button>

                  <button type="button" class="btn btn-tumblr add add-entryadd left_ovalado" data-id="0" id="addInv" onclick="AddInventarioDIV('addInv')">
                    <span class="fa fa-arrow-down fa-lg"></span>Registrar Entrada Inv.
                  </button>

                  <button type="button" class="btn btn-linkedin add add-entryadd left_ovalado" data-id="1" id="salInv" onclick="AddInventarioDIV('salInv')">
                    <span class="fa fa-arrow-up fa-lg"></span>Registrar Salida Inv.
                  </button>


                  <a type="button" class="btn btn-success left_ovalado"  href="{{url('entryinventory/listEntryInventory/export')}}">
                    <span class="fa fa-file-o"></span> Exportar Excel
                  </a>

                  <button type="button" class="btn btn-danger left_ovalado"  onclick="download_pdf_reporte()">
                    <span class="fa fa-file-o"></span> Exportar PDF
                  </button>

                  <input type="text" name="searchList_inv" id="searchList_inv" class="form-control pull-right right_field" placeholder="Buscar"  autocomplete="off"> 


                  <input type="text" class="form-control pull-right right_field" name="filterInventory_x_date" id="filterInventory_x_date" value="{{date('Y-m-d')}}" autocomplete="off" placeholder="000-00-00" >

                  <!--<i class="fa fa-close pull-right fa-lg" style=" padding-top: 10px; margin-right: 15px;" hidden=""></i>-->
                  </div>
                </div>
                <div class="panel-body" id="divSalidaIngresoInvList" >
                  <div class="table-responsive">
                    <table id="foo-table" class="table">
                      <thead>
                        <tr class="bg-primary">
                            <th>Número de Guía</th>
                            <th>Tipo de Operación</th>
                            <th class="th_td_right">Valor de Venta</th>
                            <th class="th_td_right">Impuesto</th>
                            <th class="th_td_right">Total Neto</th>
                            <th class="th_td_center">Fecha de Registro</th>
                            <th class="th_td_center">Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tbody id="listPaginates_inv">  
                             @foreach($listainventario as $valueInvent)
                              <tr>
                                  <td>{{$valueInvent->description}}</td> 
                                  <td>{{$valueInvent->motive}}</td>
                                  <td class="th_td_right">{{$valueInvent->value_sale}}</td>
                                  <td class="th_td_right">{{$valueInvent->taxt}}</td>
                                  <td class="th_td_right">{{$valueInvent->total_net}}</td>
                                  <td class="th_td_center">{{$valueInvent->date_registration}}</td>
                                  <td class="th_td_center">
                                    @if($valueInvent->status=='Activo')
                                      <span class="label label-success">{{$valueInvent->status}}</span>
                                    @else
                                      <span class="label label-danger">{{$valueInvent->status}}</span>
                                    @endif
                                  </td>
                              </tr>
                              @endforeach 
                            </tbody>
                      </tbody>
                    </table>
                  </div>
                  
                  <div class="row">
                     <div class="pull-left" id="mostraradd" style="margin-left: 15px;   margin-top: 20px;">
                        <select id="cant_mostrar" class="form-control" onchange="filterProducts();">
                          <option value="10">Paginacion de 10</option>
                          <option value="20">Paginacion de 20</option>
                          <option value="50">Paginacion de 50</option>
                          <option value="100">Paginacion de 100</option>
                        </select>
                    </div>
                    <div class="pull-right" id="paginates_inv" style="margin-right: 10px;">
                        {{$listainventario->links()}}
                    </div>
                  </div>
                </div>


                <!--AQUIII-->
                <div class="panel-body" id="divSalidaIngresoInv" hidden="" >
                  <div id="wizard0" class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">FORMULARIO DE <label>REGISTRO</label> DE <label id="tipoAdd"></label> DE INVENTARIO</h3>
                    </div>
                  </div>
                  <form id="entryinventory-submit" enctype="multipart/form-data">
                   <div class="col-md-12">
                      <input type="hidden" name="id_entry_inventoryss" id="id_entry_inventoryss">
                      <input type="hidden" name="EntryInventoryid" id="EntryInventoryid">
                      <input type="hidden" name="EntryExitInventory" id="EntryExitInventory">
                      <div class="panel panel-default" style="margin-top: 0px;">
                        <div class="panel-body">
                         <div class="form-group has-success">
                           <div class="col-md-2">
                              <div class="form-group has-success">
                                <label for="inputSuccess1" class="control-label">Fecha:</label>
                                <input type="text" class="form-control mayusc" name="EntryInventorydate" id="EntryInventorydate" value="{{date('Y-m-d')}}" required> 
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Movito:</label>
                                <select class="form-control mayusc" name="EntryInventoryid_reasons" id="EntryInventoryid_reasons" required></select>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Almacén:</label>
                                <select class="form-control mayusc" name="EntryInventoryid_store" id="EntryInventoryid_store" required></select>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Moneda:</label>
                                <select class="form-control mayusc" name="EntryInventoryid_coins" id="EntryInventoryid_coins" onchange="coins('textsymbol','EntryInventoryid_coins');" required></select>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Estado:</label>
                                <select class="form-control mayusc" name="EntryInventorystatus" id="EntryInventorystatus" required></select> 
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-primary">
                                <label for="inputSuccess1" class="control-label">Total Neto:</label> <label class='textsymbol'></label>
                                 <input type="text" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytotal_nets" id="EntryInventorytotal_nets" placeholder="0.00" readonly> 
                              </div>
                            </div>

                            <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytaxt" id="EntryInventorytaxt" placeholder="0.00" readonly>
                            <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventoryvalue_sale" id="EntryInventoryvalue_sale" placeholder="0.00" readonly>
                            <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytotal_net" id="EntryInventorytotal_net" placeholder="0.00" readonly>

                          </div>
                        </div>
                      </div>
                    </div>
                    <div hidden="">
                      <button type="submit" class="btn btn-primary" id="submit_form" value="add">
                        Registrar movi
                      </button>
                    </div>
                  </form>

                  <div class="col-md-8">
                    <div id="panels-scollable" class="row">
                      <div class="col-md-12">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <div class="panel-title"><i class="fa fa-desktop"></i>Productos agregados</div>
                          </div>
                          <div class="panel-body hight_scroll">
                            <!-- //Notice .scroll class-->
                            <div class="scroll">
                              <div class="table-responsive">
                                <!-- <table   class="table table-striped table-bordered table-hover table-responsive listEntryRefres"> -->
                                <table   class="table table-striped table-bordered table-hover table-responsive">
                                  <thead>
                                    <tr class="bg-primary">
                                      <td>No.</td>
                                      <th>PRODUCTO</th>
                                      <th>CANTIDAD</th>
                                      <th>PRECIO</th>
                                      <th>TOTAL</th>
                                      <th><center>ACCION</center></th>
                                    </tr>
                                  </thead>
                                  <tbody id="listitemsAdd">
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <div class="panel-footer">
                             <center>
                              <button type="button" class="btn btn-primary margin5" id="btn-entryinventory"  value="add" onclick="$('#submit_form').click();">
                              </button>
                              <button type="reset" class="btn btn-danger margin5" id="cancelarRegistro"  onclick="getClearEntryInventory(0)"></button>
                            </center>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  
                   


                  
                    <div class="col-md-4">
                      <form id="entry-submit" enctype="multipart/form-data">
                      <div class="panel panel-success">
                        <div class="panel-heading">
                          <center><i>AGREGAR DETALLES DE <label id="detallesAdd"></label></i></center>
                        </div>
                        <div class="panel-body">
                            
                            <div class="col-md-12">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Codigo Producto:</label>
                                  <input type="hidden" name="id_entry" id="id_entry">
                                  <input type="hidden" name="id_entry_inventorys" id="id_entry_inventorys">
                                  <input type="text" class="form-control mayusc" name="EntryInventoryid_products" id="EntryInventoryid_products" placeholder="Código de producto" onkeyup="products('EntryInventorydescription','EntryInventoryprice','EntryInventoryid_products');" onchange="products('EntryInventorydescription','EntryInventoryprice','EntryInventoryid_products');" required autocomplete="off" list="lisproducto"> 
                                </div>
                            </div>
                            <datalist id="lisproducto" style="height: 5px;">
                            </datalist>

                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Nombre Producto:</label>
                                  <select class="form-control mayusc" name="EntryInventorydescription" id="EntryInventorydescription" onchange="products('EntryInventoryid_products','EntryInventoryprice','EntryInventorydescription');" required></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Cantidad:</label>
                                  <input type="number" class="form-control mayusc fixed-input-a" name="EntryInventoryquantity" id="EntryInventoryquantity" placeholder="0" onkeyup="pricesEntry('CANT')" onchange="pricesEntry('CANT')" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Precio:</label> <label class='textsymbol fixed-input-d'></label>
                                  <input type="text" class="form-control mayusc fixed-input-a" name="EntryInventoryprice" id="EntryInventoryprice" placeholder="0.00" onkeyup="format(this);pricesEntry('PRE')" onchange="format(this);pricesEntry('PRE')" required>
                                  
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Total:</label>  <label class='textsymbol fixed-input-d'></label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                   
                                </div>
                            </div>
                            <br>
                              <label style="color:red">(*) Todos los campos son requeridos.</label>
                              <center>
                                <div class="col-md-6">
                                  <button type="submit" class="btn btn-success margin5_ovalado" id="btn-newitems" value="add">
                                  <li class="fa fa-plus-square"></li> Agregar Item.
                                </button>
                                </div>
                                <div class="col-md-6">
                                  <button type="reset" class="btn btn-danger margin5_ovalado"  onclick="getClearNewitems(0)">
                                  <li class="fa fa-close"></li> Cancelar item.
                                </button>
                                </div>
                              </center>
                        </div>
                      </div>
                      <br>
                    </form>
                    </div>
                  
                </div>



  














              
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    @include('layouts.scripts')
    <script src="/template/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
    <script>
      $(function() {
        $('#wizard0, #wizard1, #wizard2').bootstrapWizard({
          tabClass: 'nav nav-tabs',
          onTabShow: function(tab, navigation, index) {
            var $elem = navigation.closest('.panel');
            var $total = navigation.find("li[class!='title']").length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $elem.find('.progress-bar').css({
              width: $percent + '%'
            });
            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
              $elem.find('.panel-footer .button-next').hide();
              $elem.find('.panel-footer .button-last').hide();
              $elem.find('.panel-footer .button-finish').show();
              $elem.find('.panel-footer .button-finish').removeClass('disabled');
            } else {
              $elem.find('.panel-footer .button-next').show();
              $elem.find('.panel-footer .button-last').show();
              $elem.find('.panel-footer .button-finish').hide();
            }
            /*$elem.find('.button-finish').click(function() {
              $('#myModal').modal("show");
            });*/
          },
          'nextSelector': '.button-next',
          'previousSelector': '.button-previous',
          'firstSelector': '.button-first',
          'lastSelector': '.button-last'
        });
      })
    </script>
    <script src="/js/common/common.js?v={{ time() }}"></script>
    <!--<script src="/js/product/product.js?v={{ time() }}"></script>--> 
    <script src="/js/entryinventory/entryinventory.js?v={{ time() }}"></script>
  </body>
</html>