<style type="text/css">
  * {
    margin: 10px; 
    padding: 0px; 
    box-sizing: border-box;
  }
  table {
    border-spacing: 1;
    border-collapse: collapse;
    background: white;
    border-radius: 10px;
    overflow: hidden;
    width: 100%;
    margin: 0 auto;
    position: relative;
  }
  table * {
    position: relative;
  }
  table td, table th {
    padding-left: 8px;
  }
  table thead tr {
    height: 60px;
    background: #647aab;
  }
  table tbody tr {
    height: 50px;
  }
  table tbody tr:last-child {
    border: 0;
  }
  table td, table th {
    text-align: left;
  }
  table td.l, table th.l {
    text-align: right;
  }
  table td.c, table th.c {
    text-align: center;
  }
  table td.r, table th.r {
    text-align: center;
  }
  th{
    font-family: OpenSans-Regular;
    font-size: 10px;
    color: #fff;
    line-height: 1.5;
    font-weight: bold;
  }
  tbody tr:nth-child(even) {
    background-color: #f5f5f5;
  }
  tbody tr {
    font-family: OpenSans-Regular;
    font-size: 15px;
    line-height: 2;
    font-weight: bold;
  }

  tbody tr:hover {
    color: #555555;
    background-color: #f5f5f5;
    cursor: pointer;
  }
}


  </style>
<!--===============================================================================================-->
  <center>
    <h4><b><i><u>"Sistema de gestion de talleres automotrices"</u></i></b></h4>
  </center>
  <table>
    <td width="50%">
      <div>
         <table style="" >
          <tr style="padding-bottom: 20px;"> 
            <th style="color: #566d9f;">EMPRESA: </th>
            <td>{{$DataEmp['name']}}</td>
          </tr>
          <tr >
            <th style="color: #566d9f; background: white;">RUC: </th>
            <td style="background: white;">{{$DataEmp['ruc']}}</td>
          </tr>
           <tr >
            <th style="color: #566d9f;">FECHA: </th>
            <td >{{date('Y-m-d')}}</td>
          </tr>
        </table>
      </div>
    </td>
    <td width="50%" style="text-align: right;">
      <img src="data:image/jpeg;base64,{{$DataEmp['logo']}}" class="company_logo"  width="150" alt="Company Logo" class="logo" style="margin-right: 50px;">
    </td>
  </table>
  <hr>
  <center>
    <h4>REPORTE GENERAL DE INGRESOS Y SALIDAS</h4>
  </center>
  <table>
    <thead>
      <tr>
        <TH>No.</TH>
        <th>Número de Guía</th>
        <th style="text-align: center;">Fecha de Registro</th>
        <th>Tipo de Operación</th>
        <th style="text-align: center;">Estado</th>
        <th style="text-align: right;">Valor de Venta</th>
        <th style="text-align: right;">Impuesto</th>
        <th style="text-align: right;">Total Neto</th>
        
      </tr>
    </thead>
    <tbody>
      @php($No=1)
      @php($TV=0)
      @php($TI=0)
      @php($IT=0)
      @foreach($listainventario as $valueInvent)
      <tr>
          <td>{{$No++}}</td> 
          <td>{{$valueInvent->description}}</td> 
           <td style="text-align: center;">{{$valueInvent->date_registration}}</td>
          <td>{{$valueInvent->motive}}</td>
          <td style="text-align: center;">
            @if($valueInvent->status=='Activo')
              <font color="green">{{$valueInvent->status}}</font>
            @else
              <font color="red">{{$valueInvent->status}}</font>
            @endif
          </td>
          <td style="text-align: right;">{{$valueInvent->value_sale}}</td>
          <td style="text-align: right;">{{$valueInvent->taxt}}</td>
          <td style="text-align: right;">{{$valueInvent->total_net}}</td>
      </tr>
      @endforeach 
    </tbody>
  </table>

  {{--<table>
    <td width="70%" style="text-align: right;">
    </td>
    <td width="30%">
      <div>
         <table style="" border="1">
          <tr style="padding-bottom: 20px;"> 
            <th style="color: #566d9f;">Total Ventas: </th>
            <td>{{$TV}}</td>
          </tr>
          <tr >
            <th style="color: #566d9f; background: #647aab;">Total Ventas: </th>
            <td style="background: white;">{{$TI}}</td>
          </tr>
           <tr >
            <th style="color: #566d9f;">Total Ventas: </th>
            <td >{{$IT}}</td>
          </tr>
        </table>
      </div>
    </td>
  </table> --}}