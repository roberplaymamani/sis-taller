@extends('layouts.index')
@extends('entryinventory.edit')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Listado Ingreso o Salida de Inventario</h1>
        <div style="margin-top:10px" align='right'>
            <a href="#editEntryInventory" rel="modal:open" data-id="0" data-toggle="tooltip" title="Entrada de inventario" class="add add-entryadd effetc"><img src="img/add.svg" width="30px" height="30px">
            </a>
            &nbsp;&nbsp;
            <a href="#editEntryInventory" rel="modal:open" data-id="1" data-toggle="tooltip" title="Salida de inventario" class="add add-entryadd effetc">
                <img src="img/exit.svg" width="30px" height="30px">
            </a>
            &nbsp;&nbsp;
            <a href="{{url('entryinventory/listEntryInventory/export')}}" data-id="0" data-toggle="tooltip" title="Exportar a Excel" class="effetc">
                <img src="img/excel.svg" width="35px" height="35px">
            </a>
        </div>
   
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list-ul"></i>Ingresos de Inventario</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>    

    <section class="content">
        <div class="row">
           <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        @include('common.notification')
                        <table width='100%' class="table table-bordered table-striped table-resposive mayuscInit listEntryInventoryRefres" id="listEntryInventory" align="center">
                            <thead>
                                <tr style="background:#222d32;color:#fff">
                                    <th>Id</th>
                                    <th>Número de Guía</th>
                                    <th>Tipo de Operación</th>
                                    <th>Valor de Venta</th>
                                    <th>Impuesto</th>
                                    <th>Total Neto</th>
                                    <th>Fecha de Registro</th>
                                    <th>Estatus</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection