@php($item=1)
@foreach($listDetallesIngresoSalida as $valueInventAdd)
<tr>
    <td><span class="badge badge-primary">{{$item++}}</span></td> 
    <td>{{$valueInventAdd->description}}</td>
    <td  hidden="">{{$valueInventAdd->id_products}}</td>
    <td ><span class="label label-success">{{$valueInventAdd->quantity}}</span></td>
    <td >{{$valueInventAdd->price}}</td>
    <td >{{$valueInventAdd->total}}</td>
    <td >
      <center>
        <button type="button" class="btn btn-info btn-xs edit edit-entryedit" style="border-radius: 5px;" data-id="{{$valueInventAdd->id}}/{{$valueInventAdd->description}}">
          <span class="fa fa-edit"></span>&nbsp;Editar
        </button>
        <button type="button" class="btn btn-danger btn-xs delete" style="border-radius: 5px;" data-id="{{$valueInventAdd->id}}/{{$valueInventAdd->description}}" id="delete-entrydel">
          <span class="fa fa-edit"></span>&nbsp;Eliminar
        </button>
      </center>
    </td>
</tr>
@endforeach 