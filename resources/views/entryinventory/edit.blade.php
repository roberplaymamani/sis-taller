<div id="editEntryInventory" class="modal" style="max-width: 700px;margin-top:50px;">
    <section class="content-header">
        <h1><text class="tittleEntryInventory"></text> de Inventario</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-list-ul"></i><text class="tittleEntryInventoryNew"></text> de Inventario</a></li>
            <li class="active"><text class="tittleEntryInventoryHr"></text></li>
        </ol>
    </section>   
    
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="col-md-6">
                    <div class="box-header with-border"><h3 class="box-title">Datos Generales</h3></div>
                        <form id="entryinventory-submit" enctype="multipart/form-data">
                            <div>
                                <input type="hidden" name="id_entry_inventoryss" id="id_entry_inventoryss">
                                <input type="hidden" name="EntryInventoryid" id="EntryInventoryid">
                                <input type="hidden" name="EntryExitInventory" id="EntryExitInventory">
                                <div class="box-body">
                                    <div class="form-group col-md-12">
                                        <label for="codigo">Fecha<label style="color:red">*</label></label>
                                        <input type="text" class="form-control mayusc" name="EntryInventorydate" id="EntryInventorydate" value="{{date('Y-m-d')}}" required> 
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="descripcion">Motivo<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="EntryInventoryid_reasons" id="EntryInventoryid_reasons" required></select>   
                                    </div>
                                        
                                    <div class="form-group col-md-12">
                                        <label for="placa">Almacén<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="EntryInventoryid_store" id="EntryInventoryid_store" required></select>   
                                    </div>
                                
                                    <div class="form-group col-md-12">
                                        <label for="placa">Moneda<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="EntryInventoryid_coins" id="EntryInventoryid_coins" onchange="coins('textsymbol','EntryInventoryid_coins');" required></select>   
                                    </div>
                                    
                                    <div class="form-group col-md-12">
                                        <label for="categoria">Estatus<label style="color:red">*</label></label>
                                        <select class="form-control mayusc" name="EntryInventorystatus" id="EntryInventorystatus" required></select>   
                                    </div>    
                                </div>

                                <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytaxt" id="EntryInventorytaxt" placeholder="0.00" readonly>
                                <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventoryvalue_sale" id="EntryInventoryvalue_sale" placeholder="0.00" readonly>
                                <input type="hidden" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytotal_net" id="EntryInventorytotal_net" placeholder="0.00" readonly>
                                    
                                <div class="col-md-12">
                                    <div class="box-footer">
                                        <label style="color:red">(*) Todos los campos son requeridos.</label>
                                        
                                        <center>
                                            <button type="submit" class="btn btn-primary" id='btn-entryinventory' value="edit">Editar</button>
                                            <!--button type="reset" class="btn btn-default" onclick="getClearEntryInventory()">Cerrar</button-->
                                        </center>

                                        <label style="display:block" id='textNotification'>Para activar el botón de registrar, debes agregar al menos (01) producto al listado..</label>
                                    </div>
                                </div>   
                            </div>
                        </form>
                    </div>  

                    <form id="entry-submit" enctype="multipart/form-data">
                        <div class="col-md-6">
                            <div class="box-header with-border"><h3 class="box-title">Detalle del Producto</h3></div>
                        
                            <div>
                                <div class="box-body">
                                    <div class="form-group col-md-12">
                                        <label for="placa">Producto<label style="color:red">*</label></label>
                                        <input type="hidden" name="id_entry" id="id_entry">
                                        <input type="hidden" name="id_entry_inventorys" id="id_entry_inventorys">
                                        <input type="text" class="form-control mayusc" name="EntryInventoryid_products" id="EntryInventoryid_products" placeholder="Código de producto" onkeyup="products('EntryInventorydescription','EntryInventoryprice','EntryInventoryid_products');" onchange="products('EntryInventorydescription','EntryInventoryprice','EntryInventoryid_products');" required> 
                                        &nbsp;
                                        <select class="form-control mayusc" name="EntryInventorydescription" id="EntryInventorydescription" onchange="products('EntryInventoryid_products','EntryInventoryprice','EntryInventorydescription');" required></select>
                                    </div>
                                    
                                    <div class="form-group col-md-12">
                                        <label for="categoria">Cantidad<label style="color:red">*</label></label>
                                        <input type="number" class="form-control mayusc fixed-input-a" name="EntryInventoryquantity" id="EntryInventoryquantity" placeholder="0" onkeyup="pricesEntry('CANT')" onchange="pricesEntry('CANT')" required>
                                        <label class='fixed-input-d'>Ca.</label>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="categoria">Precio<label style="color:red">*</label></label>
                                        <input type="text" class="form-control mayusc fixed-input-a" name="EntryInventoryprice" id="EntryInventoryprice" placeholder="0.00" onkeyup="format(this);pricesEntry('PRE')" onchange="format(this);pricesEntry('PRE')" required>
                                        <label class='textsymbol fixed-input-d'></label>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="categoria">Total<label style="color:red">&nbsp;</label></label>
                                        <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                        <label class='textsymbol fixed-input-d'></label>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="box-footer">
                                            <center>
                                                <button type="submit" class="btn-img"><img src="img/checked.svg" width="30px" height="30px" class="effetc" id='btn-newitems' title="Registrar" data-toggle="tooltip"></button>
                                                <button type="reset" class="btn-img" onclick="getClearNewitems(0)"><img src="img/x-button.svg" width="30px" height="30px" class="effetc" title="Cancelar" data-toggle="tooltip"></button>
                                            </center>
                                        </div>
                                    </div> 
                                </div>
                            </div>  
                        </div> 

                        <div class="col-md-12">
                            <div class="box-header with-border"><h3 class="box-title">Detalle de <text class="tittleEntryInventoryNew"></text></h3></div>

                            <div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                @include('common.notification')
                                                <table width='100%' class="table table-bordered table-striped table-resposive mayuscInit listEntryRefres" id="listEntry" align="center">
                                                    <thead>
                                                        <tr style="background:#222d32;color:#fff">
                                                            <th>Producto</th>
                                                            <th>Cantidad</th>
                                                            <th>Precio</th>
                                                            <th>Total</th>
                                                            <th>Acción</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" align='right'>
                                        <label for="categoria"  align='center'>Total Neto <label class='textsymbol'></label><label style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></label>
                                        <input type="text" class="form-control mayusc fixed-input-b ghs" name="EntryInventorytotal_nets" id="EntryInventorytotal_nets" placeholder="0.00" readonly>
                                    </div>
                                </div>
                            </div>   
                        </div> 
                </form>
            </div>
        </div>       
    </section>
</div>