 <head>
    <meta charset="utf-8">
    <title>SIS-TALLER</title>
    <link rel="icon" type="ico/favicon" href="/template/img/logo1.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/template/css/social.core.css" rel="stylesheet">
    <link href="/template/css/social.admin.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Chose your icons set-->
    <link href="/template/css/glyphicons_free/glyphicons.css" rel="stylesheet">
    <link href="/template/css/glyphicons_pro/glyphicons.css" rel="stylesheet">
    <link href="/template/css/glyphicons_pro/glyphicons.halflings.css" rel="stylesheet">
    <link href="/template/css/font-awesome/font-awesome.css" rel="stylesheet">
    <style>
      .wrapper .main {
        margin-top: 40px;
      }
      @media screen and (max-width: 480px) {
        .wrapper .main {
          margin-top: 80px;
        }
      }
    </style>
    <link href="/template/css/jquery-ui/social/jquery.ui.css" rel="stylesheet">
    <!-- Current theme-->
    <link id="current-theme" href="/template/css/themes/admin/facebook.css" rel="stylesheet">
    <!-- BEGIN CURRENT PAGE STYLES-->
    <link href="/template/js/plugins/jqvmap/jqvmap.css" rel="stylesheet">
    <link href="/template/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/template/js/plugins/pnotify/pnotify.custom.min.css" rel="stylesheet">
    <!-- END CURRENT PAGE STYLES-->
    <!-- BEGIN DEMO FILES-->
    <link href="/template/css/demo.css" rel="stylesheet">
    <link href="/template/js/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet">
    <!-- END DEMO FILES-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--[if lte IE 8]>
    <script src="/template/js/html5shiv/html5shiv.js"></script>
    <script src="/template/js/plugins/respond/respond.min.js"></script>
    <script src="/template/js/plugins/flot/excanvas.min.js"></script> 
    <![endif]-->
    <script type="text/javascript" src="/template/alertify/lib/alertify.js"></script>
    <link rel="stylesheet" href="/template/alertify/themes/alertify.core.css" />
    <link rel="stylesheet" href="/template/alertify/themes/alertify.default.css" />

    <link rel="stylesheet" href="/template/css/styles.css?v={{time()}}" />

  </head>