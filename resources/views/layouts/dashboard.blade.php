<div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Dashboard</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="btn-icon col-xs-6 col-sm-6 col-md-2 col-md-offset-1">
              <a href="{{url('user')}}" role="button" class="btn btn-warning"><i class="fa fa-user fa-lg"></i>
                <div class="title">USUARIOS</div>
              </a>
            </div>
             <div class="btn-icon col-xs-6 col-sm-6 col-md-2">
              <a href="{{url('client')}}" role="button" class="btn btn-primary"><i class="fa fa-users fa-lg"></i>
                <div class="title">CLIENTES</div>
              </a>
            </div>
            <div class="btn-icon col-xs-6 col-sm-6 col-md-2">
              <a href="{{url('products')}}" role="button" class="btn btn-success"><i class="fa fa-barcode fa-lg"></i>
                <div class="title">PRODUCTOS</div>
              </a>
            </div>
            <div class="btn-icon col-xs-6 col-sm-6 col-md-2 ">
              <a href="{{url('vehicles')}}" role="button" class="btn btn-info"><i class="fa fa-dashboard fa-lg"></i>
                <div class="title">VEHICULOS</div>
              </a>
            </div>
            <div class="btn-icon col-xs-6 col-sm-6 col-md-2">
              <a href="{{url('entryinventory')}}" role="button" class="btn btn-primary"><i class="fa fa-bar-chart-o fa-lg"></i>
                <div class="title">INVENTARIO</div>
              </a>
            </div>
            <!-- END ICON BUTTONS SET-->
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="panel-title"><i class="fa fa-bar-chart-o"></i>Visits</div>
                  <div class="panel-tools pull-right">
                    <div class="icon-tools"><i data-option="collapse" class="fa fa-chevron-down"></i><i data-toggle="modal" data-target="#myModal" class="fa fa-cog"></i><i data-dismiss="panel" class="fa fa-times"></i>
                    </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="graph-example" style="height: 300px;"></div>
                </div>
              </div>
            </div>
            <!---->
             <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Stats</h3>
                </div>
                <div class="panel-body">
                  <div class="easy-pie-chart-examples row">
                    <div class="col-sm-4 col-md-4">
                      <div data-percent="86" class="chart1 easy-pie-chart">
                        <span class="percent"></span>
                      </div>Lorem ipsum.
                    </div>
                    <div class="col-sm-4 col-md-4">
                      <div data-percent="46" class="chart2 easy-pie-chart">
                        <span class="percent"></span>
                      </div>Lorem ipsum.
                    </div>
                    <div class="col-sm-4 col-md-4">
                      <div data-percent="91" class="chart3 easy-pie-chart">
                        <span class="percent"></span>
                      </div>Lorem ipsum.
                    </div>
                  </div>
                  <br>
                  <br>
                  <div class="gauge-examples row">
                    <div class="col-sm-4 col-md-4">
                      <div id="g1" class="gauge"></div>Lorem ipsum.
                    </div>
                    <div class="col-sm-4 col-md-4">
                      <div id="g2" class="gauge"></div>Lorem ipsum.
                    </div>
                    <div class="col-sm-4 col-md-4">
                      <div id="g3" class="gauge"></div>Lorem ipsum.
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!---->
          </div>
          <div class="row"></div>
        </div>