<aside class="social-sidebar">
        <div class="social-sidebar-content">
          <!-- BEGIN USER SECTION-->
          <div class="user">
            <!-- //Notice .avatar class-->
            <img width="25" height="25" src="/template/img/avatars/avatar-30.png" alt="Julio Marquez" class="avatar"> 
            <span>{{ Auth::user()->name }}</span>
            <!-- BEGIN USER SETTINGS SECTION--><i data-toggle="dropdown" class="trigger-user-settings fa fa-user"></i>
            <div class="user-settings">
              <!-- BEGIN USER SETTINGS TITLE-->
              <h3 class="user-settings-title">Settings shortcuts</h3>
              <!-- END USER SETTINGS TITLE-->
              <!-- BEGIN USER SETTINGS CONTENT-->
              <div class="user-settings-content">
                <a href="#my-profile">
                  <!-- //Notice .icon class-->
                  <div class="icon"><i class="fa fa-user"></i>
                  </div>
                  <!-- //Notice .title class-->
                  <div class="title">My Profile</div>
                  <!-- //Notice .content class-->
                  <div class="content">View your profile</div>
                </a>
                <a href="#view-messages">
                  <!-- //Notice .icon class-->
                  <div class="icon"><i class="fa fa-envelope-o"></i>
                  </div>
                  <!-- //Notice .title class-->
                  <div class="title">View Messages</div>
                  <!-- //Notice .content class-->
                  <div class="content">
                    You have <strong>17</strong>
                    new messages
                  </div>
                </a>
                <a href="#view-pending-tasks">
                  <!-- //Notice .icon class-->
                  <div class="icon"><i class="fa fa-tasks"></i>
                  </div>
                  <!-- //Notice .title class-->
                  <div class="title">View Tasks</div>
                  <!-- //Notice .content class-->
                  <div class="content">You have <strong>8</strong> pending tasks</div>
                </a>
              </div>
              <!-- END USER SETTINGS CONTENT-->
              <!-- BEGIN USER SETTINGS FOOTER-->
              <div class="user-settings-footer">
                <a href="#more-settings">See more settings</a>
              </div>
              <!-- END USER SETTINGS FOOTER-->
            </div>
          </div>
          <!-- END USER SETTINGS SECTION-->
          <!-- EDN USER SECTION-->
          <!-- BEGIN SEARCH SECTION-->
          <div class="search-sidebar">
            <form class="search-sidebar-form has-icon">
              <label for="sidebar-query" class="fa fa-search"></label>
              <input id="sidebar-query" type="text" placeholder="Search" class="search-query">
            </form>
          </div>
          <div class="clearfix"></div>
          <!-- END SEARCH SECTION-->
          <!-- BEGIN MENU SECTION-->
          <div class="menu">
            <div class="menu-content">
              <ul id="social-sidebar-menu">
                <!-- BEGIN ELEMENT MENU-->
                <!-- //Notice .active class-->
                <li class="active">
                  <a href="/home">
                    <!-- icon--><i class="fa fa-home"></i>
                    <span>Dashboard</span>
                    <!-- badge-->
                    <span class="badge">9</span>
                  </a>
                </li>
                <!-- END ELEMENT MENU-->
                <!-- BEGIN ELEMENT MENU-->
  
               <li class="@if($options=='archivos') {{'open'}} @endif">
                  <a href="#op1" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-list-alt"></i>
                    <span>Archivos</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul  id="op1" class="collapse @if($options=='archivos') {{'in'}} @endif">
                    <li class="@if($selects=='clientes') {{'active'}} @endif">
                      <a href="{{url('client')}}"> <i class="fa fa-users"></i> Clientes</a>
                    </li>
                    <li class="@if($selects=='vehiculos') {{'active'}} @endif">
                      <a href="{{url('vehicles')}}"><i class="fa fa-car"></i> Vehículos</a>
                    </li>
                    <li class="@if($selects=='proveedores') {{'active'}} @endif">
                      <a href="{{url('listProviders')}}"><i class="fa fa-users"></i> Proveedores</a>
                    </li>
                    <li class="@if($selects=='productos') {{'active'}} @endif">
                      <a href="{{url('products')}}"><i class="fa fa-th-large"></i> Productos</a>
                    </li>
                    <li class="@if($selects=='tablas') {{'active'}} @endif">
                      <a href="{{url('system')}}"><i class="fa fa-list-alt"></i> Tablas</a>
                    </li>
                    <li class="@if($selects=='rubros') {{'active'}} @endif">
                      <a href="{{url('items')}}"><i class="fa fa-ticket"></i> Rubros</a>
                    </li>
                    
                    <li class="@if($selects=='clientes1') {{'active'}} @endif">
                      <a href="#"><i class="fa fa-desktop"></i> Kits de Mantenimiento</a>
                    </li>
                    <li class="@if($selects=='clientes2') {{'active'}} @endif">
                      <a href="#"><i class="fa fa-book"></i> Talonarios de documentos</a>
                    </li>
                    <li class="@if($selects=='parametros') {{'active'}} @endif">
                      <a href="{{url('company_parametros')}}"><i class="fa fa-cog"></i> Configuración del sistema</a>
                    </li>
                  </ul>
                </li>

                <li class="@if($options=='inventory') {{'open'}} @endif">
                  <a href="#opt2" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-sitemap"></i>
                    <span>Gestión de Almcns.</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt2" class="collapse @if($options=='inventory') {{'in'}} @endif" >
                    <li class="@if($selects=='EntryInventoryList') {{'active'}} @endif" >
                      <a  href="{{url('entryinventory')}}"><i class="fa fa-plus-circle fa-lg"></i> Ingresos y Salidas de Almacén</a>
                    </li>
                    <li>
                      <a href="#">Transferencias</a>
                    </li>
                    <li >
                      <a><i class="fa fa-truck fa-lg"></i> Inventarios</a>
                    </li>
                    <li>
                      <a href="#">Salidas pendientes por Orden de Trabajo</a>
                    </li>
                  </ul>
                </li>

                <li onclick="return false;">
                  <a href="#opt3" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Gestión de Comprs.</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt3" class="collapse">
                    <li>
                      <a href="#">Emisión de Órdenes de Compra</a>
                    </li>
                    <li>
                      <a href="#">Seguimiento de Órdenes de Compra</a>
                    </li>
                  </ul>
                </li>

                <li>
                  <a href="#opt4" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>Gestión de Ordns.</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt4" class="collapse">
                    <li class="@if($selects=='example') {{'active'}} @endif">
                      <a href="#">Registro de Citas</a>
                    </li>
                    <li class="@if($selects=='example') {{'active'}} @endif">
                      <a href="#">Cotización de servicios</a>
                    </li>
                    <li class="@if($selects=='example') {{'active'}} @endif">
                      <a href="#">Registro de Ordenes</a>
                    </li>
                    <li class="@if($selects=='example') {{'active'}} @endif">
                      <a href="#">Actualización de Ordenes</a>
                    </li>
                  </ul>
                </li>

                <li class="@if(Request::is('lcomprobante','sfactura','sboleta','sncredito','sndevito')) {{'open'}} @else {{''}} @endif">
                  <a href="#menu-comprobante" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <!-- icon--><i class="fa fa-folder-open"></i>
                    <span>Comprobantes</span>
                    <!-- arrow--><i class="fa arrow"></i>
                  </a>
                  <!-- BEGIN SUB-ELEMENT MENU-->
                  <ul id="menu-comprobante" class="collapse @if(Request::is('lcomprobante','sfactura','sboleta','sncredito','sndevito')) {{'in'}} @else {{''}} @endif">
                    <li class="@if(Request::is('lcomprobante')) {{'active'}} @else {{''}} @endif">
                      <a href="{{url('lcomprobante')}}"><i class="fa fa-list-alt"></i> Lista</a>
                    </li>
                    <li class="@if(Request::is('sfactura')) {{'active'}} @else {{''}} @endif">
                      <a href="{{url('sfactura')}}"><i class="fa fa-plus-square"></i> Factura</a>
                    </li>
                    <li class="@if(Request::is('sboleta')) {{'active'}} @else {{''}} @endif">
                      <a href="{{url('sboleta')}}"><i class="fa fa-plus-square"></i> Boleta</a>
                    </li>
                    <li class="@if(Request::is('sncredito')) {{'active'}} @else {{''}} @endif">
                      <a href="{{url('sncredito')}}"><i class="fa fa-plus-square"></i> Nota de Crédito</a>
                    </li>
                    <li class="@if(Request::is('sndevito')) {{'active'}} @else {{''}} @endif">
                      <a href="{{url('sndevito')}}"><i class="fa fa-plus-square"></i> Nota de Débito</a>
                    </li>
                  </ul>
                  <!-- END SUB-ELEMENT MENU-->            
                </li> 

                <li class="@if($options=='caja') {{'open'}} @endif">
                  <a href="#opt6" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-money"></i>
                    <span>Gestión de Caja</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt6" class="collapse @if($options=='caja') {{'in'}} @endif" >
                    <li class="@if($selects=='example1') {{'active'}} @endif">
                      <a href="#">Apertura diaria de Caja</a>
                    </li>
                     <li class="@if($selects=='example1') {{'active'}} @endif">
                      <a href="#">Registro de pagos de documentos</a>
                    </li>
                     <li class="@if($selects=='example1') {{'active'}} @endif">
                      <a href="#">Registro de Caja chica</a>
                    </li>
                     <li class="@if($selects=='cierre_caja') {{'active'}} @endif">
                      <a href="{{url('cierre_caja')}}">Cierre diario de Caja</a>
                    </li>
                  </ul>
                </li>

                <li>
                  <a href="#opt7" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-file-text"></i>
                    <span>Informes y Consultas</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt7" class="collapse">
                    <li>
                      <a href="#">Registro de Ventas</a>
                    </li>
                    <li>
                      <a href="#">Registro de Compras</a>
                    </li>
                    <li>
                      <a href="#">Kardex de Almacén</a>
                    </li>
                    <li>
                      <a href="#">Bincard de productos</a>
                    </li>
                    <li>
                      <a href="#">Detalle de Ordenes de Trabajo</a>
                    </li>
                    <li>
                      <a href="#">Historial por vehículo</a>
                    </li>
                  </ul>
                </li>

                <li class="@if($options=='usuario') {{'open'}} @endif">
                  <a href="#opt8" data-toggle="collapse" data-parent="#social-sidebar-menu">
                    <i class="fa fa-key"></i>
                    <span>Seguridad</span>
                    <i class="fa arrow"></i>
                  </a>
                  <ul id="opt8" class="collapse @if($options=='usuario') {{'in'}} @endif">
                    <li class="@if($selects=='usuarios') {{'active'}} @endif">
                      <a href="{{url('user')}}"> <i class="fa fa-users fa-lg"></i> Usuarios</a>
                    </li>
                    <li class="@if($selects=='profile') {{'active'}} @endif">
                      <a href="{{url('profile_user')}}"> <i class="fa fa-lock fa-lg"></i> Cambiar contraseña</a>
                    </li>
                  </ul>
                </li>
                
                <!-- END ELEMENT MENU-->
              </ul>
            </div>
          </div>
          <!-- END MENU SECTION-->
        </div>
        <!-- BEGIN CHAT SECTION-->
        <div class="chat visible-lg visible-md">
          <ul class="users-list">
            <li>
              <a data-firstname="Cesar" data-lastname="Mendoza" data-status="online" data-userid="1" href="#ignore">
                <img src="/template/img/avatars/user2_22.jpg" alt="User">
                <span>Cesar Mendoza</span><i class="fa fa-circle user-status online"></i>
              </a>
            </li>
            <li>
              <a data-firstname="Yadra" data-lastname="Abels" data-status="offline" data-userid="2" href="#ignore">
                <img src="/template/img/avatars/user1_22.jpg" alt="User">
                <span>Yadra Abels</span><i class="fa fa-circle user-status offline"></i>
              </a>
            </li>
            <li>
              <a data-firstname="Tobei" data-lastname="Tsumura" data-status="online" data-userid="3" href="#ignore">
                <img src="/template/img/avatars/user4_22.jpg" alt="User">
                <span>Tobei Tsumura</span><i class="fa fa-circle user-status online"></i>
              </a>
            </li>
            <li>
              <a data-firstname="John" data-lastname="Doe" data-status="offline" data-userid="4" href="#ignore">
                <img src="/template/img/avatars/user3_22.jpg" alt="User">
                <span>John Doe</span><i class="fa fa-circle user-status offline"></i>
              </a>
            </li>
          </ul>
          <form class="chat-options">
            <div class="input-group">
              <div class="input-group-btn dropup">
                <button type="button" tabindex="-1" data-toggle="dropdown" class="btn dropdown-toggle btn-xs"><i class="fa fa-cog"></i>
                </button>
                <ul role="menu" class="dropdown-menu pull-left">
                  <li>
                    <a href="#">Chat Sounds</a>
                  </li>
                  <li>
                    <a href="#">Advanced Settings...</a>
                  </li>
                  <li class="divider"></li>
                  <li>
                    <a href="#">Turn Off Chat</a>
                  </li>
                </ul>
              </div>
              <input type="text" placeholder="Search user..." class="form-control">
            </div>
          </form>
        </div>
        <!-- END CHAT SECTION-->
      </aside>