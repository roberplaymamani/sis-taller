<!DOCTYPE html>
<html>
@include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
        @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
        @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>

      <div class="main">
        <!-- BEGIN CONTENT-->
        @yield('content')
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    @include('layouts.scripts')
    <!-- BEGIN CURRENT PAGE SCRIPTS-->    
    @stack('js')
    <!-- END CURRENT PAGE SCRIPTS-->
  </body>
</html>