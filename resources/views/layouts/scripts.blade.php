  <!-- END THEME SWITCHER-->
    <!-- jQuery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
      window.jQuery || document.write('<script src="/template/js/jquery/jquery.min.js"><\/script>')
    </script>
    <!-- Bootstrap JS-->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script>
      $.fn.modal || document.write('<script src="/template/js/plugins/bootstrap/bootstrap.min.js"><\/script>')
       // Prevent jQueryUI Conflicts
       var bootstrapTooltip = $.fn.tooltip.noConflict()
       $.fn.bootstrapTooltip = bootstrapTooltip
    </script>
    <!-- jQueryUI-->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
      window.jQuery.ui || document.write('<script src="/template/js/jquery-ui/jquery-ui.min.js"><\/script>')
    </script>
    <!-- Bootstrap Hover Dropdown-->
    <script src="/template/js/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <!-- jQuery slimScroll-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.1/jquery.slimscroll.min.js"></script>
    <script>
      window.jQuery.ui || document.write('<script src="/template/js/plugins/jquery.slimscroll/jquery.slimscroll.min.js"><\/script>')
    </script>
    <!-- BEGIN THEME SWITCHER SCRIPTS-->
    <script>
      var assets_dir = '/template/'
    </script>
    <script src="/template/js/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
    <script src="/template/js/demo/theme-switcher-admin.js"></script>
    <!-- END THEME SWITCHER SCRIPTS-->
    <script src="/template/js/sidebar.js"></script>
    <script src="/template/js/panels.js"></script>
    <!-- BEGIN GENERAL SCRIPTS-->
    <script>
      /*<![CDATA[*/
      $(function() {
        $(".social-sidebar").socialSidebar();
        $('.main').panels();
        $(".main a[href='#ignore']").click(function(e) {
          e.stopPropagation()
        });
      });
      $(document).on('click', '.navbar-super .navbar-super-fw', function(e) {
        e.stopPropagation()
      });
      /*]]>*/
    </script>
    <!--<script src="/template/js/plugins/datatables/jquery.dataTables.min.js?v={{ time() }}"></script>
    <script src="/template/js/demo/dataTables.bootstrap.js?v={{ time() }}"></script>
     <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  -->

    <script src="/bower_components/datatables.net/js/jquery.dataTables.js?v={{ time() }}"></script>
   
     <script src="/template/js/demo/dataTables.bootstrap.js?v={{ time() }}"></script>

    <!-- END GENERAL SCRIPTS-->
    <!-- BEGIN CURRENT PAGE SCRIPTS
    <script src="/template/js/plugins/flot/jquery.flot.js"></script>
    <script src="/template/js/plugins/flot/jquery.flot.selection.js"></script>
    <script src="/template/js/plugins/jqvmap/jquery.vmap.js"></script>
    <script src="/template/js/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
    <script src="/template/js/plugins/jqvmap/data/jquery.vmap.sampledata.js"></script>
    <script src="/template/js/plugins/easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="/template/js/plugins/jquery.sparkline/jquery.sparkline.min.js"></script>
    <script src="/template/js/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="/template/js/plugins/justgage/lib/raphael.2.1.0.min.js"></script>
    <script src="/template/js/plugins/justgage/justgage.js"></script>
    <script src="//maps.google.com/maps/api/js?sensor=true"></script>
    <script src="/template/js/plugins/gmaps/gmaps.js"></script>
    <script src="/template/js/plugins/pnotify/pnotify.custom.min.js"></script>
    <script src="/template/js/demo/dashboard.js"></script>-->
    <!-- END CURRENT PAGE SCRIPTS-->
    <script src="/template/js/demo/ui-elements.js"></script>
    <script>
      $(function() {
        UIElements.initPanelsItems();
      });
    </script>