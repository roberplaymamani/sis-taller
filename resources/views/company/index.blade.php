<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Empresa</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Empresa y parametros</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Empresa y parametros</h3>
                </div>
                <div class="panel-body">
                  <div class="row">




                    <div class="col-md-12">
                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Datos de empresa</h3>
                        </div>
                        <div class="panel-body">
                          <!--Notice .user-profile class-->
                          @foreach($dataCompany as $valueCompany)
                          <div class="user-profile">
                            <div class="row">
                              <div class="col-sm-2 col-md-2">
                                  
                                  <div class="row">
                                    <div class="col-md-12 text-center">
                                      @if($valueCompany->logoEmpresa=='')
                                         @php($logo='/template/img/admin.png')
                                      @else
                                          @php($logo='/img/company/'.$valueCompany->logoEmpresa)
                                      @endif 
                                      <img id="LogoEmpresImg" src="{{$logo}}" alt="Avatar" class="img-thumbnail img-responsive"/>
                                    </div>
                                  </div>
                                  <div>
                                    <a class="btn btn-block btn-success"><i class="fa fa-envelope-alt"></i><b>Logo Empresa</b></a>
                                  </div>
                                <br>
                                <!-- BEGIN SOCIAL ICONS-->
                                <div class="text-center social-icons">
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-facebook"></i>
                                    </span>
                                  </a>
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-twitter"></i>
                                    </span>
                                  </a>
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-google-plus"></i>
                                    </span>
                                  </a>
                                </div>
                                <!-- END SOCIAL ICONS-->
                              </div>

                              <div class="col-sm-10 col-md-10">
                                 <form id="frm_add_updateCompany" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data">
                                  @csrf
                                <div class="row">
                                  <!-- BEGIN USER STATUS-->
                                  <div id="user-status" class="text-left col-sm-10 col-md-10">
                                    <h3 id="empresaadd">{{$valueCompany->rucEmpresa}} - {{$valueCompany->razonsocialEmpresa}}</h3>
                                    <h5 id="direccionadd">{{$valueCompany->direccionEmpresa}}</h5>
                                  </div>
                                  <!-- END USER STATUS-->
                                  <div class="col-sm-2 col-md-2 hidden-xs">
                                    <button type="submit" id="edit-profile-button" class="btn btn-block btn-primary ovalado5">
                                      <i class="fa fa-save fa-lg"></i> Guardar Cambios
                                    </button>
                                  </div>
                                </div>



                     

                                 <div class="row">
                                  <ul id="profileTab" class="nav nav-tabs">
                                    <li class="active" onclick="tab(1)">
                                      <a href="#info" data-toggle="tab"><span class="fa fa-info fa-lg"></span> Informacion de empresa</a>
                                    </li>
                                    <li class="" onclick="tab(2)">
                                      <a href="#parametros" data-toggle="tab"><span class="fa fa-cog fa-lg"></span>  Parametros</a>
                                    </li>
                                    <li class="" onclick="tab(3)"> 
                                      <a href="#soap" data-toggle="tab"><span class="fa fa-tags fa-lg"></span>  Config. Soap SUNAT</a>
                                    </li>
                                  </ul>
                                </div>
                                <!-- END TABS SELECTIONS-->

                                <div class="row">
                                  <!-- BEGIN TABS SECTIONS-->
                                  <div id="profileTabContent" class="tab-content col-sm-9 col-md-9">
                                    <div id="info" class="tab-pane active"  >
                                      <br>
                                          <div class="panel-body">
                                            <fieldset>
                                              <!-- Text input-->
                                              <input type="hidden" name="id" name="id" value="{{$valueCompany->id}}">
                                              <div class="form-group">
                                                <label for="firstname" class="col-md-2 control-label">Numero de R.U.C:</label>
                                                <div class="col-md-6">
                                                  <input id="rucEmpresa" name="rucEmpresa" type="text"  value="{{$valueCompany->rucEmpresa}}" placeholder="R.U.C" class="form-control input-md" autocomplete="off" >
                                                </div>
                                              </div>
                                              <!-- Text input-->
                                              <div class="form-group">
                                                <label for="lastname" class="col-md-2 control-label">Razon social:</label>
                                                <div class="col-md-6">
                                                  <input id="razonsocialEmpresa" name="razonsocialEmpresa" type="text" value="{{$valueCompany->razonsocialEmpresa}}"  placeholder="Escriba razon social" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                              <!-- Text input-->
                                              <div class="form-group">
                                                <label for="username" class="col-md-2 control-label">Direccion empresa:</label>
                                                <div class="col-md-6">
                                                  <input id="direccionEmpresa" name="direccionEmpresa" type="text" value="{{$valueCompany->direccionEmpresa}}"  placeholder="Escriba direccion" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                              <!-- Text input-->
                                              <div class="form-group">
                                                <label for="email" class="col-md-2 control-label">Telefono</label>
                                                <div class="col-md-6">
                                                  <input id="telefonoEmpresa" name="telefonoEmpresa" type="text" value="{{$valueCompany->telefonoEmpresa}}"  placeholder="Escriba telefono" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                                <!-- Text input-->
                                              <div class="form-group">
                                                <label for="email" class="col-md-2 control-label">Celular</label>
                                                <div class="col-md-6">
                                                  <input id="celularEmpresa" name="celularEmpresa" type="text" value="{{$valueCompany->celularEmpresa}}"  placeholder="Escriba celular" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                              <!-- Text input-->
                                              <div class="form-group">
                                                <label for="email" class="col-md-2 control-label">Correo</label>
                                                <div class="col-md-6">
                                                  <input id="emailEmpresa" name="emailEmpresa" type="text" value="{{$valueCompany->emailEmpresa}}"  placeholder="Escriba correo" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                              <!-- Text input-->
                                              <div class="form-group">
                                                <label for="website" class="col-md-2 control-label">Pagina web</label>
                                                <div class="col-md-6">
                                                  <input id="paginaWeb" name="paginaWeb" type="text" value="{{$valueCompany->paginaWeb}}"  placeholder="Escriab url web" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>

                                               <div class="form-group">
                                                <label for="website" class="col-md-2 control-label">Logo</label>
                                                <div class="col-md-6">
                                                  <input id="logoEmpresa" name="logoEmpresa" type="file"    accept=".png, .jpg, .jpeg" class="form-control input-md">
                                                  <input type="hidden" name="logoDefault" id="logoDefault" value="{{$valueCompany->logoEmpresa}}" >
                                                </div>
                                              </div>
                                            </fieldset>
                                          </div>
                                      </div>
                                    <div id="parametros" hidden="">
                                      <div class="panel-body">
                                        <fieldset>
                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Moneda:</label>
                                            <div class="col-md-6">
                                              <select id="codMoneda" name="codMoneda" class="form-control" aria-required="true" aria-invalid="false">
                                                <option value="">Seleccionar...</option>
                                                @foreach($codMonedas as $cm)
                                                  @if($cm->id==$valueCompany->codMoneda)
                                                    <option value="{{$cm->id}}" selected="">{{$cm->description}}</option>
                                                  @else
                                                    <option value="{{$cm->id}}">{{$cm->description}}</option>
                                                  @endif
                                                @endforeach
                                              </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Forma pago:</label>
                                            <div class="col-md-6">
                                              <select id="codFormapago" name="codFormapago" class="form-control" aria-required="true" aria-invalid="false">
                                                <option value="">Seleccionar...</option>
                                                @foreach($codFormapagos as $cp)
                                                  @if($cp->id==$valueCompany->codFormapago)
                                                    <option value="{{$cp->id}}" selected="">{{$cp->description}}</option>
                                                  @else
                                                    <option value="{{$cp->id}}">{{$cp->description}}</option>
                                                  @endif
                                                @endforeach
                                              </select>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Almacen de ventas:</label>
                                            <div class="col-md-6">
                                              <select id="codAlmacen" name="codAlmacen" class="form-control" aria-required="true" aria-invalid="false">
                                                <option value="">Seleccionar...</option>
                                                @foreach($codCondicions as $cc)
                                                  @if($cc->id==$valueCompany->codCondicion)
                                                    <option value="{{$cc->id}}" selected="">{{$cc->description}}</option>
                                                  @else
                                                    <option value="{{$cc->id}}">{{$cc->description}}</option>
                                                  @endif
                                                @endforeach
                                              </select>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">IGV %:</label>
                                            <div class="col-md-2">
                                             <input id="IGV" name="IGV" type="text"  value="{{$valueCompany->IGV}}" class="form-control input-md" autocomplete="off" placeholder="Ingrese % IGV." /> 
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Ganancias %:</label>
                                            <div class="col-md-2">
                                              <input id="Ganancia" name="Ganancia" type="text" value="{{$valueCompany->Ganancia}}"  class="form-control input-md" autocomplete="off" placeholder="Ingrese % de ganacia." />
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Condicion de venta:</label>
                                            <div class="col-md-6">
                                              <select id="codCondicion" name="codCondicion" class="form-control" aria-required="true" aria-invalid="false">
                                                <option value="">Seleccionar...</option>
                                                @foreach($codAlmacens as $ca)
                                                  @if($ca->id==$valueCompany->codAlmacen)
                                                    <option value="{{$ca->id}}" selected="">{{$ca->description}}</option>
                                                  @else
                                                    <option value="{{$ca->id}}">{{$ca->description}}</option>
                                                  @endif
                                                @endforeach
                                              </select>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="firstname" class="col-md-2 control-label">Link SUNAT:</label>
                                            <div class="col-md-6">
                                              <input id="LinkSunat" name="LinkSunat" type="text" value="{{$valueCompany->LinkSunat}}"  class="form-control input-md" autocomplete="off" placeholder="Ingrese url SUNAT" />
                                            </div>
                                          </div>
                                        </fieldset>
                                      </div>
                                    </div>
                                    <div id="soap" hidden="" >
                                      <br>
                                          <div class="panel-body">
                                            <fieldset>
                                              <div class="form-group">
                                                <label for="firstname" class="col-md-2 control-label">Nombre user SOAP:</label>
                                                <div class="col-md-6">
                                                  <input id="firstname" name="firstname" type="text" placeholder="Escriba usuario soap" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="firstname" class="col-md-2 control-label">Clave SOAP:</label>
                                                <div class="col-md-6">
                                                  <input id="firstname" name="firstname" type="text"  class="form-control input-md" autocomplete="off" placeholder="**********************" />
                                                </div>
                                              </div>

                                              <div class="form-group">
                                                <label for="firstname" class="col-md-2 control-label">Certificado Soap:</label>
                                                <div class="col-md-6">
                                                  <input id="firstname" name="firstname" type="file" accept=".cer" placeholder="Certificado" class="form-control input-md" autocomplete="off">
                                                </div>
                                              </div>

                                              <div class="form-group">
                                                <label for="firstname" class="col-md-2 control-label">Clave Certificado Soap:</label>
                                                <div class="col-md-6">
                                                  <input id="firstname" name="firstname" type="text"  class="form-control input-md" autocomplete="off" placeholder="**********************" />
                                                </div>
                                              </div>
                                            </fieldset>
                                          </div>
                                    </div>
                                  </div>
                                  <!-- END TABS SECTIONS-->
                                  <div id="user-links" class="col-sm-3 col-md-3" hidden="">
                                    <h4>Other Profiles</h4>
                                    <ul class="list-unstyled">
                                      <li>
                                        <a href="#"><i class="fa fa-github"></i>Gihub</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-pinterest"></i>Pinterest</a>
                                      </li>
                                    </ul>
                                    <h4>Recomended Links</h4>
                                    <ul class="list-unstyled">
                                      <li>
                                        <a href="#"><i class="fa fa-css3"></i>CS3 Documentation</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-hospital-o"></i>Local Hospital</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-html5"></i>HTML5 Documentation</a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                </form>


                              </div>
                            </div>
                          </div>
                          @endforeach

                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
 <!-- <script src="/js/user/user.js?v={{ time() }}"></script>-->
  <script src="/js/company/company.js?v={{ time() }}"></script>
</html>