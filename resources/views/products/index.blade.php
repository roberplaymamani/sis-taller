<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div id="content_sistema">
          <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Productos</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Mantenimiento de productos</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <div class="row">
                  <button type="button" class="btn btn-primary add add-products" id="btnAdd" style="margin-left: 5px;" onclick="AddproductoDIV(this.value)" value="divListaproducto">
                    <span class="fa fa-pencil"></span>&nbsp;Agregar producto
                  </button>
                  <a type="button" class="btn btn-success" style="margin-left: 5px;" href="{{url('products/listProducts/export')}}">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar Excel
                  </a>
                   <button type="button" class="btn btn-danger" style="margin-left: 5px;" onclick="window.open('/downloadpdfproducts')">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar PDF
                  </button>
                  <input type="text" name="searchList" id="searchList" class="form-control pull-right" placeholder="Buscar" style="width: 200px; margin-right: 10px;">
                  </div>
                </div>
                <div class="panel-body" id="divListaproducto" >
                  <div class="table-responsive">
                    <table id="foo-table" class="table">
                      <thead>
                        <tr class="bg-primary">
                            <th>Descripcion</th>
                            <th>Codigo</th>
                            <th>Unidad</th>
                            <th>Linia</th>
                            <th>Sub Linia</th>
                            <th>Estado</th>
                            <th><center>Acción</center></th>
                        </tr>
                      </thead>
                      <tbody>
                          <tbody id="listPaginates">  
                              @foreach($listproductos as $valueProd)
                              <tr>
                                  <td>{{$valueProd->description}}</td> 
                                  <td>{{$valueProd->id_products}}</td>
                                  <td>{{$valueProd->measurement_units}}</td>
                                  <td>{{$valueProd->lines_product}}</td>
                                  <td>{{$valueProd->sublines_product}}</td>
                                  <td><span class="label label-success">{{$valueProd->status}}</span></td>
                                  <td>
                                      <center>
                                          <a href="#" rel="modal:open" data-id="{{$valueProd->id}}/{{$valueProd->description}}"  title="Editar" class="edit edit-products">
                                            <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
                                          </a>
                                           &nbsp;
                                          <a  href="#" id="delete-products" title="Eliminar" data-id="{{$valueProd->id}}/{{$valueProd->description}}" class="delete">
                                            <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
                                          </a>
                                      </center>
                                  </td>
                              </tr>
                              @endforeach
                            </tbody>
                      </tbody>
                    </table>
                  </div>
                  
                  <div class="row">
                     <div class="pull-left" id="mostraradd" style="margin-left: 15px;   margin-top: 20px;">
                        <select id="cant_mostrar" class="form-control" onchange="filterProducts();">
                          <option value="10">Paginacion de 10</option>
                          <option value="20">Paginacion de 20</option>
                          <option value="50">Paginacion de 50</option>
                          <option value="100">Paginacion de 100</option>
                        </select>
                    </div>
                    <div class="pull-right" id="paginates" style="margin-right: 10px;">
                        {{$listproductos->links()}}
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="divRegistrarproducto" hidden="">
                  <div id="wizard0" class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">FORMULARIO DE <label id="frmprod">REGISTRO</label> DE PRODUCTO</h3>
                    </div>
                    <form  id="products-submit" enctype="multipart/form-data">
                    <div class="panel-body">
                      <div>
                        <ul class="nav nav-tabs nav-tabs-primary nav-tabs-advanced ">
                          <li class="">
                            <a href="#tab0" data-toggle="tab"><i class="fa fa-laptop fa-lg"></i>&nbsp;</a>
                          </li>
                          <li class="active">
                            <a href="#tab10" data-toggle="tab"><i class="fa fa-keyboard-o fa-lg"></i>&nbsp;Datos generales</a>
                          </li>
                          <li>
                            <a href="#tab20" data-toggle="tab"><i class="fa fa-money fa-lg"></i>&nbsp;Costeo del Producto</a>
                          </li>
                          <li>
                            <a href="#tab30" data-toggle="tab"><i class="fa fa-info fa-lg"></i>&nbsp;Otros datos</a>
                          </li>
                        </ul>
                      </div>
                        <div class="progress">
                          <div role="progressbar" class="progress-bar progress-bar-success" style="width: 33.3333%;"></div>
                        </div>
                        <div class="tab-content">
                          <div id="tab10" class="tab-pane active">
                            <!-- Text input-->
                            <input type="hidden" name="Productsid" id="Productsid">
                            <input type="hidden" name="ProductsiamgeAct" id="ProductsiamgeAct">
                            <div class="form-group has-error col-md-4">
                              <label for="inputWarning1" class="control-label">Código:</label>
                              <input type="text" class="form-control mayusc" name="Productscode" id="Productscode" placeholder="Ingresar código" required> 
                            </div>

                            <div class="form-group has-error col-md-4">
                              <label for="inputWarning1" class="control-label">Descripción:</label>
                              <input type="text" class="form-control mayusc" name="Productsdescription" id="Productsdescription" placeholder="Ingresar descripción" required>  
                            </div>

                            <div class="form-group has-error col-md-4">
                              <label for="inputWarning1" class="control-label">Abreviación:</label>
                              <input type="text" class="form-control mayusc" name="Productsabreviation" id="Productsabreviation" placeholder="Ingresar abreviación" required>
                            </div>

                            <div class="form-group has-success col-md-4">
                              <label for="inputWarning1" class="control-label">Linea:</label>
                              <select class="form-control mayusc" name="Productsid_lines" id="Productsid_lines" onchange="sublines('Productsid_sublines','Productsid_lines');"></select>
                            </div>

                            <div class="form-group has-success col-md-4">
                              <label for="inputWarning1" class="control-label">Sub Linea:</label>
                              <select class="form-control mayusc" name="Productsid_sublines" id="Productsid_sublines"></select> 
                            </div>

                            <div class="form-group has-success col-md-4">
                              <label for="inputWarning1" class="control-label">Unidad de Medida:</label>
                              <select class="form-control mayusc" name="Productsid_measureunit" id="Productsid_measureunit"></select>
                            </div>

                            <div class="form-group has-success col-md-4">
                              <label for="inputWarning1" class="control-label">Marca de Respuesto:</label>
                              <select class="form-control mayusc" name="Productsid_marks" id="Productsid_marks"></select> 
                            </div>
                            <!-- Password input-->
                            
                          </div>
                          <div id="tab20" class="tab-pane">
                            <!-- Text input-->
                             <div class="form-group has-success col-md-4">
                                <label for="inputWarning1" class="control-label">Moneda:</label>
                                 <select class="form-control mayusc" name="Productsid_coins" id="Productsid_coins" onchange="coins('textsymbol','Productsid_coins');"></select>
                              </div>

                              <div class="form-group has-success col-md-4">
                                <label for="inputWarning1" class="control-label">Costo:</label>   <label class='textsymbol fixed-input-d'></label> 
                                <input type="text" class="form-control mayusc fixed-input-a" name="Productscost" id="Productscost" placeholder="0.00" onkeyup="format(this);prices('P')" onchange="format(this);prices('P')">
                                       
                              </div>

                              <div class="form-group has-success col-md-4">
                                <div class="col-md-6">
                                   <label for="inputWarning1" class="control-label">Ganancia:</label>  <label class='textsymbol fixed-input-d'></label>
                                  <input type="text" class="form-control mayusc fixed-input-a" name="Productsgain" id="Productsgain" placeholder="0.00" onkeyup="format(this);prices('G')" onchange="format(this);prices('G')">
                                   
                                </div>
                                <div class="col-md-6">
                                   <label for="inputWarning1" class="control-label"> </label> <label class='textporcent fixed-input-p'></label> 
                                   <input type="text" class="form-control mayusc fixed-input" name="Productsgainporc" id="Productsgainporc" placeholder="0" onkeyup="prices('GP')" onchange="prices('GP')">
                                        
                                </div>
                              </div>

                              <div class="form-group has-success col-md-4">
                                <div class="col-md-6">
                                  <label for="inputWarning1" class="control-label">Impuestos:</label>  <label class='textsymbol fixed-input-d'></label>
                                  <input type="text" class="form-control mayusc fixed-input-a" name="Productstax" id="Productstax" placeholder="0.00" onkeyup="format(this);prices('I')" onchange="format(this);prices('I')">
                                 
                                </div>
                                <div class="col-md-6">
                                   <label for="inputWarning1" class="control-label"></label> <label class='textporcent fixed-input-p'></label>
                                    <input type="text" class="form-control mayusc fixed-input" name="Productstaxporc" id="Productstaxporc" placeholder="0" onkeyup="prices('IP')" onchange="prices('IP')">
                                    
                                </div>    
                              </div>
                              <div class="form-group has-success col-md-4">
                                <label for="inputWarning1" class="control-label">P.V.P:</label>  <label class='textsymbol fixed-input-d'></label>
                                <input type="text" class="form-control mayusc fixed-input-a ghs" name="ProductsPVP" id="ProductsPVP" placeholder="0.00" onkeyup="format(this)" onchange="format(this)" readonly>
                                         
                              </div>
                              <!--<div class="form-group has-success col-md-4">
                                <div id="datetimepicker2" class="input-group ">
                                  <input type="text" class="form-control">
                                  <span class="input-group-addon">
                                    
                                  </span>
                                </div>
                              </div>-->
                              <div class="form-group has-success col-md-4">
                                <label for="inputWarning1" class="control-label">Proveedor:</label>
                                <select class="form-control mayusc" name="Productsid_provider" id="Productsid_provider"></select>  
                              </div>
                              <div class="form-group has-success col-md-4">
                                <div class="col-md-6">
                                   <label for="inputWarning1" class="control-label">Última Compra:</label>  <label class='textsymbol fixed-input-l'></label>
                                   <input type="text" class="form-control mayusc ghs" name="Productsdate_purchase" id="Productsdate_purchase" 
                                    value="{{date('Y-m-d')}}"  readonly>
                                        
                                </div>
                                <div class="col-md-6">
                                  <label for="inputWarning1" class="control-label">.</label>
                                  <input type="text" class="form-control mayusc fixed-input-s ghs" name="Productsprice_purchasec" id="Productsprice_purchasec" placeholder="0.00" onkeyup="format(this)" onchange="format(this)" readonly>
                                </div> 
                              </div>

                            <!-- Text input-->
                          </div>
                          <div id="tab30" class="tab-pane">
                            <!-- Text input-->
                            <div class="form-group has-success col-md-4">
                                <label for="categoria">País de Procedencia<label style="color:red">&nbsp;</label></label>
                                <select class="form-control mayusc" name="Productsid_country" id="Productsid_country"></select>   
                            </div>

                            <div class="form-group has-success col-md-4">
                                <label for="categoria">Meses de Garantía<label style="color:red">&nbsp;</label></label>
                                <input type="number" class="form-control mayusc" name="Productswarranty" id="Productswarranty" placeholder="Ingresar Meses">   
                            </div>
                            
                            <div class="form-group has-success col-md-4">
                                <label for="categoria">Estatus<label style="color:red">*</label></label>
                                <select class="form-control mayusc" name="Productsstatus" id="Productsstatus" required></select>   
                            </div>

                             <div class="form-group has-success col-md-4">
                              
                              <div id="preview" class="thumbnail">
                                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=" id="selectImagen" style=" width: 245px;"/>
                              </div>
                              <input id="file" name="file" type="file" class="form-control" />
                            </div>

                           
                          </div>
                        </div>
                      
                    </div>
                    <div class="panel-footer">
                      <div style="float:right">
                        <input type="button" name="next" value="Siguiente" class="btn btn-info button-next">
                        <input type="button" name="last" value="Ultimo" class="btn btn-primary button-last">
              
                        <!--<button type="submit" name="previous" class="btn btn-success button-finish" id="btn-products" value="Registrar" style="display: none;">
                          <li class="fa fa-save"></li> Registrar
                        </button >
                        getClearProducts-->
                         <button  type="submit" class="btn btn-success button-finish"  id="btn-products" >
                          <li class="fa fa-save"></li> Registrar reg.
                        </button>
                        <button type="button"  class="btn btn-danger button-finish" onclick="getClearProducts();">
                          <li class="fa fa-close"></li> Cancelar reg.
                        </button>
                        
                      </div>
                      <div style="float:left">
                        <input type="button" name="first" value="Primero" class="btn btn-primary button-first">
                        <input type="button" name="previous" value="Anterior" class="btn btn-info button-previous disabled">
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    </form>
                  </div>
                </div>




  














              
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    @include('layouts.scripts')
    <script src="/template/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
    <script>
      $(function() {
        $('#wizard0, #wizard1, #wizard2').bootstrapWizard({
          tabClass: 'nav nav-tabs',
          onTabShow: function(tab, navigation, index) {
            var $elem = navigation.closest('.panel');
            var $total = navigation.find("li[class!='title']").length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $elem.find('.progress-bar').css({
              width: $percent + '%'
            });
            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
              $elem.find('.panel-footer .button-next').hide();
              $elem.find('.panel-footer .button-last').hide();
              $elem.find('.panel-footer .button-finish').show();
              $elem.find('.panel-footer .button-finish').removeClass('disabled');
            } else {
              $elem.find('.panel-footer .button-next').show();
              $elem.find('.panel-footer .button-last').show();
              $elem.find('.panel-footer .button-finish').hide();
            }
            /*$elem.find('.button-finish').click(function() {
              $('#myModal').modal("show");
            });*/
          },
          'nextSelector': '.button-next',
          'previousSelector': '.button-previous',
          'firstSelector': '.button-first',
          'lastSelector': '.button-last'
        });
      })
    </script>
    <script src="/js/common/common.js?v=1565450320"></script>
    <script src="/js/product/product.js?v={{ time() }}"></script> 
  </body>
</html>