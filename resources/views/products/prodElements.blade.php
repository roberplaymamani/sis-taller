@foreach($listproductos as $valueProd)
<tr>
  <td>{{$valueProd->description}}</td> 
  <td>{{$valueProd->id_products}}</td>
  <td>{{$valueProd->measurement_units}}</td>
  <td>{{$valueProd->lines_product}}</td>
  <td>{{$valueProd->sublines_product}}</td>
  <td><span class="label label-success">{{$valueProd->status}}</span></td>
  <td>
      <center>
           <a href="#" rel="modal:open" data-id="{{$valueProd->id}}/{{$valueProd->description}}"  title="Editar" class="edit edit-products">
              <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
            </a>
             &nbsp;
            <a  href="#" id="delete-products" title="Eliminar" data-id="{{$valueProd->id}}/{{$valueProd->description}}" class="delete">
              <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
            </a>
      </center>
  </td>
</tr>
@endforeach