<style type="text/css">
  * {
    margin: 10px; 
    padding: 0px; 
    box-sizing: border-box;
  }
  table {
    border-spacing: 1;
    border-collapse: collapse;
    background: white;
    border-radius: 10px;
    overflow: hidden;
    width: 100%;
    margin: 0 auto;
    position: relative;
  }
  table * {
    position: relative;
  }
  table td, table th {
    padding-left: 8px;
  }
  table thead tr {
    height: 60px;
    background: #647aab;
  }
  table tbody tr {
    height: 50px;
  }
  table tbody tr:last-child {
    border: 0;
  }
  table td, table th {
    text-align: left;
  }
  table td.l, table th.l {
    text-align: right;
  }
  table td.c, table th.c {
    text-align: center;
  }
  table td.r, table th.r {
    text-align: center;
  }
  th{
    font-family: OpenSans-Regular;
    font-size: 18px;
    color: #fff;
    line-height: 1.5;
    font-weight: bold;
  }
  tbody tr:nth-child(even) {
    background-color: #f5f5f5;
  }
  tbody tr {
    font-family: OpenSans-Regular;
    font-size: 15px;
    line-height: 2;
    font-weight: bold;
  }

  tbody tr:hover {
    color: #555555;
    background-color: #f5f5f5;
    cursor: pointer;
  }
}


  </style>
<!--===============================================================================================-->
  <center>
    <h4><b><i><u>"Sistema de gestion de talleres automotrices"</u></i></b></h4>
  </center>
  <table>
    <td width="50%">
      <div>
         <table style="" >
          <tr style="padding-bottom: 20px;"> 
            <th style="color: #566d9f;">EMPRESA: </th>
            <td>{{$DataEmp['name']}}</td>
          </tr>
          <tr >
            <th style="color: #566d9f; background: white;">RUC: </th>
            <td style="background: white;">{{$DataEmp['ruc']}}</td>
          </tr>
           <tr >
            <th style="color: #566d9f;">FECHA: </th>
            <td >{{date('Y-m-d')}}</td>
          </tr>
        </table>
      </div>
    </td>
    <td width="50%" style="text-align: right;">
      <img src="data:image/jpeg;base64,{{$DataEmp['logo']}}" class="company_logo"  width="150" alt="Company Logo" class="logo" style="margin-right: 50px;">
    </td>
  </table>
  <hr>
  <center>
    <h4>LISTA DE GENERAL DE PRODUCTOS</h4>
  </center>
  <table>
    <thead>
      <tr >
        <th>No</th>
          <th>Descripcion</th>
          <th>Codigo</th>
          <th>Unidad</th>
          <th>Linia</th>
          <th>Sub Linia</th>
          <th>Estado</th>
      </tr>
    </thead>
    <tbody>
      @php($No=1)
      @foreach($listproductos as $valuelist)
      <tr style="font-size: 8;">
        <td>{{$No++}}</td>
        <td>{{$valuelist->description}}</td> 
        <td>{{$valuelist->id_products}}</td>
        <td>{{$valuelist->measurement_units}}</td>
        <td>{{$valuelist->lines_product}}</td>
        <td>{{$valuelist->sublines_product}}</td>
        <td>
          @if($valuelist->status=='Activo')
          <b><font style="color: green">{{$valuelist->status}}</font></b>
          @else
          <b><font style="color: red">{{$valuelist->status}}</font></b>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>