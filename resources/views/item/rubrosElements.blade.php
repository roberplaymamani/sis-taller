@php($item=1)
@foreach($listRubros as $valueRubros)
<tr>
  <td>{{$valueRubros->items}}</td>
  <td>{{$valueRubros->description}}</td>
  <td>{{$valueRubros->abreviation}}</td>
  <td>{{$valueRubros->symbol}}</td>
  <td>{{$valueRubros->price}}</td>
  <td>
    @if($valueRubros->status=='Activo')
      <span class="label label-success">{{$valueRubros->status}}</span>
    @else
      <span class="label label-danger">{{$valueRubros->status}}</span>
    @endif
  </td>
  <td>
    <center>
      <a data-id="{{$valueRubros->id}}/{{$valueRubros->description}}" title="Editar" class="edit edit-items">
        <img src="img/edit.svg" width="25px" height="25px">
      </a>
      &nbsp;
      <a href="javascript:void(0);"id="delete-items" data-toggle="tooltip" title="Eliminar" data-id="{{$valueRubros->id}}/{{$valueRubros->description}}" class="delete">
        <img src="img/delete.svg" width="35px" height="25px">
      </a>
    </center>
  </td>
</tr>
@endforeach