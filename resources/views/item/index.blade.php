<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Rubros</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Mantenimiento de rubros</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Mantenimiento de rubros</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    
                    <div class="col-md-9">
                      <div class="panel panel-default ovalado5">
                        <div class="panel-heading" style="margin: 0px;padding-top: 5px;padding-bottom: 5px;">
                          <div class="row" >
                             <div class="input-group pull-left" style="margin-left: 5px;">
                                <center><h3 class="panel-title" style="padding: 8px;" id="list_title">RUBROS</h3></center>
                             </div>
                            <div class="input-group pull-right" style="margin-right: 15px;">
                              <div class="form-group has-success">
                                  <input placeholder="Buscar..." type="text" class="form-control" id="searchList" name="searchList">
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="table-responsive">
                            <table id="listSystem12" class="table table-striped table-bordered table-hover table-responsive">
                              <thead>
                                <tr class="bg-primary">
                                  <th>Rubro</th>
                                  <th>Sub Rubro</th>
                                  <th>Abreviación</th>
                                  <th>Simbolo</th>
                                  <th>Precio</th>
                                  <th>Estado</th>
                                  <th><center>Acción</center></th>
                                </tr>
                              </thead>
                              <tbody id="listPaginates">
                                @php($item=1)
                                @foreach($listRubros as $valueRubros)
                                <tr>
                                  <td>{{$valueRubros->items}}</td>
                                  <td>{{$valueRubros->description}}</td>
                                  <td>{{$valueRubros->abreviation}}</td>
                                  <td>{{$valueRubros->symbol}}</td>
                                  <td>{{$valueRubros->price}}</td>
                                  <td>
                                    @if($valueRubros->status=='Activo')
                                      <span class="label label-success">{{$valueRubros->status}}</span>
                                    @else
                                      <span class="label label-danger">{{$valueRubros->status}}</span>
                                    @endif
                                  </td>
                                  <td>
                                    <center>
                                      <a data-id="{{$valueRubros->id}}/{{$valueRubros->description}}" title="Editar" class="edit edit-items">
                                        <img src="img/edit.svg" width="25px" height="25px">
                                      </a>
                                      &nbsp;
                                      <a href="javascript:void(0);"id="delete-items" data-toggle="tooltip" title="Eliminar" data-id="{{$valueRubros->id}}/{{$valueRubros->description}}" class="delete">
                                        <img src="img/delete.svg" width="35px" height="25px">
                                      </a>
                                    </center>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                                <div class="pull-left" id="mostraradd" style="margin-left: 15px;   margin-top: 20px;">
                                    <select id="cant_mostrar" class="form-control" onchange="filterRubros();">
                                      <option value="10">Paginacion de 10</option>
                                      <option value="20">Paginacion de 20</option>
                                      <option value="50">Paginacion de 50</option>
                                      <option value="100">Paginacion de 100</option>
                                    </select>
                                </div>
                                <div class="pull-right" id="paginates" style="margin-right: 10px;">
                                    {{$listRubros->links()}}
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="panel panel-success ovalado5">
                        <div class="panel-heading">
                          <center><h3 class="panel-title">FORMULARIO DE  <text id="tittle">REGISTRO</text></h3></center>
                        </div>
                        <div class="panel-body">
                          <form id="items-submit" enctype="multipart/form-data">
                            @csrf
                            <h4 class="box-title"> </h4>
                            <input type="hidden" name="Itemsid" id="Itemsid"/>
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Rubro:</label>
                                   <select class="form-control mayusc" name="Itemsidrubro" id="Itemsidrubro" required></select>
                                </div>
                            </div>
                            <div class="col-md-12">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Sub Rubro:</label>
                                  <input type="text" class="form-control mayusc" name="Itemsdescription" id="Itemsdescription" placeholder="Ingresar sub Rubro" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Abreviación:</label>
                                 <input type="text" class="form-control mayusc" name="Itemsabreviation" id="Itemsabreviation" placeholder="Ingresar abreviación" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Moneda:</label>
                                   <select class="form-control mayusc" name="Itemsidcoins" id="Itemsidcoins" required></select>   
                                </div>
                            </div>
                             <div class="col-md-12">
                                <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Precio:</label>
                                   <input type="text" class="form-control mayusc" name="Itemsprice" id="Itemsprice" placeholder="Ingresar precio" required onkeyup="format(this)" onchange="format(this)"> 
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Estado:</label>
                                    <select class="form-control mayusc" name="Itemsstatus" id="Itemsstatus" required></select> 
                                </div>
                            </div>
                            <br>
                            <label style="color:red">(*) Todos los campos son requeridos.</label>
                            <div class="col-md-12">
                                <center>
                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary margin5_ovalado" id="btn-items" value="add">
                                      <li class="fa fa-save"></li> Registrar
                                    </button>
                                  </div>
                                  <div class="col-md-6">
                                    <button type="button" class="btn btn-danger margin5_ovalado" onclick="getClearItems();">
                                      <li class="fa fa-close"></li> Cancelar
                                    </button>
                                  </div>
                                   <div class="col-md-12">
                                    <button type="reset" class="btn btn-success margin5_ovalado" onclick="location.href='items/listItems/export'">
                                      <li class="fa fa-file-text"></li> Exportar  Excel.
                                    </button>
                                  </div>
                                </center>
                            </div> 
                          </form>
                        </div>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
  <script src="/js/item/item.js?v={{ time() }}"></script>
  </body>
</html>