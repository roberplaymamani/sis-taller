<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Usuario</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Perfil usuario</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Perfil usuario</h3>
                </div>
                <div class="panel-body">
                  <div class="row">




                    <div class="col-md-12">
                      <div class="panel panel-primary">
                        <div class="panel-heading">
                          <h3 class="panel-title">Datos de usuario</h3>
                        </div>
                        <div class="panel-body">
                          <!--Notice .user-profile class-->
                          @foreach($ListUsers as $valueUsers)
                          <div class="user-profile">
                            <div class="row">
                              <div class="col-sm-2 col-md-2">
                                  
                                  <div class="row">
                                    <div class="col-md-12 text-center">
                                    @if($valueUsers->description=='Administrador')
                                        <img src="/template/img/admin.png" alt="Avatar" class="img-thumbnail img-responsive"/>
                                      @else
                                        <img src="/template/img/user.png" alt="Avatar" class="img-thumbnail img-responsive"/>
                                      @endif 
                                    </div>
                                  </div>
                                  <div>
                                    <a class="btn btn-block btn-success"><i class="fa fa-envelope-alt"></i><b>{{$valueUsers->description}}</b></a>
                                  </div>
                                <br>
                                <!-- BEGIN SOCIAL ICONS-->
                                <div class="text-center social-icons">
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-facebook"></i>
                                    </span>
                                  </a>
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-twitter"></i>
                                    </span>
                                  </a>
                                  <a href="#">
                                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-google-plus"></i>
                                    </span>
                                  </a>
                                </div>
                                <!-- END SOCIAL ICONS-->
                              </div>
                              <div class="col-sm-10 col-md-10">
                                <form id="frm_profile_users">
                                <div class="row">
                                  <!-- BEGIN USER STATUS-->
                                  <div id="user-status" class="text-left col-sm-10 col-md-10">
                                    <h3 id="nombrelabel">{{$valueUsers->name}}</h3>
                                    <h5>Geophysical Engineer, Company Inc., USA</h5>
                                  </div>
                                  <!-- END USER STATUS-->
                                  <div class="col-sm-2 col-md-2 hidden-xs">
                                    <button type="submit" id="edit-profile-button" class="btn btn-block btn-primary"><i class="fa fa-save fa-lg"></i> Guardar Informacion</button>
                                  </div>
                                </div>

                                 <div class="row">
                                  <ul id="profileTab" class="nav nav-tabs">
                                    <li class="active">
                                      <a href="#info" data-toggle="tab">Informacion personal</a>
                                    </li>
                                  </ul>
                                </div>
                                <!-- END TABS SELECTIONS-->
                                <div class="row">
                                  <!-- BEGIN TABS SECTIONS-->
                                  <div id="profileTabContent" class="tab-content col-sm-9 col-md-9">
                                    <div id="info" class="tab-pane active">
                                      <br>
                                      <dl class="dl-horizontal">
                                        <dt>Nombes y Apellidos:</dt>
                                        <dd><input type="text" class="formcontrol" name="nombreapel" id="nombreapel" value="{{$valueUsers->name}}" required="" ></dd>
                                        <dd class="divider"></dd>
                                        <br>
                                        <dt>Nombre Usuario</dt>
                                        <dd><input type="text" class="formcontrol" name="nombreusuario" value="{{$valueUsers->email}}" required=""></dd>
                                        <dd class="divider"></dd>
                                        <br>
                                        <dt>Perfil Usuario:</dt>
                                        <dd>{{$valueUsers->description}}</dd>
                                        <dd class="divider"></dd>
                                        <br>
                                        <dt>Clave usuario:</dt>
                                        <dd><input type="password" class="formcontrol" name="clavesuaurio" id="clavesuaurio" placeholder="*********************"></dd>
                                        <br>
                                         <dt>Ingrese clave nueva:</dt>
                                        <dd><input type="password" class="formcontrol" name="clavesuaurio1" id="clavesuaurio1" placeholder="*********************"></dd>
                                        <br>
                                        <dt>Repetir clave nueva:</dt>
                                        <dd><input type="password" class="formcontrol" name="clavesuaurio2" id="clavesuaurio2" placeholder="*********************"></dd>
                                        <dd class="divider"></dd>
                                        <br>
                                        <dt>Estado Usuario</dt>
                                        <dd>
                                          <span class="label label-success">{{$valueUsers->status}}</span>
                                        </dd>
                                      </dl>
                                    </div>
                                  </div>
                                  <!-- END TABS SECTIONS-->
                                  <div id="user-links" class="col-sm-3 col-md-3" hidden="">
                                    <h4>Other Profiles</h4>
                                    <ul class="list-unstyled">
                                      <li>
                                        <a href="#"><i class="fa fa-github"></i>Gihub</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-pinterest"></i>Pinterest</a>
                                      </li>
                                    </ul>
                                    <h4>Recomended Links</h4>
                                    <ul class="list-unstyled">
                                      <li>
                                        <a href="#"><i class="fa fa-css3"></i>CS3 Documentation</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-hospital-o"></i>Local Hospital</a>
                                      </li>
                                      <li>
                                        <a href="#"><i class="fa fa-html5"></i>HTML5 Documentation</a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </form>
                              </div>
                            </div>
                          </div>
                          @endforeach

                        </div>
                      </div>
                    </div>
                            


                    
                    <div class="col-md-4">
                      <table>
                        <tr>
                          <td>Administrador_ _: </td><td><img src="/template/img/admin.png" width="30"> </td>
                        </tr>
                        <tr>
                          <td> Usuario_ _ _ _ _ _: </td><td><img src="/template/img/user.png" width="30"></td>
                        </tr>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
  <script src="/js/user/user.js?v={{ time() }}"></script>
</html>