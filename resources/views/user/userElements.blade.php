 @php($item=1)
@foreach($ListUsers as $valueUsers)
<tr>
  <td hidden=""><span class="badge badge-primary">{{$item++}}</span></td>
  <td> 
    <center>
      @if($valueUsers->description=='Administrador')
      <img src="/template/img/admin.png" width="30">
      @else
      <img src="/template/img/user.png" width="30">
      @endif
    </center>
  </td>
  <td>{{$valueUsers->email}}</td>
  <td>{{$valueUsers->name}}</td>
  <td><span class="label label-success">{{$valueUsers->status}}</span></td>
  <td>
    <center>
        <a  data-id="{{$valueUsers->id}}/{{$valueUsers->name}}" title="Editar" class="edit edit-user">
          <img src="img/edit.svg" width="25px" height="25px">
        </a>
        &nbsp;
        <a href="javascript:void(0);"id="delete-user" data-toggle="tooltip" title="Eliminar" data-id="{{$valueUsers->id}}/{{$valueUsers->name}}" class="delete">
          <img src="img/delete.svg" width="35px" height="25px">
        </a>
    </center>
  </td>
</tr>
@endforeach 