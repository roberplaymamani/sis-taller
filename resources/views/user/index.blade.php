<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Usuarios</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Mantenimiento de usuarios</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Mantenimiento de usuarios</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    
                    <div class="col-md-8">
                      <div class="panel panel-default">
                        <div class="panel-heading" style="margin: 0px;padding-top: 5px;padding-bottom: 5px;">
                          <div class="row" >
                             <div class="input-group pull-left" style="margin-left: 5px;">
                                <center><h3 class="panel-title" style="padding: 8px;" id="list_title">USUARIOS DEL SISTEMA</h3></center>
                             </div>
                            <div class="input-group pull-right" style="margin-right: 15px;">
                              <div class="form-group has-success">
                                  <input placeholder="Buscar..." type="text" class="form-control" id="searchList" name="searchList">
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="table-responsive">
                            <table id="listSystem12" class="table table-striped table-bordered table-hover table-responsive">
                              <thead>
                                <tr class="bg-primary">
                                  <th hidden="">No</th>
                                  <th></th>
                                  <th>ID Usuario</th>
                                  <th>Nombre</th>
                                  
                                  <th>Estado</th>
                                  <th><center>Acción</center></th>
                                </tr>
                              </thead>
                              <tbody id="listPaginates">
                                @php($item=1)
                                @foreach($ListUsers as $valueUsers)
                                <tr>
                                  <td hidden=""><span class="badge badge-primary">{{$item++}}</span></td>
                                  <td> 
                                    <center>
                                      @if($valueUsers->description=='Administrador')
                                      <img src="/template/img/admin.png" width="30">
                                      @else
                                      <img src="/template/img/user.png" width="30">
                                      @endif
                                    </center>
                                  </td>
                                  <td>{{$valueUsers->email}}</td>
                                  <td>{{$valueUsers->name}}</td>
                                 
                                  <td><span class="label label-success">{{$valueUsers->status}}</span></td>
                                  <td>
                                    <center>
                                      <a  data-id="{{$valueUsers->id}}/{{$valueUsers->name}}" title="Editar" class="edit edit-user">
                                        <img src="img/edit.svg" width="25px" height="25px">
                                      </a>
                                      &nbsp;
                                      <a href="javascript:void(0);"id="delete-user" data-toggle="tooltip" title="Eliminar" data-id="{{$valueUsers->id}}/{{$valueUsers->name}}" class="delete">
                                        <img src="img/delete.svg" width="35px" height="25px">
                                      </a>
                                    </center>
                                  </td>
                                </tr>
                                @endforeach 
                              </tbody>
                            </table>
                                <div class="pull-right" id="paginates" style="margin-right: 10px;">
                                    {{$ListUsers->links()}}
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="panel panel-success">
                        <div class="panel-heading">
                          <center><h3 class="panel-title">FORMULARIO DE  <text id="tittle">REGISTRO</text></h3></center>
                        </div>
                        <div class="panel-body">
                          <form id="users-submit" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="Userid" id="Userid" value=""/>
                            <input type="hidden" name="UserpasswordAct" id="UserpasswordAct" value=""/>
                            
                            <div class="col-md-12">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Nombre:</label>
                                  <input name="Username" id="Username" type="text" class="form-control" placeholder="Ingrese nombre">
                                </div>
                            </div>
                            <div class="col-md-12">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Usuario:</label>
                                  <input name="Useruser" id="Useruser" type="text" class="form-control" placeholder="Ingrese usuario">
                                </div>
                            </div>
                            <div class="col-md-12">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Perfil:</label>
                                  <select class="form-control mayusc" name="Userprofile" id="Userprofile" required="">
                                    <option value="1">Administrador</option>
                                    <option value="2" selected="">Usuario</option>
                                  </select>
                                </div>
                            </div>

                             <div class="col-md-6">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Clave:</label>
                                  <input name="Userpassword" id="Userpassword" type="password" class="form-control" placeholder="******************">
                                </div>
                            </div>

                             <div class="col-md-6">
                               <div class="form-group has-error">
                                  <label for="inputSuccess1" class="control-label">Confirmar clave:</label>
                                  <input name="Userpassword2" id="Userpassword2" type="password" class="form-control" placeholder="******************">
                                </div>
                            </div>
                           
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Estado:</label>
                                   <select class="form-control mayusc" name="Userstatus" id="Userstatus" required="">
                                      <option value="1">Activo</option>
                                      <option value="2">bloqueado</option>
                                      <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <label style="color:red">(*) Todos los campos son requeridos.</label>
                            <div class="col-md-12">
                                <center>
                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary" id="btn-user" style="margin: 5px;" value="Registrar">
                                    <li class="fa fa-save"></li> Registrar us.
                                  </button>
                                  </div>
                                  <div class="col-md-6">
                                    <button type="button" class="btn btn-danger" style="margin: 5px;" onclick="getClearUsers();">
                                    <li class="fa fa-close"></li> Cancelar reg.
                                  </button>
                                  </div>
                                   <div class="col-md-12">
                                    <a type="reset" class="btn btn-success" style="margin: 5px;" href="{{url('user/listUser/export')}}">
                                    <li class="fa fa-file-text"></li> Exportar  Excel.
                                  </a>
                                  </div>
                                </center>
                            </div>
                          </form>
                        </div>
                      </div>
                      <br>
                    </div>
                    <div class="col-md-4">
                      <table>
                        <tr>
                          <td>Administrador_ _: </td><td><img src="/template/img/admin.png" width="30"> </td>
                        </tr>
                        <tr>
                          <td> Usuario_ _ _ _ _ _: </td><td><img src="/template/img/user.png" width="30"></td>
                        </tr>
                      </table>
                       
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
  <script src="/js/user/user.js?v={{ time() }}"></script>
</html>