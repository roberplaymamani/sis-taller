<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div id="content_sistema">
          <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Clientes</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Lista cliente</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <div class="row">
                  <button type="button" class="btn btn-primary" id="btnAdd" style="margin-left: 5px;" onclick="AddClienteDIV(this.value)" value="divListaCliente">
                    <span class="fa fa-plus-square fa-lg"></span>&nbsp;Agregar Cliente
                  </button>
                  <button type="button" class="btn btn-success" style="margin-left: 5px;"  onclick="window.open('/client/listClient/export')">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar EXCEL
                  </button>
                  <button type="button" class="btn btn-danger" style="margin-left: 5px;" onclick="window.open('/downloadpdfCliente/1')">
                    <span class="fa fa-file-o"></span>&nbsp;Exportar PDF
                  </button>
                  <input type="text" name="searchList" id="searchList" class="form-control pull-right" placeholder="Buscar" style="width: 200px; margin-right: 10px;">
                  </div>
                </div>
                <div class="panel-body" id="divListaCliente" >
                  <div class="table-responsive">
                    <table id="foo-table" class="table">
                      <thead>
                        <tr class="bg-primary">
                            <!--<th>Id</th>-->
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Documento</th>
                            <th>No. Telefono</th>
                            <th>No. Celular</th>
                            <th>Correo</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tbody id="listPaginates">    
                                  @foreach($listaClientes as $valueCl)
                                  <tr>
                                      <td>{{$valueCl->name}}</td>
                                      <td>{{$valueCl->type}}</td>
                                      <td>{{$valueCl->document}}</td>
                                      <td>{{$valueCl->phone}}</td>
                                      <td>{{$valueCl->celphone}}</td>
                                      <td>{{$valueCl->email}}</td>
                                      <td>
                                        @if($valueCl->description=='Activo')
                                        <span class="label label-success">{{$valueCl->description}}</span>
                                        @else
                                        <span class="label label-danger">{{$valueCl->description}}</span>
                                        @endif
                                      </td>
                                      <td>
                                          <a href="#" rel="modal:open" data-id="{{$valueCl->id}}/{{$valueCl->name}}" title="Editar" class="edit edit-client">
                                            <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
                                          </a>
                                          &nbsp;
                                          <a  href="#" id="delete-client" title="Eliminar" data-id="{{$valueCl->id}}/{{$valueCl->name}}" class="delete">
                                            <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
                                          </a>
                                      </td>
                                  </tr>
                                  @endforeach
                            </tbody>
                      </tbody>
                    </table>
                  </div>
                  
                  <div class="row">
                    <div class="pull-left" id="mostraradd" style="margin-left: 15px;   margin-top: 20px;">
                        <select id="cant_mostrar" class="form-control" onchange="filterClients();">
                          <option value="10">Paginacion de 10</option>
                          <option value="20">Paginacion de 20</option>
                          <option value="50">Paginacion de 50</option>
                          <option value="100">Paginacion de 100</option>
                        </select>
                    </div>
                    <div class="pull-right" id="paginates" style="margin-right: 10px;">
                        {{$listaClientes->links()}}
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="divRegistrarCliente" hidden="">
                  <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">Formulario de registro</h3>
                        </div>
                        <div class="panel-body">
                          <form id="clients-submit" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="Clientsid" id="Clientsid" value="">
                            <input type="hidden" name="typeClient" id="typeClient" value="1">
                            <div class="col-md-12">
                              <h4>Informacion basica.</h4>
                              <div class="form-group has-error col-md-3">
                                <label for="inputSuccess1" class="control-label">Tipo Documento:</label>
                                <select class="form-control" name="Clienttype" id="Clienttype" required="">
                                  <option value="1">D.N.I</option>
                                  <option value="2">R.U.C</option>
                                </select>
                              </div>
                              <div class="form-group has-error col-md-2">
                                <label for="inputWarning1" class="control-label">No. Documento:</label>
                                <input name="Clientdocument" id="Clientdocument" type="text" class="form-control" placeholder="Ingrese No. documento" required="" minlength="8" maxlength="11">
                              </div>
                              <div class="form-group has-error col-md-4">
                                <label for="inputError1" class="control-label">Nombre:</label>
                                <input name="Clientname" id="Clientname" type="text" class="form-control" required="" placeholder="Ingrese nombre">
                              </div>
                              <div class="form-group has-error col-md-3">
                                <label for="inputSuccess1" class="control-label">Celular:</label>
                                <input name="celphone" id="celphone" type="text" class="form-control" required="" placeholder="Ingrese No celular">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <h4>Informacion de contacto.</h4>
                              <div class="form-group has-success col-md-6">
                                <label for="inputSuccess1" class="control-label">Direccion:</label>
                                <input name="Clientaddress" id="Clientaddress" type="text" class="form-control" placeholder="Ingrese direccion">
                              </div>
                              <div class="form-group has-success col-md-2">
                                <label for="inputSuccess1" class="control-label">Departamento:</label>
                                 <select class="form-control" name="idDepa" id="idDepa">
                                  <option value="">Seleccionar</option>
                                </select>
                              </div>
                              <div class="form-group has-success col-md-2">
                                <label for="inputSuccess1" class="control-label">Provincia:</label>
                                <select class="form-control" name="idProv" id="idProv">
                                  <option value="">Seleccionar</option>
                                </select>
                              </div>
                              <div class="form-group has-success col-md-2">
                                <label for="inputSuccess1" class="control-label">Distrito:</label>
                                 <select class="form-control" name="idDist" id="idDist">
                                  <option value="">Seleccionar</option>
                                </select>
                              </div>

                              <div class="form-group has-success col-md-4">
                                <label for="inputWarning1" class="control-label">Correo:</label>
                                <input name="Clientemail" id="Clientemail" type="text" class="form-control" placeholder="Ingrese correo ">
                              </div>

                              <div class="form-group has-success col-md-2">
                                <label for="inputSuccess1" class="control-label">Telefono:</label>
                                <input name="Clientphone" id="Clientphone" type="text" class="form-control"  placeholder="Ingrese No telefono">
                              </div>
                              
                              <div class="form-group has-success col-md-2">
                                <label for="inputError1" class="control-label">Contacto:</label>
                                <input name="Clientcontact" id="Clientcontact" type="text" class="form-control" placeholder="Ingrese contacto">
                              </div>
                              
                            </div>
                            <div class="col-md-12">
                              <h4>Otras Informaciones.</h4>
                              <div class="form-group has-success col-md-3">
                                <label for="inputSuccess1" class="control-label">Fecha de Nacimiento:</label>
                                <input name="Clientbirthdate" id="Clientbirthdate" type="text" class="form-control" autocomplete="off" placeholder="Ingrese fecha de nacimiento">
                              </div>
                              <div class="form-group has-success col-md-3">
                                <label for="inputSuccess1" class="control-label">Fecha de Aniversario:</label>
                                <input name="Clientanniversary" id="Clientanniversary" type="text" class="form-control" autocomplete="off" placeholder="Ingrese fecha aniversario">
                              </div>
                              <div class="form-group has-success col-md-2">
                                <label for="inputWarning1" class="control-label">Estatus:</label>
                                <select class="form-control" name="Clientstatus" id="Clientstatus">
                                  <option value="1">Activo</option>
                                  <option value="0">Inactivo</option>
                                </select>
                              </div>
                              <div class="form-group has-success col-md-4">
                                <label for="inputError1" class="control-label">Observación:</label>
                                <input name="Clientobservations" id="Clientobservations" type="text" class="form-control" placeholder="Ingrese alguna observacion">
                              </div>
                            </div>
                            <div class="col-md-12">
                             <font color="red"> (*) Todos los campos son requeridos.</font>
                            </div>
                            <div class="col-md-12">
                              <hr>
                              <center>
                               <button type="submit" class="btn btn-primary" id="btn-client" value="add">
                                 <li class="fa fa-save"></li> REGISTRAR CLIENTE
                               </button>
                               <button type="reset" class="btn btn-danger" onclick="getClearClient()">
                                 <li class="fa fa-close"></li> CANCELAR REGISTRO
                               </button>
                              </center>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    @include('layouts.scripts')
    <script src="/js/common/common.js?v=1565450320"></script>
    <script src="/js/client/client.js?v={{ time() }}"></script>
  </body>
</html>