@foreach($listaClientes as $valueCl)
    <tr>
        <td>{{$valueCl->name}}</td>
        <td>{{$valueCl->type}}</td>
        <td>{{$valueCl->document}}</td>
        <td>{{$valueCl->phone}}</td>
        <td>{{$valueCl->celphone}}</td>
        <td>{{$valueCl->email}}</td>
        <td>
            @if($valueCl->description=='Activo')
            <span class="label label-success">{{$valueCl->description}}</span>
            @else
            <span class="label label-danger">{{$valueCl->description}}</span>
            @endif
        </td>
        <td>
            <a href="#" rel="modal:open" data-id="{{$valueCl->id}}/{{$valueCl->name}}" title="Editar" class="edit edit-client">
                <img src="{{asset('/img/edit.svg')}}" width="25px" height="25px">
            </a>
              &nbsp;
            <a  href="#" id="delete-client" title="Eliminar" data-id="{{$valueCl->id}}/{{$valueCl->name}}" class="delete">
                <img src="{{asset('/img/delete.svg')}}" width="35px" height="25px">
            </a>
        </td>
    </tr>
@endforeach