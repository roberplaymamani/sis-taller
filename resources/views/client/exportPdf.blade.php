<style type="text/css">
  * {
    margin: 10px; 
    padding: 0px; 
    box-sizing: border-box;
  }
  table {
    border-spacing: 1;
    border-collapse: collapse;
    background: white;
    border-radius: 10px;
    overflow: hidden;
    width: 100%;
    margin: 0 auto;
    position: relative;
  }
  table * {
    position: relative;
  }
  table td, table th {
    padding-left: 8px;
  }
  table thead tr {
    height: 60px;
    background: #647aab;
  }
  table tbody tr {
    height: 50px;
  }
  table tbody tr:last-child {
    border: 0;
  }
  table td, table th {
    text-align: left;
  }
  table td.l, table th.l {
    text-align: right;
  }
  table td.c, table th.c {
    text-align: center;
  }
  table td.r, table th.r {
    text-align: center;
  }
  th{
    font-family: OpenSans-Regular;
    font-size: 18px;
    color: #fff;
    line-height: 1.5;
    font-weight: bold;
  }
  tbody tr:nth-child(even) {
    background-color: #f5f5f5;
  }
  tbody tr {
    font-family: OpenSans-Regular;
    font-size: 15px;
    line-height: 2;
    font-weight: bold;
  }

  tbody tr:hover {
    color: #555555;
    background-color: #f5f5f5;
    cursor: pointer;
  }
}


  </style>
<!--===============================================================================================-->
  <center>
    <h4><b><i><u>"Sistema de gestion de talleres automotrices"</u></i></b></h4>
  </center>
  <table>
    <td width="50%">
      <div>
         <table style="" >
          <tr style="padding-bottom: 20px;"> 
            <th style="color: #566d9f;">EMPRESA: </th>
            <td>{{$DataEmp['name']}}</td>
          </tr>
          <tr >
            <th style="color: #566d9f; background: white;">RUC: </th>
            <td style="background: white;">{{$DataEmp['ruc']}}</td>
          </tr>
           <tr >
            <th style="color: #566d9f;">FECHA: </th>
            <td >{{date('Y-m-d')}}</td>
          </tr>
        </table>
      </div>
    </td>
    <td width="50%" style="text-align: right;">
      <img src="data:image/jpeg;base64,{{$DataEmp['logo']}}" class="company_logo"  width="150" alt="Company Logo" class="logo" style="margin-right: 50px;">
    </td>
  </table>
  <hr>
  <center>
    <h4>LISTA GENERAL DE {{$DataEmp['type']}}</h4>
  </center>
  <table>
    <thead>
      <tr class="table100-head">
        <th class="">No.</th>
        <th class="">Nombre cliente</th>
        <th class="">T.Doc.</th>
        <th class="">No. Doc.</th>
        <th class="">Celular</th>
        <th class="">Correo</th>
        <th class="">Estado</th>
      </tr>
    </thead>
    <tbody>
      @php($No=1)
      @foreach($listaClientes as $valuelist)
      <tr style="font-size: 8;">
        <td>{{$No++}}</td>
        <td>{{ $valuelist->name }}</td>
        <td class="hidden-sm">{{ $valuelist->type }}</td>
        <td class="hidden-sm">{{ $valuelist->document }}</td>
        <td class="hidden-sm">{{ $valuelist->celphone }}</td>
        <td class="hidden-sm">{{ $valuelist->email }}</td>
        <td>
          @if($valuelist->description=='Activo')
            <b><font style="color: green">{{$valuelist->description}}</font></b>
            @else
            <b><font style="color: red">{{$valuelist->description}}</font></b>
            @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
