@php($item=1)
@foreach($DataSymstem as $valueDSytem)
<tr>
  <td><span class="badge badge-primary">{{$item++}}</span></td>
  <td>{{$valueDSytem->description}}</td>
  @if($valueDSytem->campo4)
    <td>{{$valueDSytem->campo4}}</td>
  @endif
  <td>{{$valueDSytem->abreviation}}</td>
  <td>
    @if($valueDSytem->status!='Activo')
      <span class="label label-danger">{{$valueDSytem->status}}</span></td>
    @else
      <span class="label label-success">{{$valueDSytem->status}}</span></td>
    @endif
  <td>
    <center>
        <a  data-id="{{$valueDSytem->id}}/{{$valueDSytem->description}}" title="Editar" class="edit edit-system">
          <img src="img/edit.svg" width="25px" height="25px">
        </a>
        &nbsp;
        <a href="javascript:void(0);"id="delete-system" data-toggle="tooltip" title="Eliminar" data-id="{{$valueDSytem->id}}/{{$valueDSytem->description}}" class="delete">
          <img src="img/delete.svg" width="35px" height="25px">
        </a>
    </center>
  </td>
</tr>
@endforeach