<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Tablas de sistema</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Mantenimiento de tablas de sistema</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Mantenimiento de tablas de sistema</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    
                    <div class="col-md-9">
                      <div class="panel panel-default ovalado5">
                        <div class="panel-heading" style="margin: 0px;padding-top: 5px;padding-bottom: 5px;">
                          <div class="row" >
                             <div class="input-group pull-left" style="margin-left: 5px;">
                                <center><h3 class="panel-title" style="padding: 8px;" id="list_title">ALAMCENES</h3></center>
                             </div>
                            <div class="input-group pull-right" style="margin-right: 15px;">
                              <div class="form-group has-success">
                                  <input placeholder="Buscar..." type="text" class="form-control" id="searchList" name="searchList">
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="table-responsive">
                            <table id="listSystem12" class="table table-striped table-bordered table-hover table-responsive">
                              <thead>
                                <tr class="bg-primary">
                                  <th>No</th>
                                  <th>Nombre</th>
                                  <th id="Campo4th" hidden=""></th>
                                  <th>Abreviación</th>
                                  <th>Estado</th>
                                  <th><center>Acción</center></th>
                                </tr>
                              </thead>
                              <tbody id="listPaginates">
                                @php($item=1)
                                @foreach($DataSymstem as $valueDSytem)
                                <tr>
                                  <td><span class="badge badge-primary">{{$item++}}</span></td>
                                  <td>{{$valueDSytem->description}}</td>
                                  <td>{{$valueDSytem->abreviation}}</td>
                                  <td><span class="label label-success">{{$valueDSytem->status}}</span></td>
                                  <td>
                                    <center>
                                      <a data-id="{{$valueDSytem->id}}/{{$valueDSytem->description}}" title="Editar" class="edit edit-system">
                                        <img src="img/edit.svg" width="25px" height="25px">
                                      </a>
                                      &nbsp;
                                      <a href="javascript:void(0);"id="delete-system" data-toggle="tooltip" title="Eliminar" data-id="{{$valueDSytem->id}}/{{$valueDSytem->description}}" class="delete">
                                        <img src="img/delete.svg" width="35px" height="25px">
                                      </a>
                                    </center>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                                <div class="pull-right" id="paginates" style="margin-right: 10px;">
                                    {{$DataSymstem->links()}}
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="panel panel-success ovalado5">
                        <div class="panel-heading">
                          <center><h3 class="panel-title">FORMULARIO DE  <text id="tittle">REGISTRO</text></h3></center>
                        </div>
                        <div class="panel-body">
                          <form id="systems-submit" enctype="multipart/form-data">
                            @csrf
                            <h4 class="box-title"> </h4>
                            <div class="col-md-12">

                                <div class="form-group has-error">
                                   <label for="inputSuccess1" class="control-label">Tablas de sistema:</label>
                                   <select class="form-control mayusc" name="DBactual" id="DBactual" required="">
                                      <option value="store" selected="">Almacenes</option>
                                      <option value="advisors_service" >Asesores de Servicio</option>
                                      <option value="bank" >Bancos</option>
                                      <option value="categories" >Categorias de Vehiculos</option>
                                      <option value="companies_insurance" >Compañias de Seguros</option>
                                      <option value="conditions_sale_purchase" >Condiciones de Venta/Compra</option>
                                      <option value="damage_car" >Daños a Vehiculos</option>
                                      <option value="form_payments" >Formas de Pago</option>
                                      <option value="lines_product" >Líneas de Productos</option>
                                      <option value="brands_repuestos" >Marcas de Repuestos</option>
                                      <option value="marks" >Marcas de Vehiculos</option>
                                      <option value="mechanics" >Mecanicos</option>
                                      <!--<option value="months" >Meses del Año</option>-->
                                      <option value="models" >Modelos de Vehiculos</option>
                                      <option value="coins" >Monedas</option>
                                      <option value="reasons_revenue_warehouse" >Motivos de Ingreso a Almacen</option>
                                      <option value="motives_credit_note" >Motivos de Nota de Crédito</option>
                                      <option value="motives_outputs_warehouse" >Motivos de Salidas de Almacen</option>
                                      <option value="movements_cash" >Movimientos de Caja</option>
                                      <option value="country_origin" >Paises de Procedencia</option>
                                      <option value="items" disabled="">Rubros</option>
                                      <option value="sublines_product" >Sublineas de productos</option>
                                      <option value="types_document" >Tipos de Documento</option>
                                      <option value="types_items" >Tipos de Rubros</option>
                                      <option value="types_services" >Tipos de Servicios</option>
                                      <option value="types_card" >Tipos de Tarjeta</option>
                                      <option value="locations_workshop" >Ubicaciones en Taller</option>
                                      <option value="measurement_units" >Unidades de Medida</option>
                                      <option value="orders_work" >Vehiculos en Ordenes de Trabajo</option>

                                    </select>
                                </div>
                            </div>
                            <h4 class="box-title">Datos Generales </h4>
                            <input type="hidden" name="Systemid" id="Systemid" value="">
                            <!--<input type="hidden" name="DBactual" id="DBactual" value="store">-->
                            
                            <div class="col-md-12">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Nombre:</label>
                                  <input name="Systemdescription" id="Systemdescription" type="text" class="form-control" placeholder="Escriba nombre...">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Abreviación:</label>
                                  <input name="Systemabreviation" id="Systemabreviation" type="text" class="form-control" placeholder="Escriba abreviación...">
                                </div>
                            </div>
                            <div class="col-md-12" hidden="" id="div4campos">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label" id="NameLabel"></label>
                                  <input name="campo4" id="campo4" type="text" class="form-control">
                                  <select name="campo4_1" id="campo4_1" class="form-control" type="hidden">
                                    <option value="">Seleccionar...</option>
                                  </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Estado:</label>
                                   <select class="form-control mayusc" name="Systemstatus" id="Systemstatus" required="" >
                                      <option value="1">Activo</option>
                                      <option value="2">bloqueado</option>
                                      <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <label style="color:red">(*) Todos los campos son requeridos.</label>
                            <div class="col-md-12">
                                <center>
                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary margin5_ovalado" id="btn-system" value="add">
                                      <li class="fa fa-save"></li> Registrar reg.
                                    </button>
                                  </div>
                                  <div class="col-md-6">
                                    <button type="button" class="btn btn-danger margin5_ovalado" onclick="getClearSystem();">
                                      <li class="fa fa-close"></li> Cancelar reg.
                                    </button>
                                  </div>
                                   <div class="col-md-12">
                                    <button type="reset" class="btn btn-success margin5_ovalado" onclick="location.href='system/listSystem/export'">
                                      <li class="fa fa-file-text"></li> Exportar  Excel.
                                    </button>
                                  </div>
                                </center>
                            </div>
                          </form>
                        </div>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
  <!--<script src="/js/item/item.js?v={{ time() }}"></script>
  <script src="/js/coin/coin.js?v={{ time() }}"></script>
  <script src="/js/model/model.js?v={{ time() }}"></script>-->
  <script src="/js/system/system.js?v={{ time() }}"></script>
  <!--<script src="/js/advisor/advisor.js?v={{ time() }}"></script>
  <script src="/js/mechanic/mechanic.js?v={{ time() }}"></script>
  <script src="/js/subproduct/subproduct.js?v={{ time() }}"></script>
  <script src="/js/movementscash/movementscash.js?v={{ time() }}"></script> -->
  </body>
</html>