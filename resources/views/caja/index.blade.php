<!DOCTYPE html>
<!--
 Product:        Social - Premium Responsive Admin Template
 Version:        2.1.3
 Copyright:      2015 cesarlab.com
 License:        http://themeforest.net/licenses
 Live Preview:   http://go.cesarlab.com/SocialAdminTemplate2
 Purchase:       http://go.cesarlab.com/PurchaseSocial2
-->
<html>
 @include('layouts.heads')
  <body>
    <div class="wrapper">
      <!-- BEGIN SIDEBAR-->
       @include('layouts.sidebar')
      <!-- END SIDEBAR-->
      <header>
        <!-- BEGIN NAVBAR-->
         @include('layouts.navbar')
        <!-- END NAVBAR-->
      </header>
      <div class="main">
        <!-- BEGIN CONTENT-->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="page-title">Caja</h3>
              <ul class="breadcrumb breadcrumb-arrows breadcrumb-default">
                <li>
                  <a href="#ignore"><i class="fa fa-home fa-lg"></i>
                  </a>
                </li>
                <li>
                  <a href="#ignore">Dashboard</a>
                </li>
                <li>
                  <a href="#ignore">Cierre de caja</a>
                </li>
              </ul>
            </div>
          </div>
        </div>



<!-- Modal -->
<div class="modal fade" id="AperturarCaja" tabindex="-1" role="dialog" aria-labelledby="AperturarCajaTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="AperturarCajaTitle"> <i class="fa fa-plus-square fa-lg"></i> <b>Aperturar Caja</b></h5>
      </div>
      <div class="modal-body">
      <form id="advanced-form" class="form-horizontal" novalidate="novalidate">
        <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">DATOS GENERALES</h3>
                </div>
                  <div class="panel-body">
                    <fieldset>
                       <!-- Text input-->
                      <div class="form-group">
                        <label for="firstname" class="col-md-4 control-label">Fecha: </label>
                        <div class="col-md-4">
                          <input id="firstname" name="firstname" type="text" placeholder="Your Frist Name" class="form-control input-md" value="{{date('Y-m-d')}}">
                        </div>
                      </div>
                      <!-- Select Basic-->
                      <div class="form-group">
                        <label for="title" class="col-md-4 control-label">Moneda:</label>
                        <div class="col-md-4">
                          <select id="title" name="title" class="form-control" aria-required="true" aria-invalid="false">
                            <option value="1">NUEVOS SOLES</option>
                          </select>
                        </div>
                      </div>
                      <!-- Text input-->
                      <div class="form-group">
                        <label for="lastname" class="col-md-4 control-label">Saldo Anterior S/.:</label>
                        <div class="col-md-4">
                          <input id="lastname" name="lastname" type="text" placeholder="0.00" class="form-control input-md">
                        </div>
                      </div>
                      <!-- Text input-->
                      <div class="form-group">
                        <label for="username" class="col-md-4 control-label">Importe Adicional S/.</label>
                        <div class="col-md-4">
                          <input id="username" name="username" type="text" placeholder="0.00" class="form-control input-md">
                        </div>
                      </div>
                      <!-- Password input-->
                      <div class="form-group">
                        <label for="password1" class="col-md-4 control-label">Saldo inicial S/.</label> 
                        <div class="col-md-4">
                          <input id="password1" name="password1" type="password" placeholder="0.00" class="form-control input-md">
                        </div>
                      </div>
                    </fieldset>
                  </div>
                  <div class="panel-footer">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <center>
                          <button type="submit" class="btn btn-primary ovalado5"> <i class="fa fa-save fa-lg"></i> Guardar y aperturar</button>
                          <button type="button" class="btn btn-danger ovalado5"> <i class="fa fa-close fa-lg"></i> Cancelar Apertura</button>
                        </center>
                      </div>
                    </div>
                  </div>
                
              </div>
            </form>

            </div>
          <div class="modal-footer" hidden="">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>




<!-- Modal -->
<div class="modal fade" id="CajaChica" tabindex="-1" role="dialog" aria-labelledby="CajaChicaTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="CajaChicaTitle"> <i class="fa fa-plus-square fa-lg"></i> <b>Movimientos de Caja Chica</b></h5>
      </div>
      <div class="modal-body">

        <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">DATOS GENERALES</h3>
                </div>
                <form id="advanced-form" class="form-horizontal" novalidate="novalidate">
                  <div class="panel-body">
                    <fieldset>
                          <!-- Text input-->
                      <div class="form-group">
                        <label for="firstname" class="col-md-4 control-label">Fecha: </label>
                        <div class="col-md-4">
                          <input id="firstname" name="firstname" type="text" placeholder="Your Frist Name" class="form-control input-md" value="{{date('Y-m-d')}}">
                        </div>
                      </div>
                      <!-- Select Basic-->
                      <div class="form-group">
                        <label for="title" class="col-md-4 control-label">Movimiento:</label>
                        <div class="col-md-4">
                          <select id="title" name="title" class="form-control" aria-required="true" aria-invalid="false">
                            <option value="1">SERVICIO DE LIMPIEZA</option>
                          </select>
                        </div>
                      </div>
                       <!-- Select Basic-->
                      <div class="form-group">
                        <label for="firstname" class="col-md-4 control-label">Fecha: </label>
                        <div class="col-md-4">
                          <input id="firstname" name="firstname" type="text" placeholder="Your Frist Name" class="form-control input-md" value="{{date('Y-m-d')}}">
                        </div>
                      </div>
                       <!-- Select Basic-->
                      <div class="form-group">
                        <label for="title" class="col-md-4 control-label">Moneda:</label>
                        <div class="col-md-4">
                          <select id="title" name="title" class="form-control" aria-required="true" aria-invalid="false">
                            <option value="1">NUEVOS SOLES</option>
                          </select>
                        </div>
                      </div>
                      <!-- Text input-->
                      <div class="form-group">
                        <label for="lastname" class="col-md-4 control-label">Importe Total S/.:</label>
                        <div class="col-md-4">
                          <input id="lastname" name="lastname" type="text" placeholder="0.00" class="form-control input-md">
                        </div>
                      </div>

                      <hr>
                      <!-- Text input-->
                      <div class="form-group">
                        <label for="username" class="col-md-4 control-label">Proveedor:</label>
                        <div class="col-md-4">
                          <select id="title" name="title" class="form-control" aria-required="true" aria-invalid="false">
                            <option value="1">PROVEEDOR 01</option>
                          </select>
                        </div>
                      </div>
                      <!-- Password input-->
                      <div class="form-group">
                        <label for="password1" class="col-md-4 control-label">Comentarios:</label> 
                        <div class="col-md-4">
                          <input id="password1" name="password1" type="password" placeholder="" class="form-control input-md">
                        </div>
                      </div>
                    </fieldset>
                  </div>
                  <div class="panel-footer">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <center>
                          <button type="submit" class="btn btn-primary ovalado5"> <i class="fa fa-save fa-lg"></i> Guardar r</button>
                          <button type="button" class="btn btn-danger ovalado5"> <i class="fa fa-close fa-lg"></i> Cancelar</button>
                        </center>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          <div class="modal-footer" hidden="">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>


        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <div class="row">

                  <button type="button" class="btn btn-tumblr add add-entryadd left_ovalado" data-id="0" id="addInv" onclick="AddInventarioDIV('addInv')" data-toggle="modal" data-target="#AperturarCaja">
                    <span class="fa fa-arrow-down fa-lg"></span>APERTURAR CAJA
                  </button>

                  <button type="button" class="btn btn-linkedin add add-entryadd left_ovalado" data-id="1" id="salInv" onclick="AddInventarioDIV('salInv')" data-toggle="modal" data-target="#CajaChica">
                    <span class="fa fa-arrow-up fa-lg"></span>CAJA CHICA
                  </button>

                  <!--<i class="fa fa-close pull-right fa-lg" style=" padding-top: 10px; margin-right: 15px;" hidden=""></i>-->
                  </div>
                </div>
                <div class="panel-body">
                  <div class="row">

                  <div class="col-md-8">
                    <div id="panels-scollable" class="row">
                      <div class="col-md-12">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <div class="panel-title"><i class="fa fa-desktop"></i>Detalle de movimientos en general</div>
                          </div>
                          <div class="panel-body" style="height: 360px;">
                            <!-- //Notice .scroll class-->
                            <div class="scroll">
                              <div class="table-responsive" >
                                <!-- <table   class="table table-striped table-bordered table-hover table-responsive listEntryRefres"> -->
                                <table   class="table table-striped table-bordered table-hover table-responsive">
                                  <thead>
                                    <tr class="bg-primary">
                                      <td>No.</td>
                                      <th>Tipo Movimiento</th>
                                      <th>Tipo tarjeta</th>
                                      <th>Moneda</th>
                                      <th>Importe Total</th>
                                    </tr>
                                  </thead>
                                  <tbody id="listitemsAdd">
                                     <tr >
                                      <td>No.</td>
                                      <td>Tipo Movimiento</td>
                                      <td>Tipo tarjeta</td>
                                      <td>Moneda</td>
                                      <td>Importe Total</td>
                                    </tr>
                                    <tr>
                                      <td>No.</td>
                                      <td>Tipo Movimiento</td>
                                      <td>Tipo tarjeta</td>
                                      <td>Moneda</td>
                                      <td>Importe Total</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group has-success">
                                <label for="inputSuccess1" class="control-label">Total Ingresos:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalIngresos" id="totalIngresos" placeholder="0.00" style="padding-top: 0px;"> 
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group has-success">
                                <label for="inputSuccess1" class="control-label">Total Salidas:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalSalida" id="totalSalida" placeholder="0.00" style="padding-top: 0px;">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group has-success">
                                <label for="inputSuccess1" class="control-label">Total Saldo:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalSaldo" id="totalSaldo" placeholder="0.00" style="padding-top: 0px;">
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                   
                  
                  <div class="col-md-4">
                    <form id="entry-submit" enctype="multipart/form-data">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <center><i>DATOS GENERALES</i></center>
                        </div>
                        <div class="panel-body">
                            
                            <div class="col-md-6">
                               <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Fecha:</label>
                                  <input type="text" class="form-control mayusc" name="fechaCierre" id="fechaCierre" value="{{date('Y-m-d')}}" readonly=""> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Moneda:</label>
                                  <select class="form-control mayusc" name="tipoMoneda" id="tipoMoneda" required></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Saldo Inicial:</label>
                                  <input type="number" class="form-control mayusc fixed-input-a" name="saldoInicial" id="saldoInicial"  required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Total:</label>  <label>S/.</label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                   
                                </div>
                            </div>
                            <div class="panel-heading">
                              <center>
                                <i>
                                  ====================================================
                                </i>
                              </center>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Movimientos:</label>  <label>S/.</label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Saldo Proliminar:</label>  <label>S/.</label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                   
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Ajuste:</label>  <label>S/.</label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                   
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group has-success">
                                  <label for="inputSuccess1" class="control-label">Saldo final:</label>  <label>S/.</label>
                                  <input type="text" class="form-control mayusc fixed-input-a ghs" name="EntryInventorytotal" id="EntryInventorytotal" placeholder="0.00" readonly>
                                </div>
                            </div>
                        </div>
                      </div>
                      <br>
                    </form>
                  </div>


                   <div class="col-md-12">
                    <div class="panel panel-default">
                         
                      <div class="panel-heading">
                          <center><i>DETALLE DE MOVIMIENTOS EN EFECTIVO</i></center>
                      </div>
                      <div class="panel panel-default" style="margin-top: 0px;">
                        <div class="panel-body">
                         <div class="form-group has-success">
                           <div class="col-md-2">
                              <div class="form-group has-success">
                                <label for="inputSuccess1" class="control-label">Total Ingresos:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalIngresos" id="totalIngresos" placeholder="0.00" required> 
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Total Salidas:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalSalida" id="totalSalida" placeholder="0.00" required>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group has-error">
                                <label for="inputSuccess1" class="control-label">Total Saldo:</label> <label>S/.</label>
                                <input type="text" class="form-control mayusc" name="totalSaldo" id="totalSaldo" placeholder="0.00" required>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group has-primary">
                                <label for="inputSuccess1" class="control-label">Observacion:</label>
                                <input type="text" class="form-control mayusc" name="observacionIng" id="observacionIng" placeholder="Ingrese si tiene alguna observacion" required>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      <div class="panel-footer">
                             <center>
                              <button type="button" class="btn btn-primary margin5" id="btn-entryinventory"  value="add" onclick="$('#submit_form').click();">
                               <i class="fa fa-save fa-log"></i> GUARDAR CIERRE CAJA
                              </button>
                              <button type="reset" class="btn btn-danger margin5" id="cancelarRegistro"  onclick="getClearEntryInventory(0)">
                               <i class="fa fa-close fa-log"></i> CANCELAR CIERRE CAJA
                              </button>
                            </center>
                          </div>
                    </div>
                  </div>


                    

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT-->
      </div>
      @include('layouts.footer')
    </div>
    <br>
    <br>
    <br>
    <br>
    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="myModalLabel" class="modal-title">Modal Settings</h4>
          </div>
          <div class="modal-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, deserunt!</div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.scripts')
  <script src="/js/jquery.modal.min.js"></script>
  <script src="/js/common/common.js?v=1565886637"></script>
 <!-- <script src="/js/user/user.js?v={{ time() }}"></script>-->
  <script src="/js/company/company.js?v={{ time() }}"></script>
</html>