@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT-->
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3 class="page-title">Nota de Débito Electrónica</h3>
    </div>
  </div>
</div>
<div class="container" style="max-width: 1200px">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<div class="alert bg-primary alert-styled-left" style="margin: 0 auto;">
							<button type="button" class="close" data-dismiss="alert">
								<span>×</span><span class="sr-only">Close</span>
							</button>
							<span class="text-semibold">Te encuentras en un ambiente de pruebas</a></span>
						</div>
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
		                      	<label class="control-label"><i class="fa fa-user"></i> Numero de Documento</label>
		                        <div class="input-group">
								  <span class="input-group-btn">
		                            <button type="button" class="btn btn-primary">RUC</button>
		                          </span>
		                          <input id="do" name="do" type="text" class="form-control" maxlength="11">
		                          <span class="input-group-btn">
		                            <button id="bdoc" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
		                          </span>
		                        </div>
		                    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
		                      	<label class="control-label"><i class="fa fa-credit-card"></i> Nombre o Razon Social</label>
		                        <input name="no" id="no" type="text" class="form-control">
		                    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
		                      	<label class="control-label"><i class="fa fa-home"></i> Direccion</label>
		                        <input name="di" id="di" type="text" class="form-control">
		                    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
		                      	<label class="control-label"><i class="fa fa-globe"></i> Estado</label>
		                        <input name="ub" id="ub" type="text" class="form-control">
		                    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group" id="bootstrapSwitchs">		                      	
		                        <label class="switch">
								  <input id="ck" type="checkbox">
								  <span id="check" class="slider round"></span>
								</label>
		                        <label class="control-label">¿Deseas Enviar el Comprobante Electrónico al Email del Cliente?</label>
		                    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group" id="cor" hidden="true">
								<label class="control-label"><i class="fa fa-envelope"></i> Correo del Cliente</label>
		                        <input type="text" class="form-control">
		                    </div>
						</div>
						<div class="col-md-12">
							<label class="control-label">Lista de Productos</label>
							<br>
							<div class="btn-group">
	                            <a class="btn btn-warning" href="#"><i class="fa fa-edit"> Editar</i></a>
	                            <a class="btn btn-success" href="#"><i class="fa fa-plus"> Agregar</i></a>
	                            <a class="btn btn-danger" href="#"><i class="fa fa-trash"> Eliminar</i></a>
                            </div>
							<table class="table table-hover table-bordered">
								<thead class="bg-primary">
									<tr>
										
										<th>Descipcion</th>
										
										<th>Tipo IGV</th>
										
										<th>Und/Medida</th>
										
										<th>Precio</th>
										<th>Cantidad</th>
										<th>Sub Total</th>
										<th>IGV</th>
										<th>Importe</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						<div class="col-md-12"><label class="control-label">Detalle de Documento</label></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-sm-6">
									<div class="col-md-12 mt-15" id="content_descuento_porcentaje">
										<label><i class="icon-percent position-left"></i> Descuento Total (en porcentaje %): </label>
										<input type="text" value="0.0" placeholder="Porcentaje de Descuento Total" class="form-control">
									</div>
									<div class="col-md-12 mt-15">
										<div class="content-group">
											<h5><i class="icon-notebook position-left"></i> Observación:</h5>
											<div class="mb-15 mt-15">
												<textarea rows="5" cols="100" name="observacion_documento" class="form-control" placeholder="Escribe aquí una observación"></textarea>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="content-group"><label for=""><h5>Resumen:</h5></label>
										<table class="table">
											<tbody>
												<tr id="row_gravada_documento">
													<th>Gravada:</th>
													<td class="text-right">
														<div class="row">
															<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
															<div class="col-md-10"><input class="form-control" readonly="true" name="monto" id="monto" value="0.00"></div>
														</div>
													</td>
												</tr>
												<tr id="row_igv_documento">
													<th>IGV: <span class="text-regular">(18%)</span></th>
													<td class="text-right">
														<div class="row">
															<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
															<div class="col-md-10"><input class="form-control" readonly="true" name="igv" id="igv" value="0.00"></div>
														</div>
													</td>
												</tr>
												<tr id="row_descuento_documento">
													<th><span class="text-danger">(-)</span> Descuento Total:</th>
													<td class="text-right">
														<div class="row">
															<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
															<div class="col-md-10"><input class="form-control" readonly="true" name="dsct" id="dsct" value="0.00"></div>
														</div>
													</td>
												</tr>
												<tr id="row_total_documento">
													<th>Total:</th>
													<td class="text-right text-primary">
														<div class="row">
															<div class="col-md-2"><h5 class="text-semibold"><span class="simbolo_moneda">S/.</span></h5></div>
															<div class="col-md-10"><input class="form-control" readonly="true" name="total" id="total" value="0.00"></div>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CONTENT-->
@endsection
@push('css')
	<link rel="stylesheet" href="{{url('css/switch.css')}}">
@endpush

@push('js')
<script>
$(document).on('click', '#check', function (e) {
	if( $('#ck').is(':checked') ) {
	    $("#cor").prop("hidden",true);
	    $('#ck').prop("checked", true);
	}else{
		$("#cor").prop("hidden",false);
		$('#ck').prop("checked", false);
	}
});
</script>
<script>
	$(document).on('click', '#bdoc', function (e) {
		var a=$("#do").val();
		if(a.length===11){
			$.post("{{url('../ruc/consulta.php')}}",{nruc:a, _token: '{{csrf_token()}}'},function(r){
				$("#do").val(r.result.ruc);
				$("#no").val(r.result.razon_social);
				$("#di").val(r.result.direccion);
				$("#ub").val(r.result.condicion);
				$("#bdoc").removeClass("btn-danger");
			});
		}else{
			$("#no").val("RUC Invalido");
			$("#bdoc").addClass("btn-danger");
		}
	});
</script>
@endpush
