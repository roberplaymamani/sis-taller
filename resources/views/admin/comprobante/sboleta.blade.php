@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT-->
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h3 class="page-title">Boleta Electrónica</h3>
    </div>
  </div>
</div>
<div class="container" style="max-width: 1200px">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<div class="alert bg-primary alert-styled-left" style="margin: 0 auto;">
							<button type="button" class="close" data-dismiss="alert">
								<span>×</span><span class="sr-only">Close</span>
							</button>
							<span class="text-semibold">Te encuentras en un ambiente de pruebas</a></span>
						</div>
					</h3>
				</div>
				<div class="panel-body">
					<form id="fform" method="post">
						@csrf
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
			                      	<label class="control-label"><i class="fa fa-user"></i> Numero de Documento</label>
			                        <div class="input-group">
									  <span class="input-group-btn">
			                            <button type="button" class="btn btn-primary">DNI</button>
			                          </span>
			                          <input id="do" name="do" type="text" class="form-control" maxlength="8">
			                          <span class="input-group-btn">
			                            <button id="bdoc" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
			                          </span>
			                        </div>
			                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
			                      	<label class="control-label"><i class="fa fa-credit-card"></i> Nombre o Razon Social</label>
			                        <input name="no" id="no" type="text" class="form-control">
			                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
			                      	<label class="control-label"><i class="fa fa-home"></i> Direccion</label>
			                        <input name="di" id="di" type="text" class="form-control">
			                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
			                      	<label class="control-label"><i class="fa fa-globe"></i> Estado</label>
			                        <input name="ub" id="ub" type="text" class="form-control">
			                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group" id="bootstrapSwitchs">		                      	
			                        <label class="switch">
									  <input id="ck" type="checkbox">
									  <span id="check" class="slider round"></span>
									</label>
			                        <label class="control-label">¿Deseas Enviar el Comprobante Electrónico al Email del Cliente?</label>
			                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group" id="cor" hidden="true">
									<label class="control-label"><i class="fa fa-envelope"></i> Correo del Cliente</label>
			                        <input type="text" class="form-control">
			                    </div>
							</div>
							<div class="col-md-12">
								<label class="control-label">Lista de Productos</label>
								<br>
								<div class="btn-group">
		                            <a class="btn btn-warning" href="#"><i class="fa fa-edit"> Editar</i></a>
		                            <a class="btn btn-success" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"> Agregar</i></a>
		                            <a class="btn btn-danger" href="#"><i class="fa fa-trash"> Eliminar</i></a>
	                            </div>
								<table id="tbproducto" class="table table-hover table-bordered">
									<thead class="bg-primary">
										<tr>
											<th>Descipcion</th>
											<th>Tipo IGV</th>
											<th>Und/Medida</th>
											<th>Precio</th>
											<th>Cantidad</th>
											<th>Sub Total</th>
											<th>IGV</th>
											<th>Importe</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							<div class="col-md-12"><label class="control-label">Detalle de Documento</label></div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-sm-6">
										<div class="col-md-12 mt-15" id="content_descuento_porcentaje">
											<label><i class="icon-percent position-left"></i> Descuento Total (en porcentaje %): </label>
											<input id="descto" onkeyup="ctotal()" type="text" value="0.00" placeholder="Porcentaje de Descuento Total" class="form-control">
										</div>
										<div class="col-md-12 mt-15">
											<div class="content-group">
												<h5><i class="icon-notebook position-left"></i> Observación:</h5>
												<div class="mb-15 mt-15">
													<textarea rows="5" cols="100" name="observacion_documento" class="form-control" placeholder="Escribe aquí una observación"></textarea>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="content-group"><label for=""><h5>Resumen:</h5></label>
											<table class="table">
												<tbody>
													<tr id="row_gravada_documento">
														<th>Gravada:</th>
														<td class="text-right">
															<div class="row">
																<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
																<div class="col-md-10"><input style="border:none;background-color: white;" class="form-control" readonly="true" name="monto" id="monto" value="0.00"></div>
															</div>
														</td>
													</tr>
													<tr id="row_igv_documento">
														<th>IGV: <span class="text-regular">(18%)</span></th>
														<td class="text-right">
															<div class="row">
																<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
																<div class="col-md-10"><input style="border:none;background-color: white;" class="form-control" readonly="true" name="igv" id="igv" value="0.00"></div>
															</div>
														</td>
													</tr>
													<tr id="row_descuento_documento">
														<th><span class="text-danger">(-)</span> Descuento Total:</th>
														<td class="text-right">
															<div class="row">
																<div class="col-md-2"><span class="simbolo_moneda">S/.</span></div>
																<div class="col-md-10"><input style="border:none;background-color: white;" class="form-control" readonly="true" name="dsct" id="dsct" value="0.00"></div>
															</div>
														</td>
													</tr>
													<tr id="row_total_documento">
														<th>Total:</th>
														<td class="text-right text-primary">
															<div class="row">
																<div class="col-md-2"><h5 class="text-semibold"><span class="simbolo_moneda">S/.</span></h5></div>
																<div class="col-md-10"><input style="border:none;background-color: white;" class="form-control" readonly="true" name="total" id="total" value="0.00"></div>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12"><label class="control-label">Modo de Envio</label></div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-4">
										<div class="radio"><label><input type="radio" name="docs">Solo Firma</label></div>
									</div>
									<div class="col-md-4">
										<div class="radio"><label><input type="radio" name="docs" checked="checked">Enviar Ahora</label></div>
									</div>
									<div class="col-md-4">
										<div class="radio"><label><input type="radio" name="docs">Solo Guardar</label></div>
									</div>
								</div>
							</div>
							<div class="col-md-12 text-center"><a class="btn btn-primary" id="bform"><i class="fa fa-save"></i> Guardar Factura</a></div>
						</div>
						<input name="lista" type="text" id="lista" >
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bs-example-modal-lg">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
        <h4 id="myModalLabel2" class="modal-title">Agregar Producto</h4>
      </div>
      <div class="modal-body">
		<div class="container">
			<div class="row">
				<div class="input-group">
			      <input type="text" class="form-control" placeholder="Escribe el Código del Producto" id="cop" onkeyup="bsp()">
			      <span class="input-group-btn">
			        <button class="btn btn-primary" type="button">Buscar</button>
			      </span>
			    </div>
			    <br>
				<div id="data" style="max-height: 70vh;overflow: auto;">
					
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Terminar</button>
      </div>
    </div>
  </div>
</div>
<!-- END CONTENT-->
@endsection
@push('css')
	<link rel="stylesheet" href="{{url('css/switch.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>

@endpush

@push('js')
<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
<script src="{{url('assets/js/notify.js')}}"></script>

<script src="{{url('assets/js/ajax/boleta.js')}}"></script>
<script>
$(document).on('click', '#bdoc', function (e) {
	var a=$("#do").val();
	if(a.length==8){
		$.post("{{url('../dni/consulta.php')}}",{ndni:a, _token: '{{csrf_token()}}'},function(r){
			if (r.success) {
				$.notify("DNI encontrado",{
				    className:"success",
				    globalPosition: "top center"
				});
				$("#no").val(r.result.nombre);
				$("#bdoc").removeClass("btn-danger");
			}else{
			  $.notify(r.message,{
			    className:"error",
			    globalPosition: "top center"
			  });
			  $("#no").val("");
			}
		});
	}else{
		$.notify("DNI no valido",{
		    className:"error",
		    globalPosition: "top center"
		  });
		$("#bdoc").addClass("btn-danger");
		$("#no").val("");
	}
});
</script>
@endpush
