<table class="table table-hover table-bordered">
	<tr class="bg-primary">
		<th>Codigo</th>
		<th>Descripcion</th>
		<th>Precio</th>
		<th>Action</th>
	</tr>
	@foreach($cp as $co)
		<tr>
			<td>{{$co->id_products}}</td>
			<td>{{$co->description}}</td>
			<td>{{$co->PVP}}</td>
			<td><a onclick="addproducto(JSON.stringify({{$co}}))" class="btn btn-primary" href="#"><i class="fa fa-plus"></i></a></td>
		</tr>
	@endforeach
</table>