<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EntryInventory extends Model
{   
    protected $table='entry_inventorys';
    protected $fillable = ['id','id_store','id_reasons','id_coins','entry_exit','num_guide','date_registration','value_sale','taxt','total_net','status','profile_edit'];
}
