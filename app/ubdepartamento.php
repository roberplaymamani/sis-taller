<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ubdepartamento extends Model
{
	protected $table='ubdepartamento';
    protected $fillable = ['idDepa','departamento'];
}
