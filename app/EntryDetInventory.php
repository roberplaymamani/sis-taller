<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EntryDetInventory extends Model
{   
    protected $table='det_entry_inventorys';
    protected $fillable = ['id','id_entry_inventorys','id_products','quantity','price','total','updated_at','created_at'];
}
