<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Products extends Model
{   
    protected $table = 'products';
    protected $fillable = ['id','id_products','date_purchase','description','abreviation','id_lines','id_sublines','id_provider','id_measureunit','price_purchase','id_coins','cost','gain','by_profit','tax','by_tax','PVP','status','imagen','id_marks','id_country','warranty','profile_edit'];
}