<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ubprovincia extends Model
{
	protected $table='ubprovincia';
    protected $fillable = ['idProv','provincia','idDepa'];
}
