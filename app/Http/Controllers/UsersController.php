<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Privileges;
use App\Status;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class UsersController extends Controller
{

    public function index()
    {
        $options = 'usuario';
        $selects = 'usuarios';

        if (Input::get('search') == '1') {
            $ListUsers = Users::select('a.id', 'a.name', 'a.email', 'b.description as status', 'c.description')->from('users as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('privileges as c', 'a.id_privileges', '=', 'c.id')->where('a.name', 'like', Input::get('valueInput') . '%');
            $ListUsers = $ListUsers->orderBy('a.id', 'desc')->paginate(10);
        } else {
            $ListUsers = Users::select('a.id', 'a.name', 'a.email', 'b.description as status', 'c.description')->from('users as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('privileges as c', 'a.id_privileges', '=', 'c.id')->orderBy('a.id', 'desc')->paginate(10);
        }
        if (request()->ajax()) {
            $elements = (String) \View::make('user.userElements', [
                'ListUsers' => $ListUsers,
            ]);
            $pagination = (String) \View::make('user.userPaginate', [
                'ListUsers' => $ListUsers,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }
        return view('user.index', ['ListUsers' => $ListUsers, 'options' => $options, 'selects' => $selects]);

    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function create()
    {
        $options    = 'Users';
        $selects    = 'UsersRegister';
        $status     = Status::select('a.id', 'a.description')->from('status_globals as a')->orderby('a.description')->get();
        $privileges = Privileges::select('a.id', 'a.description')->from('privileges as a')->orderby('a.description')->get();
        return view('user.create', ['status' => $status, 'privileges' => $privileges, 'options' => $options, 'selects' => $selects]);
    }

    public function store(Request $request)
    {
        $Userid       = $request->Userid;
        $Username     = $request->Username;
        $Useruser     = $request->Useruser;
        $Userstatus   = $request->Userstatus;
        $Userprofile  = $request->Userprofile;
        $Userpassword = Hash::make($request->Userpassword);

        $user = Users::updateOrCreate(
            ['id' => $Userid],
            [
                'name'          => $Username,
                'email'         => $Useruser,
                'status'        => $Userstatus,
                'password'      => $Userpassword,
                'id_privileges' => $Userprofile,
            ]
        );
        return Response::json($user);
    }

    public function edit($id)
    {
        $where      = array('id' => $id);
        $user       = Users::where($where)->first();
        $status     = Status::select('a.id', 'a.description')->from('status_globals as a')->orderby('a.description')->get();
        $privileges = Privileges::select('a.id', 'a.description')->from('privileges as a')->orderby('a.description')->get();
        return Response::json(['user' => $user, 'status' => $status, 'privileges' => $privileges]);
    }

    public function destroy($id)
    {
        $user = Users::where('id', $id)->delete();
        return Response::json($user);
    }
    public function profileuser($value = '')
    {

        $options = 'usuario';
        $selects = 'profile';

        $ListUsers = Users::select('a.id', 'a.name', 'a.email', 'b.description as status', 'c.description')->from('users as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('privileges as c', 'a.id_privileges', '=', 'c.id')->where('a.id', '=', \Auth::user()->id)->orderBy('a.id', 'desc')->get();
        return view('user.profile_user', ['ListUsers' => $ListUsers, 'options' => $options, 'selects' => $selects]);
    }
    public function save_profile_user(Request $request)
    {
        $verificar = 'success';
        $Userpassword = \Auth::user()->password;
        if ($request->clavesuaurio1 != '' && $request->clavesuaurio2 != '') {
            if (password_verify($request->clavesuaurio, \Auth::user()->password)) {
                $Userpassword=Hash::make($request->clavesuaurio1);
            }else{
                $verificar = 'error';
            }
        }
        if ($verificar != 'success') {
             return Response::json(array('status'=>$verificar));
        }
        $user = Users::updateOrCreate(
            ['id' => \Auth::user()->id],
            [
                'name'          => $request->nombreapel,
                'email'         => $request->nombreusuario,
                'password'      => $Userpassword,
            ]
        );
        return Response::json(array('status'=>$verificar));
    }
}
