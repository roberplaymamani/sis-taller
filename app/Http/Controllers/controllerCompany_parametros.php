<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\company;
use App\System;
use App\Coins;
class controllerCompany_parametros extends Controller
{
    public function index()
    {
    	$options = 'archivos';
        $selects = 'parametros';

        $codMonedas=Coins::select('id','description','abreviation')->get();
        $codFormapagos=System::select('id','description','abreviation')->from('form_payments')->get();;
        $codCondicions=System::select('id','description','abreviation')->from('conditions_sale_purchase')->get();;
        $codAlmacens=System::select('id','description','abreviation')->from('store')->get();;
        $dataCompany = company::select('id', 'rucEmpresa', 'razonsocialEmpresa', 'direccionEmpresa', 'telefonoEmpresa', 'celularEmpresa', 'emailEmpresa', 'codMoneda', 'codFormapago', 'codCondicion', 'IGV', 'Ganancia', 'LinkSunat', 'paginaWeb', 'logoEmpresa', 'codAlmacen')->get();

        return view('company.index', [
            'codMonedas'    => $codMonedas,
            'codFormapagos' => $codFormapagos,
            'codCondicions' => $codCondicions,
            'codAlmacens'   => $codAlmacens,
            'dataCompany'   => $dataCompany,
            'options'       => $options,
            'selects'       => $selects
        ]);
    }
    public function store(Request $request)
    {
        //return $request;

        if ($request->hasFile('logoEmpresa')) {
            $archivo        = $request->file('logoEmpresa');
            $extention      = explode('/', $_FILES['logoEmpresa']['type']);
            $logo = time() . '.' . end($extention);
            //$archivo->move(base_path() . '/../public_html/img/company/', $nombre_archivo);#prodcucion
            $archivo->move(public_path() . '/img/company/', $logo);#Local
            $statusLogo=1;
        } else {
            $logo = $request->input('logoDefault');
            $statusLogo=0;
        }

    	$client = company::updateOrCreate(
            ['id' => $request->id],
                [
                   'id'=>$request->id,
                   'rucEmpresa'=>$request->rucEmpresa,
                   'razonsocialEmpresa'=>$request->razonsocialEmpresa,
                   'direccionEmpresa'=>$request->direccionEmpresa,
                   'telefonoEmpresa'=>$request->telefonoEmpresa,
                   'celularEmpresa'=>$request->celularEmpresa,
                   'emailEmpresa'=>$request->emailEmpresa,
                   'codMoneda'=>$request->codMoneda,
                   'codFormapago'=>$request->codFormapago,
                   'codCondicion'=>$request->codCondicion,
                   'IGV'=>$request->IGV,
                   'Ganancia'=>$request->Ganancia,
                   'LinkSunat'=>$request->LinkSunat,
                   'paginaWeb'=>$request->paginaWeb,
                   'logoEmpresa'=>$logo,
                   'codAlmacen'=>$request->codAlmacen,

                ]
        );
        return \Response::json(['img'=>$logo,'statuslogo'=>$statusLogo]);
    }
    public function FunctionName($value='')
    {
    	# code...
    }
}
