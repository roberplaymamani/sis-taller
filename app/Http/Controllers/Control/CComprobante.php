<?php

namespace App\Http\Controllers\Control;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CComprobante extends Controller{

    public function vfactura() {
        $options='Products';
        $selects='ProductsList';
    	return view("admin.comprobante.sfactura",compact("options","selects"));
    }
    public function vboleta() {
        $options='Products';
        $selects='ProductsList';
    	return view("admin.comprobante.sboleta",compact("options","selects"));
    }

    public function vncredito() {
        $options='Products';
        $selects='ProductsList';
    	return view("admin.comprobante.sncredito",compact("options","selects"));
    }

    public function vndevito() {
        $options='Products';
        $selects='ProductsList';
    	return view("admin.comprobante.sndevito",compact("options","selects"));
    }

    public function lcomprobante() {
        $options='Products';
        $selects='ProductsList';
    	return view("admin.comprobante.lcomprobante",compact("options","selects"));
    }

    public function sfactura(Request $r) {
        $li=json_decode($r->lista);
        return count($li);
    }
    public function sboleta(Request $r) {
        $li=json_decode($r->lista);
        return count($li);
    }

    public function sncredito() {
        return view("admin.comprobante.sncredito");
    }

    public function sndevito() {
        return view("admin.comprobante.sndevito");
    }

    public function bsproducto($cop) {
        $cp=\App\Modelo\Products::with("measurement_units")->where("id_products","LIKE","%".$cop."%")->get();
        return view("admin.comprobante.modal.producto",compact("cp"));
    }



    public function padron($ruc) {
        $pa=\App\Padron::where("ruc",$ruc)->get();

        //$pa=\App\Padron::all();
        /*
        on(\Session::get('kodo'))->
        $pa=new \App\Padron();
        $pa->setConnection('kodo');
        $ls=$pa->where("ruc",$ruc)->get();
        */

        return $pa;
    }

}
