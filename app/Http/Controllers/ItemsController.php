<?php
 
namespace App\Http\Controllers;
 
use App\Items;
use App\Coins;
use App\Status;
use App\Types_Items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\ItemsExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;
 
class ItemsController extends Controller
{

    public function index()
    {  
        $options='archivos';
        $selects='rubros';

        if (Input::get('cant_mostrar')>0) {
           $N_paginador=Input::get('cant_mostrar');
        }else{
            $N_paginador=10;
        }
        if (Input::get('search')=='1') {
            $listRubros= Items::select('a.id','c.description as items','a.description','a.abreviation','d.symbol','a.price','b.description as status')->from('items  as a')->join('status_globals  as b','a.status','=','b.id')->join('types_items  as c','a.id_types_items','=','c.id')->join('coins  as d','a.id_coins','=','d.id')->where('a.description','like',Input::get('valueInput').'%')->orWhere('c.description','like',Input::get('valueInput').'%');
            $listRubros=$listRubros->orderBy('a.id', 'desc')->paginate($N_paginador);
        }else{
            $listRubros=Items::select('a.id','c.description as items','a.description','a.abreviation','d.symbol','a.price','b.description as status')->from('items  as a')->join('status_globals  as b','a.status','=','b.id')->join('types_items  as c','a.id_types_items','=','c.id')->join('coins  as d','a.id_coins','=','d.id')->orderBy('a.id', 'desc')->paginate($N_paginador);
        }

        if (request()->ajax()) {
            $elements = (String) \View::make('item.rubrosElements', [
            'listRubros' => $listRubros,
            ]);
            $pagination = (String) \View::make('item.rubrosPaginate', [
                'listRubros' => $listRubros,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }

         $listRubros=Items::select('a.id','c.description as items','a.description','a.abreviation','d.symbol','a.price','b.description as status')->from('items  as a')->join('status_globals  as b','a.status','=','b.id')->join('types_items  as c','a.id_types_items','=','c.id')->join('coins  as d','a.id_coins','=','d.id')->orderBy('a.id', 'desc')->paginate(10);
        
        return view('item.index',['listRubros'=>$listRubros,'options'=>$options,'selects'=>$selects]);
    }

    public function export() 
    { 
        return Excel::download(new ItemsExport, 'items.xlsx');
    }

    public function create()
    {   
        $options='Users';
        $selects='UsersRegister';
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $privileges=Privileges::select('a.id','a.description')->from('privileges as a')->orderby('a.description')->get();
        return view('user.create',['status'=>$status,'privileges'=>$privileges,'options'=>$options,'selects'=>$selects]);
    }
    
    public function store(Request $request)
    {   
        $Itemsid=$request->Itemsid;
        $Itemsprice=$request->Itemsprice;
        $Itemsstatus=$request->Itemsstatus;
        $Itemsidcoins=$request->Itemsidcoins;
        $Itemsidrubro=$request->Itemsidrubro;
        $Itemsdescription=$request->Itemsdescription;
        $Itemsabreviation=$request->Itemsabreviation;
     
        $items=Items::updateOrCreate(
            ['id' => $Itemsid], 
            [   
                'price'=> $Itemsprice,
                'status'=> $Itemsstatus,
                'id_coins'=> $Itemsidcoins,
                'hour'=> date('m:s:i'),
                'id_types_items'=> $Itemsidrubro,
                'profile_edit'=> 'ADMINISTRADOR',
                'description' => $Itemsdescription, 
                'abreviation' => $Itemsabreviation
            ]
        );        
        return Response::json($items);
    }
    
    public function edit($id)
    {   
        $coins=Coins::select('a.id','a.symbol')->from('coins as a')->orderby('a.description')->get();
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $types_items=Types_Items::select('a.id','a.description')->from('types_items as a')->orderby('a.description')->get();        
        $items=Items::select('a.id','a.id_types_items','a.description','a.abreviation','a.id_coins','a.price','a.status')->from('items  as a')->join('status_globals  as b','a.status','=','b.id')->join('types_items  as c','a.id_types_items','=','c.id')->join('coins  as d','a.id_coins','=','d.id')->where('a.id','=',$id)->orderBy('a.description', 'asc')->get();
        return Response::json(['items'=>$items,'types_items'=>$types_items,'coins'=>$coins,'status'=>$status]);
    }
    
    public function destroy($id)
    {
        $items = Items::where('id',$id)->delete();
        return Response::json($items);
    }
}