<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ubdepartamento;
use App\ubprovincia;
use App\ubdistrito;
use Illuminate\Support\Facades\Input;

class controllerUbigeos extends Controller
{
    public function lista_departamentos()
    {
    	return ubdepartamento::select('idDepa','departamento')->get();
    }
    public function lista_provincia_x_departamentos()
    {
    	return ubprovincia::select('idProv','provincia','idDepa')->where('idDepa','=',Input::get('iddepa'))->get();
    }
    public function lista_distritos_x_provincia()
    {
    	return ubdistrito::select('idDist','distrito','idProv')->where('idProv','=',Input::get('idProv'))->get();
    }

}
