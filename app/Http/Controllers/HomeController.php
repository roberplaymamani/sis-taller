<?php

namespace App\Http\Controllers;

use Cache;
use App\Home;
use App\Vehicles;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $options='Home';
        $selects='Home';
        $homes=Cache::put('key',Home::select('a.id','a.name','a.description','a.route','a.selects')->from('tables_system as a')->orderby('a.description')->get());
        return view('home',['options'=>$options,'selects'=>$selects]);
    }

    public function index1()
    {
        $options='Home';
        $selects='Home';
        $listVehiculos=Vehicles::select('a.id','a.plate','g.description as category','e.description as mark','f.description as model','a.year','d.description','c.name')->from('vehicles as a')->leftJoin('clients__vehicles as b','a.id','=','b.id_vehicles')->leftJoin('clients as c','b.id_clients','=','c.id')->leftJoin('status_globals as d','a.status','=','d.id')->leftJoin('categories as g','a.category','=','g.id')->leftJoin('marks as e','a.mark','=','e.id')->leftJoin('models as f','a.model','=','f.id')->where('b.status','1')->paginate(10);

        return view('vehicles.index1',['listVehiculos'=>$listVehiculos,'options'=>$options,'selects'=>$selects]);
    }
}
