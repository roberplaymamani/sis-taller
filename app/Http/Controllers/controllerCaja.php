<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class controllerCaja extends Controller
{
    public function cierreCaja()
    {
    	$options = 'caja';
        $selects = 'cierre_caja';
        $lisParametrros='';
        return view('caja.index', ['lisParametrros' => $lisParametrros, 'options' => $options, 'selects' => $selects]);
    }
}
