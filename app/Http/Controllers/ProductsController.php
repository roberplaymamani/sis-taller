<?php
 
namespace App\Http\Controllers;

use App\Coins;
use App\Status;
use App\Client;
use App\Products;
use App\SubProducts;
use App\LinesProducts;
use App\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\ProductsExport;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;
 
class ProductsController extends Controller
{

    public function index()
    {   
        $options='archivos';
        $selects='productos';

        if (Input::get('cant_mostrar')>0) {
           $N_paginador=Input::get('cant_mostrar');
        }else{
            $N_paginador=10;
        }

        if (Input::get('search')=='1') {
            $listproductos= Products::select('a.id','a.id_products','a.description','a.abreviation','d.description as measurement_units','b.description as lines_product','c.description as sublines_product','g.description as brands_repuestos','h.description as country_origin','f.description as status')->from('products as a')->leftJoin('lines_product as b','a.id_lines','=','b.id')->leftJoin('sublines_product as c','a.id_sublines','=','c.id')->leftJoin('measurement_units as d','a.id_measureunit','=','d.id')->leftJoin('status_globals as f','a.status','=','f.id')->leftJoin('brands_repuestos as g','a.id_marks','=','g.id')->leftJoin('country_origin as h','a.id_country','=','h.id')->where('a.description','like',Input::get('valueInput').'%'); 
            $listproductos=$listproductos->orderBy('a.description', 'asc')->paginate($N_paginador);
        }else{
           $listproductos=Products::select('a.id','a.id_products','a.description','a.abreviation','d.description as measurement_units','b.description as lines_product','c.description as sublines_product','g.description as brands_repuestos','h.description as country_origin','f.description as status')->from('products as a')->leftJoin('lines_product as b','a.id_lines','=','b.id')->leftJoin('sublines_product as c','a.id_sublines','=','c.id')->leftJoin('measurement_units as d','a.id_measureunit','=','d.id')->leftJoin('status_globals as f','a.status','=','f.id')->leftJoin('brands_repuestos as g','a.id_marks','=','g.id')->leftJoin('country_origin as h','a.id_country','=','h.id')->orderBy('a.description', 'asc')->paginate($N_paginador);  
        }
        if (request()->ajax()) {
            $elements = (String) \View::make('products.prodElements', [
            'listproductos' => $listproductos,
            ]);
            $pagination = (String) \View::make('products.prodPaginate', [
                'listproductos' => $listproductos,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }  

        return view('products.index',['listproductos'=>$listproductos,'options'=>$options,'selects'=>$selects]);
    }

    public function export() 
    { 
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function create()
    {   
        $options='Users';
        $selects='UsersRegister';
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $privileges=Privileges::select('a.id','a.description')->from('privileges as a')->orderby('a.description')->get();
        return view('user.create',['status'=>$status,'privileges'=>$privileges,'options'=>$options,'selects'=>$selects]);
    }
    
    public function store(Request $request)
    {     
        $file=$request->hasFile('file');
        if ($request->hasFile('file')) {
        	$file=$request->file('file');
        	$name=time().$file->getClientOriginalName(); // no repetirnombre
        	$file->move(public_path().'/img/products/', $name);
        }  else {
            $name=$request->ProductsiamgeAct;
        }
        
        $Productsid=$request->Productsid;
        $ProductsPVP=$request->ProductsPVP;
        $Productstax=$request->Productstax;
        $Productscost=$request->Productscost;
        $Productsgain=$request->Productsgain;
        $Productscode=$request->Productscode;
        $Productsstatus=$request->Productsstatus;
        $Productstaxporc=$request->Productstaxporc;
        $Productsgainporc=$request->Productsgainporc;
        $Productsid_coins=$request->Productsid_coins;
        $Productswarranty=$request->Productswarranty;
        $Productsid_lines=$request->Productsid_lines;
        $Productsid_marks=$request->Productsid_marks;
        $Productsid_country=$request->Productsid_country;
        $Productsid_provider=$request->Productsid_provider;
        $Productsdescription=$request->Productsdescription;
        $Productsabreviation=$request->Productsabreviation;
        $Productsid_sublines=$request->Productsid_sublines;
        $Productsdate_purchase=$request->Productsdate_purchase;
        $Productsid_measureunit=$request->Productsid_measureunit;
        $Productsprice_purchasec=$request->Productsprice_purchasec;
     
        $products=Products::updateOrCreate(
            ['id' => $Productsid],
            [   
                'imagen'=>$name,
                'by_tax'=>$Productstaxporc,
                'by_profit'=>$Productsgainporc,
                'tax'=>$Productstax,
                'PVP'=>$ProductsPVP,
                'cost'=>$Productscost,
                'gain'=>$Productsgain,
                'status'=>$Productsstatus,
                'id_products'=>$Productscode,
                'id_lines'=>$Productsid_lines,
                'id_coins'=>$Productsid_coins,
                'warranty'=>$Productswarranty,
                'id_marks'=>$Productsid_marks,
                'profile_edit'=>'ADMINISTRADOR',
                'id_country'=>$Productsid_country,
                'description'=>$Productsdescription,
                'abreviation'=>$Productsabreviation, 
                'id_sublines'=>$Productsid_sublines,
                'id_provider'=>$Productsid_provider,
                'date_purchase'=>$Productsdate_purchase,
                'id_measureunit'=>$Productsid_measureunit,
                'price_purchase'=>$Productsprice_purchasec
            ]
        );        
        return Response::json($products);
    }
    
    public function edit($id)
    {   
        $coins=Coins::select('a.id','a.description')->from('coins as a')->orderby('a.description')->get();
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $clients=Client::select('a.id','a.name')->from('clients as a')->where([['a.type','=','2'],['a.status','=','1']])->get(); 
        $countryorigin=SubProducts::select('a.id','a.description')->from('country_origin as a')->orderby('a.description')->where('a.status','1')->get(); 
        $subproducts=SubProducts::select('a.id','a.description')->from('sublines_product as a')->orderby('a.description')->where('a.status','1')->get();
        $linesproducts=LinesProducts::select('a.id','a.description')->from('lines_product as a')->orderby('a.description')->where('a.status','1')->get();
        $brandsrepuestos=SubProducts::select('a.id','a.description')->from('brands_repuestos as a')->orderby('a.description')->where('a.status','1')->get();        
        $measurementunits=SubProducts::select('a.id','a.description')->from('measurement_units as a')->orderby('a.description')->where('a.status','1')->get(); 
        $products=Products::select('a.id','a.imagen','a.by_tax','a.by_profit','a.tax','a.PVP','a.cost','a.gain','a.status','a.id_products','a.id_lines','a.id_coins','a.warranty','a.id_marks','a.profile_edit','a.id_country','a.description','a.abreviation','a.id_sublines','a.id_provider','a.date_purchase','a.id_measureunit','a.price_purchase','b.symbol')->from('products as a')->leftJoin('coins as b','a.id_coins','=','b.id')->orderby('a.description')->where('a.id','=',$id)->get(); 
        return Response::json(['products'=>$products,'subproducts'=>$subproducts,'brandsrepuestos'=>$brandsrepuestos,'countryorigin'=>$countryorigin,'linesproducts'=>$linesproducts,'measurementunits'=>$measurementunits,'status'=>$status,'coins'=>$coins,'clients'=>$clients]);
    }
    
    public function subline(Request $request)
    {   
        $id=$request->id;
        $subproducts=SubProducts::select('a.id','a.description')->from('sublines_product as a')->orderby('a.description')->where([['a.id_product','=',$id],['a.status','=','1']])->get();
        return Response::json($subproducts);
    }

    public function coins(Request $request)
    {   
        $id=$request->id;
        $coins=Coins::select('a.id','a.symbol')->from('coins as a')->orderby('a.symbol')->where([['a.id','=',$id],['a.status','=','1']])->get();
        return Response::json($coins);
    }

    public function destroy($id)
    {
        $products=Products::where('id',$id)->delete();
        return Response::json($products);
    }

    public function Donwloadpdf($value='')
    {
        $listproductos=Products::select('a.id','a.id_products','a.description','a.abreviation','d.description as measurement_units','b.description as lines_product','c.description as sublines_product','g.description as brands_repuestos','h.description as country_origin','f.description as status')->from('products as a')->leftJoin('lines_product as b','a.id_lines','=','b.id')->leftJoin('sublines_product as c','a.id_sublines','=','c.id')->leftJoin('measurement_units as d','a.id_measureunit','=','d.id')->leftJoin('status_globals as f','a.status','=','f.id')->leftJoin('brands_repuestos as g','a.id_marks','=','g.id')->leftJoin('country_origin as h','a.id_country','=','h.id')->orderBy('a.description', 'asc')->get();

        $dataCompany = company::select('id', 'rucEmpresa', 'razonsocialEmpresa', 'direccionEmpresa', 'telefonoEmpresa', 'celularEmpresa', 'emailEmpresa', 'codMoneda', 'codFormapago', 'codCondicion', 'IGV', 'Ganancia', 'LinkSunat', 'paginaWeb', 'logoEmpresa', 'codAlmacen')->get();

        foreach ($dataCompany as $key => $valueEmpresa) {
           $logoempresa   = file_get_contents(public_path().'/img/company/'.$valueEmpresa->logoEmpresa);
           $DataEmp['logo'] = base64_encode($logoempresa);
           $DataEmp['name'] =$valueEmpresa->razonsocialEmpresa;
           $DataEmp['ruc'] =$valueEmpresa->rucEmpresa;
        }

        

        $pdf = \PDF::loadView('products.exportPdf',['listproductos'=>$listproductos,'DataEmp'=>$DataEmp]);
        return $pdf->stream();
    }
}