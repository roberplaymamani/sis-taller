<?php
 
namespace App\Http\Controllers;
 
use App\Status;
use App\Client;
use App\ubdistrito;
use App\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\ClientsExport;
use App\Exports\ProvidersExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;
 
class ClientController extends Controller
{
    public function index()
    {   
        $options='archivos';
        $selects='clientes';
        if (Input::get('cant_mostrar')>0) {
           $N_paginador=Input::get('cant_mostrar');
        }else{
            $N_paginador=10;
        }
        if (Input::get('typeClient')=='') {
           $typeClient=1;
        }else{
            $typeClient=Input::get('typeClient');
        }
        if (Input::get('search')=='1') {
            $listaClientes= Client::select('a.id','a.name','a.document','a.email','a.type as idtype','a.phone','a.celphone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('typeClient','=',$typeClient)->where('a.name','like',Input::get('valueInput').'%')->orWhere('a.document','like',Input::get('valueInput').'%'); 
            $listaClientes=$listaClientes->orderBy('a.name', 'asc')->paginate($N_paginador);
        }else{
          $listaClientes= Client::select('a.id','a.name','a.document','a.email','a.type as idtype','a.phone','a.celphone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('typeClient','=',$typeClient)->orderBy('a.name', 'asc')->paginate($N_paginador);  
        }
        if (request()->ajax()) {
            $elements = (String) \View::make('client.clientElements', [
            'listaClientes' => $listaClientes,
            ]);
            $pagination = (String) \View::make('client.clientPaginate', [
                'listaClientes' => $listaClientes,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }  
        return view('client.index',['listaClientes'=>$listaClientes,'options'=>$options,'selects'=>$selects]);
    }
    public function redireccionaraProveedores()
    {
        $options='archivos';
        $selects='proveedores';
        $ListProveedores= Client::select('a.id','a.name','a.document','a.email','a.type as idtype','a.phone','a.celphone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('typeClient','=',2)->orderBy('a.name', 'asc')->paginate(10);
        return view('provider.index',['listaClientes'=>$ListProveedores,'options'=>$options,'selects'=>$selects]);
    }

    public function filterClientsForVehicles()
    {
        if (Input::get('search')=='1') {
            $listaClientes= Client::select('a.id','a.name','a.document','a.type as idtype','a.phone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('a.name','like',Input::get('valueInput').'%')->orWhere('a.document','like',Input::get('valueInput').'%');
            $listaClientes=$listaClientes->orderBy('a.name', 'asc')->paginate(5);
        }else{
          $listaClientes= Client::select('a.id','a.name','a.document','a.type as idtype','a.phone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->orderBy('a.name', 'asc')->paginate(5);  
        }
        if (request()->ajax()) {
            $elements = (String) \View::make('vehicles.client_vehic_Elements', [
            'listaClientes' => $listaClientes,
            ]);
            $pagination = (String) \View::make('vehicles.client_vehic_Paginate', [
                'listaClientes' => $listaClientes,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        } 
    }

    public function export() 
    { 
        return Excel::download(new ClientsExport, 'clients.xlsx');
    }

     public function export_providers() 
    { 
        return Excel::download(new ProvidersExport, 'Proveedores_'.date('Y-m-d').'.xlsx');
    }

    public function search()
    {   
        if(request()->ajax()) {
            return datatables()->of(Client::select('a.id','a.name','a.document','a.type as idtype','a.phone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->get())
            ->addColumn('action', 'action/clientSearch_action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
       
        return view('client.search');
    }
    
    public function store(Request $request)
    {   
        //return $request;
        $Clientid = $request->Clientsid;
        $Clientname = $request->Clientname;
        $Clienttype = $request->Clienttype;
        $Clientemail = $request->Clientemail;
        $Clientphone = $request->Clientphone;
        $Clientstatus = $request->Clientstatus;
        $Clientcontact = $request->Clientcontact;
        $Clientaddress = $request->Clientaddress;
        $Clientdocument = $request->Clientdocument;
        $Clientbirthdate = $request->Clientbirthdate;
        $Clientanniversary = $request->Clientanniversary;
        $Clientobservations = $request->Clientobservations;
        
        $client = Client::updateOrCreate(
            ['id' => $Clientid],
            [
                'name' => $Clientname,
                'type' => $Clienttype,
                'email' => $Clientemail,
                'phone' => $Clientphone,
                'status' => $Clientstatus,
                'contact' => $Clientcontact,
                'address' => $Clientaddress,
                'profile' => 'Administrador',
                'document' => $Clientdocument,
                'birthdate' => $Clientbirthdate,
                'anniversary' => $Clientanniversary,
                'observations' => $Clientobservations,
                'celphone' => $request->celphone,
                'typeClient'=>$request->typeClient,
                'idDepa' => $request->idDepa,
                'idProv' => $request->idProv,
                'idDist' => $request->idDist

            ]
        ); 
        return Response::json($client);
    }
    
    public function edit($id)
    {   
        $where = array('id' => $id);
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $client =Client::select('a.id','a.name','a.document','a.type','a.address','a.phone','a.email','a.contact','a.birthdate','a.anniversary','a.observations','a.status','b.description','a.celphone','a.typeClient','a.idDepa','a.idProv','a.idDist')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('a.id',$id)->first();
        return Response::json(['client'=>$client,'status'=>$status]);
    }
    
    public function destroy($id)
    {
        $client = Client::where('id',$id)->delete();
        return Response::json($client);
    }
    public function Donwloadpdf($typeid)
    {

        $listaClientes= Client::select('a.id','a.name','a.document','a.email','a.type as idtype','a.phone','a.celphone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->where('typeClient','=',$typeid)->orderBy('a.name', 'asc')->get();

        $dataCompany = company::select('id', 'rucEmpresa', 'razonsocialEmpresa', 'direccionEmpresa', 'telefonoEmpresa', 'celularEmpresa', 'emailEmpresa', 'codMoneda', 'codFormapago', 'codCondicion', 'IGV', 'Ganancia', 'LinkSunat', 'paginaWeb', 'logoEmpresa', 'codAlmacen')->get();

       foreach ($dataCompany as $key => $valueEmpresa) {
           $logoempresa   = file_get_contents(public_path().'/img/company/'.$valueEmpresa->logoEmpresa);
           $DataEmp['logo'] = base64_encode($logoempresa);
           $DataEmp['name'] =$valueEmpresa->razonsocialEmpresa;
           $DataEmp['ruc'] =$valueEmpresa->rucEmpresa;
        }

        $DataEmp['type'] =($typeid=='1')?'CLINETES':'PROVEEDORES';

 
        $pdf = \PDF::loadView('client.exportPdf',['listaClientes'=>$listaClientes,'DataEmp'=>$DataEmp]);
        return $pdf->stream();
    }
}