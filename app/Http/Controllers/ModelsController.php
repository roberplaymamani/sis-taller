<?php
 
namespace App\Http\Controllers;
 
use App\Marks;
use App\Models;
use App\Status;
use Illuminate\Http\Request;
use App\Exports\ModelsExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;
 
class ModelsController extends Controller
{

    public function index()
    {   
        $options='system';
        $selects='ModelsList';
        if(request()->ajax()) {
            return datatables()->of(Models::select('a.id','c.description as marks','a.description','a.abreviation','b.description as status')->from('models as a')->join('status_globals as b','a.status','=','b.id')->join('marks as c','a.idmarks','=','c.id')->orderBy('c.description', 'asc')->get())
            ->addColumn('action', 'action/models_action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        
        return view('models.index',['options'=>$options,'selects'=>$selects]);
    }

    public function export() 
    { 
        return Excel::download(new ModelsExport, 'models.xlsx');
    }

    public function create()
    {   
        $options='Users';
        $selects='UsersRegister';
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $privileges=Privileges::select('a.id','a.description')->from('privileges as a')->orderby('a.description')->get();
        return view('user.create',['status'=>$status,'privileges'=>$privileges,'options'=>$options,'selects'=>$selects]);
    }
    
    public function store(Request $request)
    {      
        $Modelsid=$request->Modelsid;
        $Modelsstatus=$request->Modelsstatus;
        $Modelsidmark=$request->Modelsidmark;
        $Modelsdescription=$request->Modelsdescription;
        $Modelsabreviation=$request->Modelsabreviation;
     
        $models=Models::updateOrCreate(
            ['id' => $Modelsid],
            [
                'status'=> $Modelsstatus,
                'idmarks'=> $Modelsidmark,
                'profile_edit'=> 'ADMINISTRADOR',
                'description' => $Modelsdescription, 
                'abreviation' => $Modelsabreviation
            ]
        );        
        return Response::json($models);
    }
    
    public function edit($id)
    {   
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $marks=Marks::select('a.id','a.description')->from('marks as a')->orderby('a.description')->get();
        $models=Models::select('a.id','a.idmarks','a.description','a.abreviation','a.status')->from('models as a')->orderby('a.description')->where('a.id','=',$id)->get();        
        return Response::json(['models'=>$models,'marks'=>$marks,'status'=>$status]);
    }
        
    public function destroy($id)
    {
        $models = Models::where('id',$id)->delete();
        return Response::json($models);
    }
}