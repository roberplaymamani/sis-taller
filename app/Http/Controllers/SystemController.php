<?php
 
namespace App\Http\Controllers;

use DB;
use Cache;
use App\System;
use App\Status;
use App\Advisors;
use App\Mechanics;
use App\Models;
use App\Coins;
use App\MovementsCash;
use App\SubProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\SystemsExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;

class SystemController extends Controller
{
    
    public function index(Request $request)
    {   
    
    //return  $request;
        if (isset($request->bd)) {
            $bd=$request->bd;
        }else{
            $bd='store';
        }
        $cantCampos  = 4;
        $nombreCampo = '';
        $tableName   = '';
        

        $options='archivos';
        $selects='tablas';
        
        switch ($bd) {
            case 'advisors_service':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','mobile as campo4')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')->where('a.description','like',Input::get('valueInput').'%'); 
                $DataSymstem=$DataSymstem->orderBy('a.description', 'asc')->paginate(10);
                $nombreCampo='No. Celular';
                break;
            case 'mechanics':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','co.description as campo4','co.id as campo4_1')->from($bd.' as a')
                ->join('status_globals as b','a.status','=','b.id')
                ->join('commission as co','a.id_commission','=','co.id')
                ->where('a.description','like',Input::get('valueInput').'%'); 
                $DataSymstem=$DataSymstem->orderBy('a.description', 'asc')->paginate(10);
                $nombreCampo='Comisión';
                $tableName   = 'commission';
                break;
            case 'models':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','ma.description as campo4','ma.id as campo4_1')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('marks as ma','a.idmarks','=','ma.id')
                    ->where('a.description','like',Input::get('valueInput').'%'); 
                $DataSymstem=$DataSymstem->orderBy('a.id', 'desc')->paginate(10);
                $nombreCampo='Marca';
                $tableName   = 'marks';
                break;
            case 'coins':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','symbol as campo4')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')->where('a.description','like',Input::get('valueInput').'%'); 
                    $DataSymstem=$DataSymstem->orderBy('a.id', 'desc')->paginate(10);
                $nombreCampo='Simbolo';
                $tableName   = '';
                break;
            case 'movements_cash':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','tm.description as campo4','tm.id as campo4_1')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('type_movement as tm','a.id_type_movement','=','tm.id')
                    ->where('a.description','like',Input::get('valueInput').'%'); 
                    $DataSymstem=$DataSymstem->orderBy('a.id', 'desc')->paginate(10);
                $nombreCampo='Tipo Movimiento';
                $tableName   = 'type_movement';
                break;
            case 'sublines_product':
                $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status','lp.description as campo4','lp.id as campo4_1')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('lines_product as lp','a.id_product','=','lp.id')
                    ->where('a.description','like',Input::get('valueInput').'%'); 
                    $DataSymstem=$DataSymstem->orderBy('a.id', 'desc')->paginate(10);
                $nombreCampo='Producto';
                $tableName   = 'lines_product';
                break;
            default:
                if (Input::get('search')=='1') {
                    $DataSymstem= System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')->where('a.description','like',Input::get('valueInput').'%'); 
                    $DataSymstem=$DataSymstem->orderBy('a.id', 'desc')->paginate(10);
                }else{
                   $DataSymstem=System::select('a.id','a.description','a.abreviation','a.date_edit as date','b.description as status')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')
                   ->paginate(10);  
                   $cantCampos=3;
                }
                $cantCampos=3;
                break;
        }

        if (request()->ajax()) {
             $elements = (String) \View::make('system.systemElements', [
            'DataSymstem' => $DataSymstem,
            ]);
             $pagination = (String) \View::make('system.systemPaginate', [
                'DataSymstem' => $DataSymstem,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination,'cantCampos'=>$cantCampos,'nombreCampo'=>$nombreCampo,'tableName'=>$tableName));
        } 
       return view('system.index',['DataSymstem'=>$DataSymstem,'options'=>$options,'selects'=>$selects]);
    }
    public function list_tables()
    {
        $bd=Input::get('db');
        $tablasList=System::select('a.id','a.description')->from($bd.' as a')->get();
        return Response::json(['tablasList'=>$tablasList]);
    }
    
    public function export() 
    { 
        return Excel::download(new SystemsExport,'systems.xlsx');
    }

    public function create()
    {   
        $options='Vehicle';
        $selects='VehRegister';
        $marks=Marks::select('a.id','a.description')->from('marks as a')->orderby('a.description')->get();
        $categories=Categories::select('a.id','a.description')->from('categories as a')->orderby('a.description')->get();
        return view('vehicles.create',['marks'=>$marks,'categories'=>$categories,'options'=>$options,'selects'=>$selects]);
    }

    public function store(Request $request)
    {  
        //return $request;
        $btn=$request->btn;
        $bd=$request->DBactual;
        $Systemid=$request->Systemid;
        $Systemstatus=$request->Systemstatus;
        $Systemdescription=$request->Systemdescription;
        $Systemabreviation=$request->Systemabreviation;

        switch ($bd) {
            case 'advisors_service':
                $vehicles   =   Advisors::updateOrCreate(
                    ['id' => $Systemid],
                    [
                        'mobile'=> $request->campo4,
                        'status'=> $Systemstatus,
                        'profile_edit'=> 'ADMINISTRADOR',
                        'description' => $Systemdescription, 
                        'abreviation' => $Systemabreviation
                    ]
                );
                break;
            case 'mechanics':
                $vehicles=Mechanics::updateOrCreate(
                    ['id' => $Systemid], 
                    [   
                        'status'=> $Systemstatus,
                        'profile_edit'=> 'ADMINISTRADOR',
                        'abreviation' => $Systemabreviation,
                        'description' => $Systemdescription, 
                        'id_commission'=> $request->campo4_1,
                    ]
                ); 
                break;
            case 'models':
                $vehicles=Models::updateOrCreate(
                    ['id' => $Systemid],
                    [
                        'status'=> $Systemstatus,
                        'idmarks'=> $request->campo4_1,
                        'profile_edit'=> 'ADMINISTRADOR',
                        'description' => $Systemdescription, 
                        'abreviation' => $Systemabreviation
                    ]
                ); 
                break;
            case 'coins':
                $vehicles   =   Coins::updateOrCreate(
                    ['id' => $Systemid],
                    [
                        'symbol'=> $request->campo4,
                        'status'=> $Systemstatus,
                        'profile_edit'=> 'ADMINISTRADOR',
                        'description' => $Systemdescription, 
                        'abreviation' => $Systemabreviation
                    ]
                );  
                break;
            case 'movements_cash':
                $vehicles=MovementsCash::updateOrCreate(
                    ['id' => $Systemid], 
                    [   
                        'status'=> $Systemstatus,
                        'profile_edit'=> 'ADMINISTRADOR',
                        'id_type_movement'=> $request->campo4,
                        'abreviation' => $Systemabreviation,
                        'description' => $Systemdescription
                    ]
                );   
                break;
            case 'sublines_product':
                $vehicles=SubProducts::updateOrCreate(
                    ['id' => $Systemid],
                    [
                        'status'       => $Systemstatus,
                        'profile_edit' => 'ADMINISTRADOR',
                        'id_product'   => $request->campo4,
                        'description'  => $Systemdescription, 
                        'abreviation'  => $Systemabreviation
                    ]
                );
                break;
            default:
                if($Systemid){
                        $vehicles=DB::update('update '.$bd.' set status='.$Systemstatus.',description="'.$Systemdescription.'",abreviation="'.$Systemabreviation.'" where id = ?', [$Systemid]);
                } else {
                    $vehicles=DB::insert('insert into '.$bd.' (description,abreviation,profile_edit,status) values (?,?,?,?)', ["$Systemdescription","$Systemabreviation",'Administrador',$Systemstatus]);
                }
                break;
        }
        
        
        return Response::json($vehicles);
    }
    
    public function edit(Request $request)
    {  

        $cantCampos  = 4;
        $nombreCampo = '';

        $id=$request->id;
        $bd=$request->bd;
        
        switch ($bd) {
            case 'advisors_service':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','mobile as campo4','a.status')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')->where('a.id','=',$id);
                $system=$system->orderBy('a.description', 'asc')->get();
                
                break;
            case 'mechanics':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','co.description as campo4','co.id as campo4_1','a.status')->from($bd.' as a')
                ->join('status_globals as b','a.status','=','b.id')
                ->join('commission as co','a.id_commission','=','co.id')
                ->where('a.id','=',$id);
                $system=$system->orderBy('a.description', 'asc')->get();
               
                break;
            case 'models':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','ma.description as campo4','ma.id as campo4_1','a.status')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('marks as ma','a.idmarks','=','ma.id')
                    ->where('a.id','=',$id);
                $system=$system->orderBy('a.id', 'desc')->get();
                
                break;
            case 'coins':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','a.status','symbol as campo4')->from($bd.' as a')->join('status_globals as b','a.status','=','b.id')->where('a.id','=',$id);
                $system=$system->orderBy('a.id', 'desc')->get();
               
                break;
            case 'movements_cash':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','tm.description as campo4','tm.id as campo4_1','a.status')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('type_movement as tm','a.id_type_movement','=','tm.id')
                    ->where('a.id','=',$id);
                $system=$system->orderBy('a.id', 'desc')->get();
                
                break;
            case 'sublines_product':
                $system= System::select('a.id','a.description','a.abreviation','a.date_edit as date','lp.description as campo4','lp.id as campo4_1','a.status')->from($bd.' as a')
                    ->join('status_globals as b','a.status','=','b.id')
                    ->join('lines_product as lp','a.id_product','=','lp.id')
                    ->where('a.id','=',$id);
                $system=$system->orderBy('a.id', 'desc')->get();
               
                break;
            default:
                $system=System::select('a.id','a.description','a.abreviation','a.date_edit as date','a.status')->from($bd.' as a')->orderby('a.description')->where('a.id','=',$id)->get();
                $cantCampos=3;
                break;
        }

        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        return Response::json(['status'=>$status,'system'=>$system,'cantCampos'=>$cantCampos]);
    }
    
    public function destroy(Request $request)
    { 
        $id=$request->id;
        $bd=$request->bd;
        $system=System::from($bd)->where('id',$id)->delete();
        return Response::json($system);
    }

    public function models(Request $request)
    {   
        $id=$request->id;
        $models=Models::select('a.id','a.description')->from('models as a')->orderby('a.description')->where([['a.idmarks','=',$id],['a.status','=','1']])->get();
        return Response::json($models);
    }
}