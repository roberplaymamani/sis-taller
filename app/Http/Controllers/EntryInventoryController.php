<?php

namespace App\Http\Controllers;

use App\Coins;
use App\EntryDetInventory;
use App\EntryInventory;
use App\Exports\EntryInventoryExport;
use App\Products;
use App\Status;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Response;

session_start();
class EntryInventoryController extends Controller
{

    public function index()
    {
        $options = 'inventory';
        $selects = 'EntryInventoryList';
        /*if(request()->ajax()) {
        return datatables()->of(EntryInventory::select('a.id','a.num_guide as description','c.description as motive','a.value_sale','a.taxt','a.total_net','a.date_registration','b.description as status')->from('entry_inventorys as a')->join('status_globals as b','a.status','=','b.id')->join('status_inventorys as c','a.entry_exit','=','c.id')->orderBy('a.num_guide', 'asc')->get())
        ->addColumn('action', 'action/entry_action')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
        }*/

        if (Input::get('cant_mostrar') > 0) {
            $N_paginador = Input::get('cant_mostrar');
        } else {
            $N_paginador = 10;
        }

        if (Input::get('search') == '1') {
            #filtro por campos
            $listainventario = EntryInventory::select('a.id', 'a.num_guide as description', 'c.description as motive', 'a.value_sale', 'a.taxt', 'a.total_net', 'a.date_registration', 'b.description as status')->from('entry_inventorys as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('status_inventorys as c', 'a.entry_exit', '=', 'c.id');
            if (Input::get('valueInput') != '') {
                $listainventario = $listainventario->where('a.num_guide', 'like', Input::get('valueInput') . '%')
                    ->orWhere('c.description', 'like', Input::get('valueInput') . '%');
            }
            if (Input::get('valuedate') != '') {
                $listainventario = $listainventario->where('a.date_registration', 'like', Input::get('valuedate') . '%');
            }
            $listainventario = $listainventario->orderBy('a.num_guide', 'desc')->paginate($N_paginador);
        } else {
            #rederigir a vista inventario fecha=now()
            $listainventario = EntryInventory::select('a.id', 'a.num_guide as description', 'c.description as motive', 'a.value_sale', 'a.taxt', 'a.total_net', 'a.date_registration', 'b.description as status')->from('entry_inventorys as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('status_inventorys as c', 'a.entry_exit', '=', 'c.id')->orderBy('a.num_guide', 'asc')->paginate($N_paginador);
        }
        //return $listainventario;
        /* $id=2;
        $name=2;

        $_SESSION['detalleingreso_salida'][$id]=[
        'id'=>$id,
        'name'=>$name,
        ];

        $nombre='xyz';
        $id=1;

        $carrito[$id]=\Session::put('carrito',
        array(
        'identificador' => $id,
        'nombre' => $nombre
        )
        );*/

        if (request()->ajax()) {
            $elements = (String) \View::make('entryinventory.invElements', [
                'listainventario' => $listainventario,
            ]);
            $pagination = (String) \View::make('entryinventory.invPaginate', [
                'listainventario' => $listainventario,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }

        return view('entryinventory.index', ['listainventario' => $listainventario, 'options' => $options, 'selects' => $selects]);
    }

    public function search(Request $request)
    {
        $id = $request->id;
        /*if(request()->ajax()) {
        return datatables()->of(EntryInventory::select('a.id','b.id_products','b.description','a.quantity','a.price','a.total')->from('det_entry_inventorys as a')->join('products as b','a.id_products','=','b.id')->where('a.id_entry_inventorys','=',$id)->orderBy('b.description', 'asc')->get())
        ->addColumn('action', 'action/entryedit_action')
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
        }

        return view('entryinventory.edit');  */
        $listDetallesIngresoSalida = EntryInventory::select('a.id', 'b.id_products', 'b.description', 'a.quantity', 'a.price', 'a.total')->from('det_entry_inventorys as a')->join('products as b', 'a.id_products', '=', 'b.id')->where('a.id_entry_inventorys', '=', $id)->orderBy('b.description', 'asc')->get();

        if (request()->ajax()) {
            $elements = (String) \View::make('entryinventory.inv_addElements', [
                'listDetallesIngresoSalida' => $listDetallesIngresoSalida,
            ]);
            return Response::json(array('html' => $elements));
        }
    }

    public function sum(Request $request)
    {
        $id             = $request->id;
        $entryinventory = EntryInventory::select('a.id', 'b.id_products', 'b.description', 'a.quantity', 'a.price', 'a.total')->from('det_entry_inventorys as a')->join('products as b', 'a.id_products', '=', 'b.id')->where('a.id_entry_inventorys', '=', $id)->orderBy('b.description', 'asc')->get();
        return Response::json(['entryinventory' => $entryinventory]);
    }

    public function export()
    {
        return Excel::download(new EntryInventoryExport, 'Inventory.xlsx');
    }

    public function store(Request $request)
    {
        $EntryInventoryid         = $request->EntryInventoryid;
        $EntryExitInventory       = $request->EntryExitInventory;
        $EntryInventorytaxt       = $request->EntryInventorytaxt;
        $EntryInventorydate       = $request->EntryInventorydate;
        $id_entry_inventorys      = $request->id_entry_inventoryss;
        $EntryInventorystatus     = $request->EntryInventorystatus;
        $EntryInventoryid_store   = $request->EntryInventoryid_store;
        $EntryInventoryid_coins   = $request->EntryInventoryid_coins;
        $EntryInventorytotal_net  = $request->EntryInventorytotal_net;
        $EntryInventoryid_reasons = $request->EntryInventoryid_reasons;
        $EntryInventoryvalue_sale = $request->EntryInventoryvalue_sale;

        $entryinventory = EntryInventory::updateOrCreate(
            ['id' => $EntryInventoryid],
            [
                'taxt'              => $EntryInventorytaxt,
                'status'            => $EntryInventorystatus,
                'profile_edit'      => 'ADMINISTRADOR',
                'entry_exit'        => $EntryExitInventory,
                'id_store'          => $EntryInventoryid_store,
                'id_coins'          => $EntryInventoryid_coins,
                'total_net'         => $EntryInventorytotal_net,
                'value_sale'        => $EntryInventoryvalue_sale,
                'id_reasons'        => $EntryInventoryid_reasons,
                'date_registration' => $EntryInventorydate,
            ]
        );

        if ($EntryInventoryid) {
            $EntryInventoryid = $EntryInventoryid;
        } else {
            $EntryInventoryid = $entryinventory->id;
            $longitud         = strlen($EntryInventoryid);

            switch ($longitud) {
                case '1':$num_guide = '001-00000' . $EntryInventoryid;
                    break;
                case '2':$num_guide = '001-0000' . $EntryInventoryid;
                    break;
                case '3':$num_guide = '001-000' . $EntryInventoryid;
                    break;
                case '4':$num_guide = '001-00' . $EntryInventoryid;
                    break;
                case '5':$num_guide = '001-0' . $EntryInventoryid;
                    break;
                case '6':$num_guide = '001-' . $EntryInventoryid;
                    break;
            }
            //dump('update entry_inventorys set num_guide="'.$num_guide.'" where id = ?', [$EntryInventoryid]);exit();

            $entryinventoryedit = DB::update('update entry_inventorys set num_guide="' . $num_guide . '" where id = ?', [$EntryInventoryid]);
            $entrydetinventory  = DB::update('update det_entry_inventorys set id_entry_inventorys=' . $EntryInventoryid . ' where id_entry_inventorys = ?', [$id_entry_inventorys]);
        }

        return Response::json(['entryinventory' => $entryinventory, 'entrydetinventory' => $entrydetinventory, 'entryinventoryedit' => $entryinventoryedit]);
    }

    public function newitems(Request $request)
    {
        $id_entry                  = $request->id_entry;
        $id_entry_inventorys       = $request->id_entry_inventorys;
        $EntryInventoryprice       = $request->EntryInventoryprice;
        $EntryInventorytotal       = $request->EntryInventorytotal;
        $EntryInventoryquantity    = $request->EntryInventoryquantity;
        $EntryInventorydescription = $request->EntryInventorydescription;

        $entrydetinventory = EntryDetInventory::updateOrCreate(
            ['id' => $id_entry],
            [
                'price'               => $EntryInventoryprice,
                'total'               => $EntryInventorytotal,
                'quantity'            => $EntryInventoryquantity,
                'id_products'         => $EntryInventorydescription,
                'id_entry_inventorys' => $id_entry_inventorys,
            ]
        );
        return Response::json($entrydetinventory);
    }

    public function edit($id)
    {
        $coins    = Coins::select('a.id', 'a.description')->from('coins as a')->orderby('a.description')->get();
        $store    = Status::select('a.id', 'a.description')->from('store as a')->orderby('a.description')->get();
        $status   = Status::select('a.id', 'a.description')->from('status_globals as a')->orderby('a.description')->get();
        $products = Products::select('a.id', 'a.description','a.id_products')->from('products as a')->orderby('a.description')->get();
        $exit     = Status::select('a.id', 'a.description')->from('motives_outputs_warehouse as a')->orderby('a.description')->get();
        $entry    = Status::select('a.id', 'a.description')->from('reasons_revenue_warehouse as a')->orderby('a.description')->get();
        $entryNew = Status::select('a.id', 'a.id_products', 'a.quantity', 'a.price', 'a.total', 'b.id_products as codigo', 'a.id_entry_inventorys')->from('det_entry_inventorys as a')->join('products as b', 'a.id_products', '=', 'b.id')->where('a.id', '=', $id)->orderby('a.id_products')->get();
        return Response::json(['coins' => $coins, 'store' => $store, 'status' => $status, 'entry' => $entry, 'exit' => $exit, 'products' => $products, 'entryNew' => $entryNew]);
    }

    public function products(Request $request)
    {
        $id       = $request->id;
        $search   = $request->search;
        $products = Products::select('a.id', 'a.id_products', 'a.cost')->from('products as a')->where([[$search, '=', $id], ['a.status', '=', '1']])->orderby('a.id_products')->get();
        return Response::json($products);
    }

    public function destroy($id)
    {
        $entrydetinventory = EntryDetInventory::where('id', $id)->delete();
        return Response::json($entrydetinventory);
    }
    public function Donwloadpdf($seach, $dateadd)
    {

        $listainventario = EntryInventory::select('a.id', 'a.num_guide as description', 'c.description as motive', 'a.value_sale', 'a.taxt', 'a.total_net', 'a.date_registration', 'b.description as status')->from('entry_inventorys as a')->join('status_globals as b', 'a.status', '=', 'b.id')->join('status_inventorys as c', 'a.entry_exit', '=', 'c.id');
        if ($seach != '0') {
            $listainventario = $listainventario->where('a.num_guide', 'like', Input::get('valueInput') . '%')
                ->orWhere('c.description', 'like', Input::get('valueInput') . '%');
        }
        if ($dateadd != '0') {
            $listainventario = $listainventario->where('a.date_registration', 'like', Input::get('valuedate') . '%');
        }
        $listainventario = $listainventario->orderBy('a.num_guide', 'desc')->get();

        $logoempresa     = file_get_contents(public_path() . '/template/img/logodsd.png');
        $DataEmp['logo'] = base64_encode($logoempresa);
        $DataEmp['name'] = 'DSDINFORMATICOS SAC';
        $DataEmp['ruc']  = '10707988661';

        $pdf = \PDF::loadView('entryinventory.exportPdf', ['listainventario' => $listainventario, 'DataEmp' => $DataEmp]);
        return $pdf->stream();

    }
}
