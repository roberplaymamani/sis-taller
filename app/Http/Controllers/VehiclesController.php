<?php
 
namespace App\Http\Controllers;

use App\Marks;
use App\Models;
use App\Status;
use App\Vehicles;
use App\Categories;
use App\Client;
use App\company;
use App\Clients_Vehicles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\VehiclesExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Redirect,Response;
 
class VehiclesController extends Controller
{
    public function index()
    {   
        $options='archivos';
        $selects='vehiculos';
    
        if (Input::get('cant_mostrar')>0) {
           $N_paginador=Input::get('cant_mostrar');
        }else{
            $N_paginador=10;
        }
        if (Input::get('search')=='1') {
            $listVehiculos= Vehicles::select('a.id','a.plate','g.description as category','e.description as mark','f.description as model','a.year','d.description','c.name')->from('vehicles as a')->leftJoin('clients__vehicles as b','a.id','=','b.id_vehicles')->leftJoin('clients as c','b.id_clients','=','c.id')->leftJoin('status_globals as d','a.status','=','d.id')->leftJoin('categories as g','a.category','=','g.id')->leftJoin('marks as e','a.mark','=','e.id')->leftJoin('models as f','a.model','=','f.id')->where('b.status','1')->where('a.plate','like',Input::get('valueInput').'%'); 
            $listVehiculos=$listVehiculos->paginate($N_paginador);
        }else{
               $listVehiculos=Vehicles::select('a.id','a.plate','g.description as category','e.description as mark','f.description as model','a.year','d.description','c.name')->from('vehicles as a')->leftJoin('clients__vehicles as b','a.id','=','b.id_vehicles')->leftJoin('clients as c','b.id_clients','=','c.id')->leftJoin('status_globals as d','a.status','=','d.id')->leftJoin('categories as g','a.category','=','g.id')->leftJoin('marks as e','a.mark','=','e.id')->leftJoin('models as f','a.model','=','f.id')->where('b.status','1')->paginate($N_paginador); 
        }
        if (request()->ajax()) {
            $elements = (String) \View::make('vehicles.vehiculoElements', [
            'listVehiculos' => $listVehiculos,
            ]);
            $pagination = (String) \View::make('vehicles.vehiculoPaginate', [
                'listVehiculos' => $listVehiculos,
            ]);
            return Response::json(array('html' => $elements, 'pagenationAdd' => $pagination));
        }
        $listaClientes= Client::select('a.id','a.name','a.document','a.type as idtype','a.phone','c.description as type','b.description')->from('clients as a')->join('status_globals as b','a.status','=','b.id')->join('type_document as c','a.type','=','c.id')->orderBy('a.name', 'asc')->paginate(5);   
         return view('vehicles.index',['listVehiculos'=>$listVehiculos,'listaClientes'=>$listaClientes,'options'=>$options,'selects'=>$selects]);

    }
    
    public function export() 
    { 
        return Excel::download(new VehiclesExport, 'vehicles.xlsx');
    }

    public function create()
    {   
        $options='Vehicle';
        $selects='VehRegister';
        $marks=Marks::select('a.id','a.description')->from('marks as a')->orderby('a.description')->get();
        $categories=Categories::select('a.id','a.description')->from('categories as a')->orderby('a.description')->get();
        return view('vehicles.create',['marks'=>$marks,'categories'=>$categories,'options'=>$options,'selects'=>$selects]);
    }

    public function store(Request $request)
    {  
        $Vehid=$request->Vehid;
        $Vehyear=$request->Vehyear;
        $Vehmark=$request->Vehmark;
        $Vehmodel=$request->Vehmodel;
        $Vehmotor=$request->Vehmotor;
        $Vehserie=$request->Vehserie;
        $Vehplate=$request->Vehplate;
        $Clientid=$request->Clientid;
        $Vehcolour=$request->Vehcolour;
        $Vehstatus=$request->Vehstatus;
        $Vehcategory=$request->Vehcategory;
        $ClientidNew=$request->ClientidNew;
        $Vehobservations=$request->Vehobservations;
        
        $vehicles=Vehicles::updateorcreate(
            ['id'=>$Vehid],
            [   
                'year'=>$Vehyear,
                'mark'=>$Vehmark,
                'model'=>$Vehmodel,
                'plate'=>$Vehplate,
                'status'=>$Vehstatus,
                'colour'=>$Vehcolour,
                'category'=>$Vehcategory,
                'number_engine'=>$Vehmotor,
                'number_series'=>$Vehserie,
                'profile' => 'Administrador',
                'observations'=>$Vehobservations
            ]
        ); 
        
        if($Vehid) { $Vehid=$Vehid; }  else { $Vehid=$vehicles->id;}          
        
        if($Clientid && !$ClientidNew) {
            
            $clients_vehicles=Clients_Vehicles::create(['id_vehicles'=>$Vehid,'id_clients'=>$Clientid]);
            return Response::json(['clients_vehicles'=>$clients_vehicles]);
        
        } else {

            if($ClientidNew) {
                if($Clientid!=$ClientidNew) {
                    // Modifico en la tabla de intermedia clients_vehicles para inactivar el status
                    $clients_vehicles=Clients_Vehicles::where([['id_clients','=',$Clientid],['id_vehicles','=',$Vehid]])->update(['status'=>'0']);
                    $clients_vehicles=Clients_Vehicles::create(['id_vehicles'=>$Vehid,'id_clients'=>$ClientidNew]);
                    return Response::json(['clients_vehicles'=>$clients_vehicles]);
                } else {
                    return Response::json($vehicles);
                } 
            } else {
                return Response::json($vehicles);
            }
        }
        return Response::json($vehicles);
    }
    
    public function edit($id)
    {   
        $status=1;
        $where=array('id'=>$id);
        $vehicles=Vehicles::select('a.id','a.plate','a.category','a.mark','a.model','a.colour','a.year','a.number_engine','a.number_series','a.observations','a.status','c.id as Clientid','c.name','c.type','c.document','d.description')->from('vehicles as a')->leftJoin('clients__vehicles as b','a.id','=','b.id_vehicles')->leftJoin('clients as c','b.id_clients','=','c.id')->leftJoin('status_globals as d','a.status','=','d.id')->where([['a.id','=',$id],['b.status','=',$status]])->first();
        $status=Status::select('a.id','a.description')->from('status_globals as a')->orderby('a.description')->get();
        $type=Status::select('a.id','a.description')->from('type_document as a')->orderby('a.description')->get();
        $marks=Marks::select('a.id','a.description')->from('marks as a')->orderby('a.description')->get();
        $categories=Categories::select('a.id','a.description')->from('categories as a')->orderby('a.description')->get();
        if($id > 0) {
            $models=Models::select('a.id','a.description')->from('models as a')->orderby('a.description')->where('a.idmarks','=',$vehicles->mark)->get();   
        } else {
            $models=Models::select('a.id','a.description')->from('models as a')->orderby('a.description')->get();   
        }
         
        return Response::json(['vehicles'=> $vehicles,'marks'=> $marks,'models'=> $models,'categories'=> $categories,'status'=>$status,'type'=>$type]);
    }
    
    public function destroy($id)
    {
        $Vehicles=Vehicles::where('id',$id)->delete();
        return Response::json($Vehicles);
    }

    public function models(Request $request)
    {   
        $id=$request->id;
        $models=Models::select('a.id','a.description')->from('models as a')->orderby('a.description')->where([['a.idmarks','=',$id],['a.status','=','1']])->get();
        return Response::json($models);
    }
    public function Donwloadpdf($value='')
    {
        $listVehiculos=Vehicles::select('a.id','a.plate','g.description as category','e.description as mark','f.description as model','a.year','d.description','c.name')->from('vehicles as a')->leftJoin('clients__vehicles as b','a.id','=','b.id_vehicles')->leftJoin('clients as c','b.id_clients','=','c.id')->leftJoin('status_globals as d','a.status','=','d.id')->leftJoin('categories as g','a.category','=','g.id')->leftJoin('marks as e','a.mark','=','e.id')->leftJoin('models as f','a.model','=','f.id')->where('b.status','1')->get();

        $dataCompany = company::select('id', 'rucEmpresa', 'razonsocialEmpresa', 'direccionEmpresa', 'telefonoEmpresa', 'celularEmpresa', 'emailEmpresa', 'codMoneda', 'codFormapago', 'codCondicion', 'IGV', 'Ganancia', 'LinkSunat', 'paginaWeb', 'logoEmpresa', 'codAlmacen')->get();

       foreach ($dataCompany as $key => $valueEmpresa) {
           $logoempresa   = file_get_contents(public_path().'/img/company/'.$valueEmpresa->logoEmpresa);
           $DataEmp['logo'] = base64_encode($logoempresa);
           $DataEmp['name'] =$valueEmpresa->razonsocialEmpresa;
           $DataEmp['ruc'] =$valueEmpresa->rucEmpresa;
        }

        $pdf = \PDF::loadView('vehicles.exportPdf',['listVehiculos'=>$listVehiculos,'DataEmp'=>$DataEmp]);
        return $pdf->stream();
    }
}