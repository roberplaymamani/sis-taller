<?php

namespace App\Exports;

use App\Products;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductsExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    public function Collection()
    {   
        return Products::select('a.id_products','a.description','a.abreviation','d.description as measurement_units','b.description as lines_product','c.description as sublines_product','g.description as brands_repuestos','h.description as country_origin','a.warranty','i.description as money','a.cost','a.gain','a.tax','a.PVP','a.updated_at','a.date_purchase','a.date_purchase','a.price_purchase','f.description as status')->from('products as a')->leftJoin('lines_product as b','a.id_lines','=','b.id')->leftJoin('sublines_product as c','a.id_sublines','=','c.id')->leftJoin('measurement_units as d','a.id_measureunit','=','d.id')->leftJoin('status_globals as f','a.status','=','f.id')->leftJoin('brands_repuestos as g','a.id_marks','=','g.id')->leftJoin('country_origin as h','a.id_country','=','h.id')->leftJoin('coins as i','a.id_coins','=','i.id')->orderBy('a.description', 'asc')->get();
    }

    public function registerEvents(): array
    {   
        $styleArray=[
            array(
            'font'    => array(
                'bold'  => true,
                'color' => array('rgb' => '17202a'), 
                'size'  => 12,
                'name'  => 'Verdana'
            ),
            'fill' => array(
                'rotation'   => 90,
                'startcolor' > array(
                    'argb' => '008080'
                ),
                'endcolor'   => array(
                    'argb' => '008080'
                )
            ),
        )
        ];
        

        return [
            BeforeSheet::class=>function(BeforeSheet $event) use ($styleArray){
                $event->sheet->insertNewColumnBefore(7,2);
                $event->sheet->insertNewColumnBefore('A',2);
            },

            AfterSheet::class=>function(AfterSheet $event) use ($styleArray){
                $event->sheet->setAutoFilter('A1:R1');
                $event->sheet->getStyle('A1:R1')->applyFromArray($styleArray);
            }
        ];
    }

    public function headings(): array
    {   
        return [
            'CODIGO    ',
            'DESCRIPCION     ',
            'ABREVIACION   ',
            'UNIDAD DE MEDIDA     ', 
            'LINEA     ', 
            'SUB LINEA     ',
            'MARCA DE RESPUESTO     ',
            'PAIS DE PROCEDIENCIA     ',
            'MESES DE GARANTIA     ',
            'MONEDA     ',
            'COSTO     ',
            'GANANCIA     ',
            'IMPUESTO     ',
            'PVP     ',
            'PROVEEDOR     ',
            'FECHA ULTIMA COMPRA     ',
            'COSTO ULTIMA COMPRA     ',
            'ESTATUS     ', 


        ];
    }
}

