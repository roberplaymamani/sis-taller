<?php

namespace App\Exports;

use App\Items;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ItemsExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    public function Collection()
    {   
        return Items::select('c.description as items','a.description','a.abreviation','d.symbol','a.price','b.description as status')->from('items  as a')->join('status_globals  as b','a.status','=','b.id')->join('types_items  as c','a.id_types_items','=','c.id')->join('coins  as d','a.id_coins','=','d.id')->orderBy('a.description', 'asc')->get();
    }

    public function registerEvents(): array
    {   
        $styleArray=[
            'font'=>array(
                'size'=>11,
                'bold'=>true,
                'name'=>'Calibri',
                'background-color'=>'#000000',
            )
        ];

        return [
            BeforeSheet::class=>function(BeforeSheet $event) use ($styleArray){
                $event->sheet->insertNewColumnBefore(7,2);
                $event->sheet->insertNewColumnBefore('A',2);
            },

            AfterSheet::class=>function(AfterSheet $event) use ($styleArray){
                $event->sheet->setAutoFilter('A1:F1');
                $event->sheet->getStyle('A1:F1')->applyFromArray($styleArray);
            }
        ];
    }

    public function headings(): array
    {   
        return [
            'RUBRO    ',
            'SUB RUBRO     ',
            'ABREVIACION   ',
            'SIMBOLO   ',
            'PRECIO   ',
            'ESTATUS     ',  
        ];
    }
}
