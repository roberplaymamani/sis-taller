<?php

namespace App\Exports;

use App\EntryInventory;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EntryInventoryExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    public function Collection()
    {   
        return EntryInventory::select('a.num_guide as description','c.description as motive','a.value_sale','a.taxt','a.total_net','a.date_registration','b.description as status')->from('entry_inventorys as a')->join('status_globals as b','a.status','=','b.id')->join('status_inventorys as c','a.entry_exit','=','c.id')->orderBy('a.num_guide', 'asc')->get();
    }

    public function registerEvents(): array
    {   
        $styleArray=[
            'font'=>array(
                'size'=>11,
                'bold'=>true,
                'name'=>'Calibri',
                'background-color'=>'#000000',
            )
        ];

        return [
            BeforeSheet::class=>function(BeforeSheet $event) use ($styleArray){
                $event->sheet->insertNewColumnBefore(7,2);
                $event->sheet->insertNewColumnBefore('A',2);
            },

            AfterSheet::class=>function(AfterSheet $event) use ($styleArray){
                $event->sheet->setAutoFilter('A1:G1');
                $event->sheet->getStyle('A1:G1')->applyFromArray($styleArray);
            }
        ];
    }

    public function headings(): array
    {   
        return [
            'NUMERO DE GUIA    ',
            'TIPO DE OPERACION     ',
            'VALOR DE VENTA   ',
            'IMPUESTO     ',  
            'TOTAL NETO     ',
            'FECHA DE REGISTRO     ',
            'ESTATUS     ',
        ];
    }
}