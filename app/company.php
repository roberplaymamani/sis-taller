<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    protected $table='parameters';
    protected $fillable = ['id', 'rucEmpresa', 'razonsocialEmpresa', 'direccionEmpresa', 'telefonoEmpresa', 'celularEmpresa', 'emailEmpresa', 'codMoneda', 'codFormapago', 'codCondicion', 'IGV', 'Ganancia', 'LinkSunat', 'paginaWeb', 'logoEmpresa', 'codAlmacen'];
}
