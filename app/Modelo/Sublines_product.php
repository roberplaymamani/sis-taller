<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Sublines_product extends Model{
    protected $table='sublines_product';
	protected $primaryKey='id';

    public function products()	{
    	return $this->hasMany('\App\Modelo\Products','id_sublines','id');
	}
	public function lines_product(){
    	return $this->belongsTo('\App\Modelo\Lines_product','id_product','id');
	}
}
