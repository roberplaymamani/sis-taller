<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Lines_product extends Model{
   	protected $table='lines_product';
	protected $primaryKey='id';
	public $timestamps=false;
    public function products(){
    	return $this->hasMany('\App\Modelo\Products','id_lines ','id');
	}
	public function sublines_product(){
    	return $this->hasMany('\App\Modelo\Sublines_product','id_product ','id');
	}
}
