<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Products extends Model{

    protected $table='products';
	protected $primaryKey='id';

    public function sublines_products(){
    	return $this->belongsTo('\App\Modelo\Sublines_product','id_sublines','id');
	}
	public function coins(){
    	return $this->belongsTo('\App\Modelo\Coins','id_coins','id');
	}
	public function country_origin(){
    	return $this->belongsTo('\App\Modelo\Country_origin','id_country','id');
	}
	public function lines_product(){
    	return $this->belongsTo('\App\Modelo\Lines_product','id_lines','id');
	}
	public function marks(){
    	return $this->belongsTo('\App\Modelo\Marks','id_marks','id');
	}
	public function measurement_units(){
    	return $this->belongsTo('\App\Modelo\Measurement_units','id_measureunit','id');
	}
	public function provider(){
    	return $this->belongsTo('\App\Modelo\Clients','id_provider','id');
	}
}
