<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Coins extends Model{
    protected $table='coins';
	protected $primaryKey='id';
	public $timestamps=false;

    public function products(){
    	return $this->hasMany('\App\Modelo\Products','id_coins','id');
	}
}
