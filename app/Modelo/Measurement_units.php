<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Measurement_units extends Model{
    protected $table='measurement_units';
	protected $primaryKey='id';
	public $timestamps=false;
	
    public function products()	{
    	return $this->hasMany('\App\Modelo\Products','id_measureunit ','id');
	}
}
