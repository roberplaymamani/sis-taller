<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model{
    protected $table='marks';
	protected $primaryKey='id';
	public $timestamps=false;
	
    public function products()	{    	
    	return $this->hasMany('\App\Modelo\Products','id_marks ','id');
	}
}
