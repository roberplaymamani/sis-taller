<?php

namespace App\Modelo;

use Illuminate\Database\Eloquent\Model;

class Country_origin extends Model{
    protected $table='country_origin';
	protected $primaryKey='id';
	public $timestamps=false;
	
    public function products(){
    	return $this->hasMany('\App\Modelo\Products','id_country','id');
	}
}
