<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ubdistrito extends Model
{
   protected $table='ubdistrito';
   protected $fillable = ['idDist','distrito','idProv'];
}
