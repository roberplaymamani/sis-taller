$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    /*$('#listVehicles').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: "/listVehicles",
            type: 'GET',
        },

        columns: [
            { data: 'id', name: 'id', 'visible': false},
            { data: 'plate', name: 'plate'},
            { data: 'category' },
            { data: 'mark' },
            { data: 'model' },
            { data: 'year' },
            { data: 'name' },
            { data: 'description' },
            { data: 'action' },
        ],
        order: [[0, 'desc']]
    });*/
    $('body').on('click', '.edit-vehicle', function() {
        var vehcile = $(this).data("id").split('/');
        var vehicle_id = vehcile[0];
        var vehicle_plate = vehcile[1];
        $.get('listVehicles/' + vehicle_id + '/edit', function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-vehicle').val('edit');
            $("#btn-vehicle").html("<li class='fa fa-edit'></li> Editar veh.");
            $('#tittleVehicles').text('Editar');
            $('#tittleVehicleshHr').text('Editar');
            $('#Clientname').prop("disabled", true);
            $('#Clientdocument').prop("disabled", true);
            // Combo de categoria
            $('#Vehcategory').val('');
            $('#Vehcategory').empty();
            var categories = data.categories;
            var select = document.getElementsByName('Vehcategory')[0];
            for (value in categories) {
                var option = document.createElement("option");
                option.value = categories[value]['id'];
                option.text = categories[value]['description'];
                select.add(option);
            }
            // Combo de marca
            $('#Vehmark').val('');
            $('#Vehmark').empty();
            var marks = data.marks;
            var select = document.getElementsByName('Vehmark')[0];
            for (value in marks) {
                var option = document.createElement("option");
                option.value = marks[value]['id'];
                option.text = marks[value]['description'];
                select.add(option);
            }
            // Combo de modelo
            $('#Vehmodel').val('');
            $('#Vehmodel').empty();
            var models = data.models;
            var select = document.getElementsByName('Vehmodel')[0];
            for (value in models) {
                var option = document.createElement("option");
                option.value = models[value]['id'];
                option.text = models[value]['description'];
                select.add(option);
            }
            // Combo de estatus
            $('#Vehstatus').val('');
            $('#Vehstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Vehstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#Vehid').val(data.vehicles['id']);
            $('#Vehmark').val(data.vehicles['mark']);
            $('#Vehyear').val(data.vehicles['year']);
            $('#Vehplate').val(data.vehicles['plate']);
            $('#Vehmodel').val(data.vehicles['model']);
            $('#Vehmotor').val(data.vehicles['motor']);
            $('#Vehserie').val(data.vehicles['serie']);
            $('#Clientname').val(data.vehicles['name']);
            $('#Clienttype').val(data.vehicles['type']);
            $('#Vehstatus').val(data.vehicles['status']);
            $('#Vehcolour').val(data.vehicles['colour']);
            $('#Vehcolour').val(data.vehicles['colour']);
            $("#Clientid").val(data.vehicles['Clientid']);
            $("#ClientidNew").val(data.vehicles['Clientid']);
            $('#Vehcategory').val(data.vehicles['category']);
            $('#Vehmotor').val(data.vehicles['number_engine']);
            $('#Vehserie').val(data.vehicles['number_series']);
            $('#Clientdocument').val(data.vehicles['document']);
            $('#Vehobservations').val(data.vehicles['observations']);
            $('#Vehobservations').text(data.vehicles['observations']);
        });
        AddVehiculoDIV('divListaVehiculo');
    });
    $('body').on('click', '.add-vehicle', function() {
        $('#tittleVehicles').text('Registrar');
        $('#tittleVehicleshHr').text('Registrar');
        $("#btn-vehicle").html("<li class='fa fa-save'></li> Registrar veh.");
        var vehcile = $(this).data("id");
        $.get('listVehicles/' + vehcile + '/edit', function(data) {
            if (data) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-vehicle').val('add');
                $('#Clientname').prop("disabled", false);
                $('#Clientdocument').prop("disabled", false);
                // Combo de categoria
                $('#Vehcategory').val('');
                $('#Vehcategory').empty();
                var categories = data.categories;
                var select = document.getElementsByName('Vehcategory')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in categories) {
                    var option = document.createElement("option");
                    option.value = categories[value]['id'];
                    option.text = categories[value]['description'];
                    select.add(option);
                }
                // Combo de marca
                $('#Vehmark').val('');
                $('#Vehmark').empty();
                var marks = data.marks;
                var select = document.getElementsByName('Vehmark')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in marks) {
                    var option = document.createElement("option");
                    option.value = marks[value]['id'];
                    option.text = marks[value]['description'];
                    select.add(option);
                }
                // Combo de modelo
                $('#Vehmodel').val('');
                $('#Vehmodel').empty();
                var models = data.models;
                var select = document.getElementsByName('Vehmodel')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in models) {
                    var option = document.createElement("option");
                    option.value = models[value]['id'];
                    option.text = models[value]['description'];
                    select.add(option);
                }
                // Combo de estatus
                $('#Vehstatus').val('');
                $('#Vehstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Vehstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                //Register data
                $('#Vehid').val('');
                $('#Vehmark').val('0');
                $('#Vehyear').val('');
                $('#Vehplate').val('');
                $('#Vehmodel').val('0');
                $('#Vehmotor').val('');
                $('#Vehserie').val('');
                $("#Clientid").val('');
                $('#Vehmotor').val('');
                $('#Vehserie').val('');
                $('#Vehstatus').val('1');
                $('#Vehcolour').val('');
                $('#Vehcolour').val('');
                $('#Clientname').val('');
                $('#Clienttype').val('1');
                $("#ClientidNew").val('');
                $('#Vehcategory').val('0');
                $('#Clientdocument').val('');
                $('#Vehobservations').val('');
                $('#Vehobservations').text('');
            }
        });
    });
    $(document).on('click', '#paginatesvehi a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && this.PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax = */
        $.ajax({
            type: 'get',
            url: '/vehicles',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                cant_mostrar: $('#cant_mostrar').val()
            },
            success: function(rpta) {
                $('#listPaginatesvehi').html(rpta.html);
                $('#paginatesvehi').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchListVehiculo', function(e) {
        e.preventDefault();
        filterVehiculos();
    });
    $('body').on('click', '#delete-vehicle', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var vehcile = $(this).data("id").split('/');
        var vehicle_id = vehcile[0];
        var vehicle_plate = vehcile[1];
        alertify.confirm("Está seguro que desea eliminar el registro con el NRO DE PLACA: " + vehicle_plate.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "get",
                    url: "/listVehicles/delete/" + vehicle_id,
                    success: function(data) {
                        //$('#mensSuccess').text('El vehículos de placa ' + vehicle_plate.toUpperCase() + ' se eliminó correctamente...');
                        alertify.success('El vehículos de placa ' + vehicle_plate.toUpperCase() + ' se eliminó correctamente...');
                        $('#contentSuccess').show();
                        getClearVehicles();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#mensDanger').text('El vehículos de placa ' + vehicle_plate.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        alertify.error('El vehículos de placa ' + vehicle_plate.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        $('#contentDanger').show();
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#vehicles-submit").on("submit", function(e) {
        var Vehmark = $("#Vehmark").val();
        var Vehyear = $("#Vehyear").val();
        var Vehplate = $("#Vehplate").val();
        var Vehmotor = $("#Vehmotor").val();
        var Vehcolour = $("#Vehcolour").val();
        if ($("#btn-vehicle").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var yes = 1;
        if (!Vehplate || !Vehcategory || !Vehmark || !Vehcolour || !Vehyear || !Vehmotor) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-vehicle").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("vehicles-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/listVehicles/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    $('#mensSuccess').text('El vehículos de placa ' + Vehplate.toUpperCase() + ' se ' + content + ' correctamente...');
                    $('#contentSuccess').show();
                    filterVehiculos();
                    AddVehiculoDIV($('#btnAdd').val());
                    getClearVehicles();
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#mensDanger').text('El vehículos de placa ' + Vehplate.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    $('#contentDanger').show();
                }
            });
        }
    });
    /*$('body').on('click', '#btn-vehicle', function () {
     
        if($("#btn-vehicle").val()=='edit') {

            var action='edit';
            var content='editó';
            var Vehmark=$("#Vehmark").val();
            var Vehyear=$("#Vehyear").val();
            var Clientid=$("#Clientid").val();
            var Vehplate=$("#Vehplate").val();
            var Vehmotor=$("#Vehmotor").val();
            var Vehserie=$("#Vehserie").val();
            var Vehmodel=$("#Vehmodel").val();
            var Vehcolour=$("#Vehcolour").val();
            var Vehstatus=$("#Vehstatus").val();
            var Clientname=$("#Clientname").val();
            var Vehcategory=$("#Vehcategory").val();
            var ClientidNew=$("#ClientidNew").val();
            var Clientdocument=$("#Clientdocument").val();
            var Vehobservations=$("#Vehobservations").val();

            if(Clientid!=ClientidNew) { 
                if(!confirm("Está seguro que desea cambiar el propietario del vehículo con el NRO DE PLACA: "+Vehplate.toUpperCase()+" !")) {
                    getClearVehicles();return;
                }
            } 

        } else {

            var ClientidNew='';
            var action='register';
            var content='Registro';
            var Vehmark=$("#Vehmarks").val();
            var Vehyear=$("#Vehyears").val();
            var Clientid=$("#Clientids").val();
            var Vehplate=$("#Vehplates").val();
            var Vehmotor=$("#Vehmotors").val();
            var Vehserie=$("#Vehseries").val();
            var Vehmodel=$("#Vehmodels").val();
            var Vehcolour=$("#Vehcolours").val();
            var Vehstatus=$("#Vehstatuss").val();
            var Clientname=$("#Clientnames").val();
            var Vehcategory=$("#Vehcategorys").val();
            var Clientdocument=$("#Clientdocuments").val();
            var Vehobservations=$("#Vehobservationss").val();
        }
        
        var yes=1;
        if(!Vehplate || !Vehcategory || !Vehmark || !Vehmodel || !Vehcolour || !Vehyear || !Vehmotor || !Vehserie || !Vehstatus || !Clientname || !Clientid) {
            yes=0;
        }    
      
        if(yes > 0) {
        
            $("#btn-vehicle").attr("disabled", true);

            $.ajax({
                data:{"action":action,"Vehid":$("#Vehid").val(),"Vehplate":Vehplate,"Vehcategory":Vehcategory,"Vehmark":Vehmark,"Vehmodel":Vehmodel,"Vehcolour":Vehcolour,"Vehyear":Vehyear,"Vehmotor":Vehmotor,"Vehserie":Vehserie,"Vehobservations":Vehobservations,"Vehstatus":Vehstatus,"Clientid":Clientid,"ClientidNew":ClientidNew},
                url: "/listVehicles/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#mensSuccess').text('El vehículos de placa '+Vehplate.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearVehicles();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensDanger').text('El vehículos de placa '+Vehplate.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentDanger').show();
                }
            });
        }  
    });*/
});
/*=============================================================================================================================*/
$(document).on('click', '#cv_paginates a', function(e) {
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var Name = 'DataNulo';
    if ($('#name').val() != '') {
        var Name = $('#name').val();
    }
    if ($('#searchListClient').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchListClient').val();
    $.ajax({
        type: 'get',
        url: '/client_search_vehicle',
        data: {
            page: page,
            search: type_filter,
            valueInput: valueInput
        },
        success: function(rpta) {
            $('#cv_listPaginates').html(rpta.html);
            $('#cv_paginates').html(rpta.pagenationAdd);
        }
    });
});
$(document).on('keyup', '#searchListClient', function(e) {
    e.preventDefault();
    filterClient_vehicle();
});

function filterClient_vehicle() {
    if ($('#searchListClient').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchListClient').val();
    $.ajax({
        type: 'get',
        url: '/client_search_vehicle',
        data: {
            search: type_filter,
            valueInput: valueInput
        },
        success: function(rpta) {
            $('#cv_listPaginates').html(rpta.html);
            $('#cv_paginates').html(rpta.pagenationAdd);
        }
    });
}

function filterVehiculos() {
    if ($('#searchListVehiculo').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchListVehiculo').val();
    $.ajax({
        type: 'get',
        url: '/vehicles',
        data: {
            search: type_filter,
            valueInput: valueInput,
            cant_mostrar: $('#cant_mostrar').val()
        },
        success: function(rpta) {
            $('#listPaginatesvehi').html(rpta.html);
            $('#paginatesvehi').html(rpta.pagenationAdd);
        }
    });
}

function SelectCleint(val1, val2, val3, val4) {
    if ($('#Clientid').val() != val1) {
        if ($('#btn-vehicle').val() == 'edit') {
            alertify.confirm("Está seguro que desea cambiar el propietario del vehículo con el NRO DE PLACA: " + $('#Vehplate').val().toUpperCase() + " !.", function(e) {
                if (e) {
                    $('#ClientidNew').val(val1);
                    $('#Clientname').val(val3);
                    $('#Clienttype').val(val2);
                    $('#Clientdocument').val(val4);
                    $('.buttonCerrarCleintes').click();
                }
            });
        } else {
            $('#ClientidNew').val('');
            $('#Clientid').val(val1);
            $('#Clientname').val(val3);
            $('#Clienttype').val(val2);
            $('#Clientdocument').val(val4);
            $('.buttonCerrarCleintes').click();
        }
    }
}
/*=============================================================================================================================*/
function models(models, marks) {
    $('#' + models).val('');
    $('#' + models).empty();
    $("#" + marks + " option:selected").each(function() {
        id = $(this).val();
        $.post("/vehicles/listVehicles/models", {
            "id": id
        }, function(data) {
            if (data.length > 0) {
                var select = document.getElementsByName(models)[0];
                for (value in data) {
                    var option = document.createElement("option");
                    option.value = data[value]['id'];
                    option.text = data[value]['description'];
                    select.add(option);
                }
            } else {
                var select = document.getElementsByName(models)[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'No hay registro';
                select.add(option);
            }
        });
    });
}

function AddVehiculoDIV(valueAdd) {
    if (valueAdd == 'divListaVehiculo') {
        $('#divListaVehiculo').prop('hidden', true);
        $('#divRegistrarVehiculo').prop('hidden', false);
        $('#btnAdd').val('divRegistrarVehiculo');
        $('#btnAdd').html('<span class="fa fa-list"></span>&nbsp;Lista vehiculos');
        $('#searchListVehiculo').prop('readonly', true);
    } else {
        $('#divListaVehiculo').prop('hidden', false);
        $('#divRegistrarVehiculo').prop('hidden', true);
        $('#btnAdd').val('divListaVehiculo');
        $('#btnAdd').html('<span class="fa fa-pencil"></span>&nbsp;Agregar vehiculos');
        $('#searchListVehiculo').prop('readonly', false);
    }
}

function getClearVehicles() {
    $('#Vehid').val('');
    $('#Vehmark').val('');
    $('#Vehyear').val('');
    $('#Vehplate').val('');
    $('#Vehmodel').val('');
    $('#Vehmotor').val('');
    $('#Vehserie').val('');
    $("#Clientid").val('');
    $('#Vehcolour').val('');
    $('#Vehstatus').val('1');
    $('#Clientname').val('');
    $('#Vehcategory').val('');
    $("#ClientidNew").val('');
    $('#Clienttype').val('1');
    $('#Clientdocument').val('');
    $('#Vehobservations').empty();
    $('#Vehobservations').text('');
    $("#btn-vehicle").attr("disabled", false);
    // Modal
    $("body").removeAttr("style");
    $('.jquery-modal').fadeOut(500);
    $('#ListClientSearch tr').removeClass('highlighted');
    $('#btn-vehicle').val('edit');
    $("#btn-vehicle").html("<li class='fa fa-save'></li> Registrar veh.");
    /*var oTable = $('.ListVehiclesRefres').dataTable();
    oTable.fnDraw(false);*/
}