function tab(id) {
    if (tab == 1) {
        $('#info').prop('hidden', false);
        $('#patametros').prop('hidden', true);
        $('soap').prop('hidden', true);
    }
    if (tab == 2) {
        $('#info').prop('hidden', true);
        $('#patametros').prop('hidden', false);
        $('soap').prop('hidden', true);
    }
    if (tab == 3) {
        $('#info').prop('hidden', true);
        $('#patametros').prop('hidden', true);
        $('soap').prop('hidden', false);
    }
}
$('#frm_add_updateCompany').on('submit', function(e) {
    e.preventDefault();
    data = new FormData(this);
    alertify.confirm("<p>Está seguro que desea alterar datos predeterminados. ?", function(e) {
        if (e) {
            $.ajax({
                method: 'post',
                url: '/company_parametros_store',
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                success: function(rpta) {
                    if (rpta.statuslogo == 1) {
                        $('#LogoEmpresImg').prop('src', '/img/company/' + rpta.img);
                    }
                    alertify.success('<b>Mensaje: </b> Datos predeterminados actualizados correctamente.');
                }
            });
        } else {
            alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
        }
    });
})
//, #razonsocialEmpresa
$('#rucEmpresa, #razonsocialEmpresa').on('keyup', function(e) {
    var nameEmpresa = $('#rucEmpresa').val() + ' - ' + $('#razonsocialEmpresa').val();
    $('#empresaadd').html(nameEmpresa);
});
$('#direccionEmpresa').on('keyup', function(e) {
    var nameEmpresa = $('#direccionEmpresa').val();
    $('#direccionadd').html(nameEmpresa);
});
/*info
parametros
soap*/