$(function(){
   $('#Clientbirthdate').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#Clientanniversary').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#Clientbirthdates').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#Clientanniversarys').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#Productsdate_purchase').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#EntryInventorydate').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });
    
    $('#preview').hover(
        function() {
            $(this).find('a').fadeIn();
        }, function() {
            $(this).find('a').fadeOut();
        }
    )
    $('#file-select').on('click', function(e) {
         e.preventDefault();
        
        $('#file').click();
    })
    
    $('input[type=file]').change(function() {
        var file=(this.files[0].name).toString();
        var reader=new FileReader();
        var ext=file.split('.');

        if(ext[1]=='png' || ext[1]=='svg' || ext[1]=='jpg' || ext[1]=='jpeg' || ext[1]=='png' || ext[1]=='gif' || ext[1]=='bmp') {
            $('#file-info').text('');
            $('#file-info').text(file);
        } else {
            $("#file").val('');
            $('#file-info').text('');
            $('#selectImagen').empty();
            $("#file-info").text('No hay archivo aún');
            alert('Por favor, suba el archivo con las extensiones .jpeg / .jpg / .png / .gif solamente.');
            $('#selectImagen').attr('src', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=');
        }
        
        reader.onload = function (e) {
            $('#preview img').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    });
});

function format(input) {
    var num=input.value.replace(/\,/g,'');
    if(!isNaN(num)){
        num=num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        num=num.split('').reverse().join('').replace(/^[\,]/,'');
        input.value=num;
    } else { 
        input.value=input.value.replace(/[^\d\.]*/g,'');
    }
}

function validate() {
    if($("#Userpassword").val()==$("#Userpassword2").val()){
        $("#btn-user").attr("disabled", false);
    } else {
        $("#btn-user").attr("disabled", true);
        if($("#Userpassword2").val() && $("#Userpassword").val().length <= $("#Userpassword2").val().length) {
            $("#Userpassword2").val('');
            $("#btn-user").attr("disabled", true);
            alert('Confirmación de la Contraseña no coincide con la Contraseña !');return 0;
        }
    }
}

function prices(val) {

    var tax=$('#Productstax').val();
    var gain=$('#Productsgain').val();
    var price=$('#Productscost').val();
    var taxporc=parseInt($('#Productstaxporc').val());
    var gainporc=parseInt($('#Productsgainporc').val());
    
    
    tax=tax.replace(',','');
    gain=gain.replace(',','');
    price=price.replace(',','');

    switch (val) {

        case 'P':
            $('#Productstax').val('');
            $('#Productsgain').val('');
            $('#Productstaxporc').val('');
            $('#Productsgainporc').val('');
        break;

        case 'G':
            if(price && gain){
                var GP=parseFloat(gain/price*100);
                $('#Productsgainporc').val(formatDecimal(GP));
            } else if(!gain) {
                $('#Productsgain').val('');
                $('#Productsgainporc').val('');
            }
        break;

        case 'GP':
            if(price && gainporc){
                var G=(price*gainporc/100);
                $('#Productsgain').val(formatMoney(G));
            } else if(!gainporc) {
                $('#Productsgain').val('');
                $('#Productsgainporc').val('');
            }
        break;

        case 'I':
            if(price && tax){
                var I=parseFloat(tax/price*100);
                $('#Productstaxporc').val(formatDecimal(I));
            } else if(!tax) {
                $('#Productstax').val('');
                $('#Productstaxporc').val('');
            }
        break;
    
        case 'IP':
            if(price && taxporc){
                var IP=(price*taxporc/100);
                $('#Productstax').val(formatMoney(IP));
            } else if(!taxporc) {
                $('#Productstax').val('');
                $('#Productstaxporc').val('');
            }
        break;
    }

    if(price && gain && tax) {
        var PVP=(parseInt(price)+parseInt(gain)+parseInt(tax));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price && gain) {
        var PVP=(parseInt(price)+parseInt(gain));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price && tax) {
        var PVP=(parseInt(price)+parseInt(tax));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price) {
        $('#ProductsPVP').val(formatMoney(price));
    }else if(!price) {
        $('#ProductsPVP').val('');
    }
}

function formatMoney(num) {
    if(!isNaN(num)){
        num=num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        num=num.split('').reverse().join('').replace(/^[\,]/,'');
       return num;
    } else { 
        return num.replace(/[^\d\.]*/g,'');
    }
}

function formatDecimal(num) {
    if(num % 1 == 0) {
        return parseInt(num);
    } else {
        return num.toFixed(1);
    }
}

function coins(coins,type) {
    $('.'+coins).val('');
    $('.'+coins).empty();
    
    $("#"+type+" option:selected").each(function () {
        id=$(this).val();
        $.post(SITEURL + "/products/listProducts/coins",{"id":id},function(data){

            if(data.length > 0) {
                $('.'+coins).text(data[0]['symbol']);
            }   
        });		
    });
}

function products(ids,price,type) {
    $('.'+ids).val('');
    $('.'+ids).empty();

    if(ids=='EntryInventoryid_products'){
        $("#"+type+" option:selected").each(function () {     
            var search='a.id';
            var id=$(this).val();

            $.post(SITEURL + "/entryinventory/listEntryInventory/products",{"id":id,"search":search},function(data){
                if(data.length > 0) {
                    $('#'+ids).val(data[0]['id_products']);       
                    if(data[0]['cost']!='0'){
                        $('#'+price).val(data[0]['cost']);
                    } else {
                        $('#'+price).val('');
                    } 
                } else {
                    $('#'+ids).val('');
                    $('#'+price).val('');
                    $('#EntryInventoryprice').val('');
                    $('#EntryInventorytotal').val('');
                }   
            });		
        });
    } else {
        var search='a.id_products';
        var id=$('#EntryInventoryid_products').val();

        $.post(SITEURL + "/entryinventory/listEntryInventory/products",{"id":id,"search":search},function(data){
            if(data.length > 0) {
                $('#'+ids).val(data[0]['id']);
                if(data[0]['cost']!='0'){
                    $('#'+price).val(data[0]['cost']);
                } else {
                    $('#'+price).val('');
                }       
            } else {
                $('#'+ids).val('');
                $('#'+price).val('');
                $('#EntryInventoryprice').val('');
                $('#EntryInventorytotal').val('');
            }  
        });	
    }    
}

function pricesEntry(val) {

    var price=$('#EntryInventoryprice').val();
    var cant=parseInt($('#EntryInventoryquantity').val());

    price=price.replace(',','');
    switch (val) {

        case 'PRE':
            var PRE=(price*cant);
            if(PRE) {
                $('#EntryInventorytotal').val(formatMoney(PRE));
            } else {
                $('#EntryInventorytotal').val('');
            }
        break;

        case 'CANT':
            var PRE=(price*cant);
            if(PRE) {
                $('#EntryInventorytotal').val(formatMoney(PRE));
            } else {
                $('#EntryInventorytotal').val('');
            }
        break;
    }

    if((!price || !cant) || (!price && !cant)) {
        $('#EntryInventorytotal').val('');
    }
}    