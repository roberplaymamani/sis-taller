$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    //==================================================================================================
    $('#contentDanger').hide();
    $('#contentSuccess').hide();
    $('#btn-items').val('Registrar');
    $('#btn-items').html('<li class="fa fa-save"></li> Registrar');
    var item = $(this).data("id");
    $.get('listItems/' + item + '/edit', function(data) {
        if (data.status.length > 0) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-itmems').val('add');
            // Combo de rubros
            $('#Itemsidrubro').val('');
            $('#Itemsidrubro').empty();
            var types_items = data.types_items;
            var select = document.getElementsByName('Itemsidrubro')[0];
            for (value in types_items) {
                var option = document.createElement("option");
                option.value = types_items[value]['id'];
                option.text = types_items[value]['description'];
                select.add(option);
            }
            // Combo de moneda
            $('#Itemsidcoins').val('');
            $('#Itemsidcoins').empty();
            var coins = data.coins;
            var select = document.getElementsByName('Itemsidcoins')[0];
            for (value in coins) {
                var option = document.createElement("option");
                option.value = coins[value]['id'];
                option.text = coins[value]['symbol'];
                select.add(option);
            }
            // Combo de estatus
            $('#Itemsstatus').val('');
            $('#Itemsstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Itemsstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#Itemsid').val('');
            $('#Itemsprice').val('');
            $('#Itemsstatus').val('1');
            $('#Itemsidcoins').val('1');
            $('#Itemsidrubro').val('1');
            $('#Itemsdescription').val('');
            $('#Itemsabreviation').val('');
            $('#tittleItems').text('Registrar');
            $('#tittleItemsHr').text('Registrar');
        }
    });
    //==================================================================================================        
    $('body').on('click', '.edit-items', function() {
        $('#tittle').html('EDICION');
        var item = $(this).data("id").split('/');
        var item_id = item[0];
        var item_name = item[1];
        $.get('listItems/' + item_id + '/edit', function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-items').val('edit');
            $('#btn-items').html('<li class="fa fa-save"></li> Editar');
            // Combo de rubros
            $('#Itemsidrubro').val('');
            $('#Itemsidrubro').empty();
            var types_items = data.types_items;
            var select = document.getElementsByName('Itemsidrubro')[0];
            for (value in types_items) {
                var option = document.createElement("option");
                option.value = types_items[value]['id'];
                option.text = types_items[value]['description'];
                select.add(option);
            }
            // Combo de moneda
            $('#Itemsidcoins').val('');
            $('#Itemsidcoins').empty();
            var coins = data.coins;
            var select = document.getElementsByName('Itemsidcoins')[0];
            for (value in coins) {
                var option = document.createElement("option");
                option.value = coins[value]['id'];
                option.text = coins[value]['symbol'];
                select.add(option);
            }
            // Combo de estatus
            $('#Itemsstatus').val('');
            $('#Itemsstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Itemsstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#tittleItems').text('Editar');
            $('#tittleItemsHr').text('Editar');
            $('#Itemsid').val(data.items[0]['id']);
            $('#Itemsprice').val(data.items[0]['price']);
            $('#Itemsstatus').val(data.items[0]['status']);
            $('#Itemsidcoins').val(data.items[0]['id_coins']);
            $('#Itemsidrubro').val(data.items[0]['id_types_items']);
            $('#Itemsdescription').val(data.items[0]['description']);
            $('#Itemsabreviation').val(data.items[0]['abreviation']);
        })
    });
    /*$('body').on('click', '.add-items', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        $('#btn-items').val('Registrar');
        $('#btn-items').text('Registrar');
        var item = $(this).data("id");
        $.get('listItems/' + item + '/edit', function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-itmems').val('add');
                // Combo de rubros
                $('#Itemsidrubro').val('');
                $('#Itemsidrubro').empty();
                var types_items = data.types_items;
                var select = document.getElementsByName('Itemsidrubro')[0];
                for (value in types_items) {
                    var option = document.createElement("option");
                    option.value = types_items[value]['id'];
                    option.text = types_items[value]['description'];
                    select.add(option);
                }
                // Combo de moneda
                $('#Itemsidcoins').val('');
                $('#Itemsidcoins').empty();
                var coins = data.coins;
                var select = document.getElementsByName('Itemsidcoins')[0];
                for (value in coins) {
                    var option = document.createElement("option");
                    option.value = coins[value]['id'];
                    option.text = coins[value]['symbol'];
                    select.add(option);
                }
                // Combo de estatus
                $('#Itemsstatus').val('');
                $('#Itemsstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Itemsstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                //Register data
                $('#Itemsid').val('');
                $('#Itemsprice').val('');
                $('#Itemsstatus').val('1');
                $('#Itemsidcoins').val('1');
                $('#Itemsidrubro').val('1');
                $('#Itemsdescription').val('');
                $('#Itemsabreviation').val('');
                $('#tittleItems').text('Registrar');
                $('#tittleItemsHr').text('Registrar');
            }
        });
    });*/
    $('body').on('click', '#delete-items', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var Items = $(this).data("id").split('/');
        var Items_id = Items[0];
        var Items_name = Items[1];
        alertify.confirm("Está seguro que desea eliminar el registro: " + Items_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "get",
                    url: "/listItems/delete/" + Items_id,
                    success: function(data) {
                        alertify.success('El registro ' + Items_name.toUpperCase() + ' se eliminó correctamente...');
                        getClearItems();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        alertify.success('El registro ' + Items_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#items-submit").on("submit", function(e) {
        if ($("#btn-items").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var Itemsprice = $('#Itemsprice').val();
        var Itemsstatus = $('#Itemsstatus').val();
        var Itemsidcoins = $('#Itemsidcoins').val();
        var Itemsidrubro = $('#Itemsidrubro').val();
        var Itemsdescription = $('#Itemsdescription').val();
        var Itemsabreviation = $('#Itemsabreviation').val();
        var yes = 1;
        if (!Itemsidrubro || !Itemsdescription || !Itemsabreviation || !Itemsidcoins || !Itemsstatus || !Itemsprice) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-items").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("items-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/listItems/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    alertify.success('El registro ' + Itemsdescription.toUpperCase() + ' se ' + content + ' correctamente...');
                    filterRubros();
                    getClearItems();
                },
                error: function(data) {
                    console.log('Error:', data);
                    alertify.success('El registro ' + Itemsdescription.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                }
            });
        }
    });
    $(document).on('click', '#paginates a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        $.ajax({
            type: 'get',
            url: '/items',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                cant_mostrar: $('#cant_mostrar').val()
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList', function(e) {
        e.preventDefault();
        filterRubros();
    });
});

function filterRubros() {
    if ($('#searchList').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchList').val();
    $.ajax({
        type: 'get',
        url: '/items',
        data: {
            search: type_filter,
            valueInput: valueInput,
            cant_mostrar: $('#cant_mostrar').val()
        },
        success: function(rpta) {
            $('#listPaginates').html(rpta.html);
            $('#paginates').html(rpta.pagenationAdd);
        }
    });
}

function getClearItems() {
    $('#Itemsid').val('');
    $('#Itemsprice').val('');
    $('#Itemsstatus').val('1');
    $('#Itemsidcoins').val('1');
    $('#Itemsidrubro').val('1');
    $('#Itemsdescription').val('');
    $('#Itemsabreviation').val('');
    $("#btn-items").attr("disabled", false);
    $('.jquery-modal').fadeOut(500);
    $("body").removeAttr("style");

    $('#btn-items').val('Registrar');
    $('#btn-items').html('<li class="fa fa-save"></li> Registrar');
    $('#tittle').html('REGISTRO');
    var oTable = $('.listItemssRefres').dataTable();
    oTable.fnDraw(false);
}