$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    /*Ubigeos*/
    $.ajax({
        type: "GET",
        url: "/departamentos",
        success: function(rpta_data) {
            var lista_departamentos = '<option value="">Seleccionar</option>';
            $.each(rpta_data, function(k, v) {
                var id = rpta_data[k].idDepa;
                var dep = rpta_data[k].departamento;
                lista_departamentos += '<option value="' + id + '">' + dep + '</option>';
            });
            $('#idDepa').html(lista_departamentos);
        }
    });
    $('#idDepa').on('change', function(e) {
        provinciasList(this.value, '');
    });
    $('#idProv').on('change', function(e) {
        distritosList(this.value, '');
    });
    /*========================================================================================*/
    // Editar cliente
    $('body').on('click', '.edit-client', function() {
        var client = $(this).data("id").split('/');
        var client_id = client[0];
        var client_name = client[1];
        $.get('listClient/' + client_id + '/edit', function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-client').val('edit');
            if ($('#typeClient').val() == 1) {
                $('#btn-client').html('<li class="fa fa-edit fa-lg"></li> EDITAR CLIENTE');
            } else {
                $('#btn-client').html('<li class="fa fa-edit fa-lg"></li> EDITAR PROVEEDOR');
            }
            // Combo de estatus
            $('#Clientstatus').val('');
            $('#Clientstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Clientstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#Clientsid').val(data.client['id']);
            $('#Clienttype').val(data.client['type']);
            $('#Clientname').val(data.client['name']);
            $('#Clientphone').val(data.client['phone']);
            $('#Clientemail').val(data.client['email']);
            $('#Clientstatus').val(data.client['status']);
            $('#Clientcontact').val(data.client['contact']);
            $('#Clientaddress').val(data.client['address']);
            $('#Clientaddress').text(data.client['address']);
            $('#Clientdocument').val(data.client['document']);
            $('#Clientbirthdate').val(data.client['birthdate']);
            $('#Clientanniversary').val(data.client['anniversary']);
            $('#Clientobservations').val(data.client['observations']);
            $('#Clientobservations').text(data.client['observations']);
            $('#tittleClients').text('Editar');
            $('#tittleClienstHr').text('Editar');
            $('#celphone').val(data.client['celphone']);
            $('#typeClient').val(data.client['typeClient'])
            $('#idDepa').val(data.client['idDepa']);
            provinciasList(data.client['idDepa'], data.client['idProv']);
            distritosList(data.client['idProv'], data.client['idDist']);
            //$('#idProv').val('');
            //$('#idDist').val('');
        })
        AddClienteDIV('divListaCliente');
    });
    $('body').on('click', '.add-client', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        $('#btn-client').val('Registrar');
        $('#btn-client').text('Registrar');
        var client = $(this).data("id");
        $.get('listClient/' + client + '/edit', function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-client').val('add');
                // Combo de estatus
                $('#Clientstatus').val('');
                $('#Clientstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Clientstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                //Register data
                $('#Clientsid').val('');
                $('#Clientname').val('');
                $('#Clienttype').val('1');
                $('#Clientphone').val('');
                $('#Clientemail').val('');
                $('#Clientstatus').val('1');
                $('#Clientcontact').val('');
                $('#Clientaddress').val('');
                $('#Clientaddress').text('');
                $('#Clientdocument').val('');
                $('#Clientbirthdate').val('');
                $('#Clientanniversary').val('');
                $('#Clientobservations').val('');
                $('#Clientobservations').text('');
                $('#tittleClients').text('Registrar');
                $('#tittleClienstHr').text('Registrar');
            }
        });
    });
    // Eliminar cliente
    $('body').on('click', '#delete-client', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var client = $(this).data("id").split('/');
        var client_id = client[0];
        var client_name = client[1];
        alertify.confirm("<p>Está seguro que desea eliminar el registro del CLIENTE: " + client_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "GET",
                    url: SITEURL + "/listClient/delete/" + client_id,
                    success: function(data) {
                        //$('#mensSuccess').text('El cliente ' + client_name.toUpperCase() + ' se eliminó correctamente...');
                        alertify.success('El cliente ' + client_name.toUpperCase() + ' se eliminó correctamente...');
                        $('#contentSuccess').show();
                        getClearClient();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#mensDanger').text('El cliente ' + client_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        alertify.error('El cliente ' + client_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        $('#contentDanger').show();
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#clients-submit").on("submit", function(e) {
        if ($("#btn-client").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
            $('#btn-client').val('add');
            if ($('#typeClient').val() == 1) {
                $('#btn-client').html('<li class="fa fa-plus-square fa-lg"></li> REGISTRAR CLIENTE');
            } else {
                $('#btn-client').html('<li class="fa fa-plus-square fa-lg"></li> REGISTRAR PROVEEDOR');
            }
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var yes = 1;
        var Clientname = $('#Clientname').val();
        var Clientphone = $('#Clientphone').val();
        if (!Clientname || !Clientphone) {
            yes = 0;
        }
        if (yes > 0) {
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("clients-submit"));
            formData.append("dato", "valor");
            formData.append("action", action);
            $.ajax({
                url: "/listClient/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("#btn-client").attr("disabled", true);
                },
                success: function(data) {
                    $("#btn-client").attr("disabled", false);
                    //$('#mensSuccess').text('El cliente ' + Clientname.toUpperCase() + ' se ' + content + ' correctamente...');
                    alertify.success('El cliente ' + Clientname.toUpperCase() + ' se ' + content + ' correctamente...');
                    //$('#contentSuccess').show();
                    if ($("#btn-client").val() == 'edit') {
                        $('#btn-client').val('add');
                        $('#btn-client').html('<li class="fa fa-plus-square fa-lg"></li> REGISTRAR CLIENTE');
                    }
                    filterClients();
                    getClearClient();
                    AddClienteDIV($('#btnAdd').val());
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#mensSuccess').text('El cliente ' + Clientname.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    alertify.error('El cliente ' + Clientname.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    //$('#contentSuccess').show();
                }
            });
        } else {
            alertify.error('Complete campos obligatorios por favor.');
        }
        return false;
    });
    var PeticionAjax = null;
    $(document).on('click', '#paginates a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && this.PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax = */
        $.ajax({
            type: 'get',
            url: '/client',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                cant_mostrar: $('#cant_mostrar').val(),
                typeClient: $('#typeClient').val()
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList', function(e) {
        e.preventDefault();
        filterClients();
    });
});

function filterClients() {
    if ($('#searchList').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchList').val();
    $.ajax({
        type: 'get',
        url: '/client',
        data: {
            search: type_filter,
            valueInput: valueInput,
            cant_mostrar: $('#cant_mostrar').val(),
            typeClient: $('#typeClient').val()
        },
        success: function(rpta) {
            $('#listPaginates').html(rpta.html);
            $('#paginates').html(rpta.pagenationAdd);
        }
    });
}

function AddClienteDIV(valueAdd) {
    if (valueAdd == 'divListaCliente') {
        $('#divListaCliente').prop('hidden', true);
        $('#divRegistrarCliente').prop('hidden', false);
        $('#btnAdd').val('divRegistrarCliente');
        if ($('#typeClient').val() == 1) {
            $('#btnAdd').html('<span class="fa fa-list"></span>&nbsp;Lista Clientes');
        } else {
            $('#btnAdd').html('<span class="fa fa-list"></span>&nbsp;Lista Proveedores');
        }
        $('#searchList').prop('readonly', true);
    } else {
        $('#divListaCliente').prop('hidden', false);
        $('#divRegistrarCliente').prop('hidden', true);
        $('#btnAdd').val('divListaCliente');
        if ($('#typeClient').val() == 1) {
            $('#btnAdd').html('<span class="fa fa-plus-square fa-lg"></span>&nbsp;Agregar Cliente');
        } else {
            $('#btnAdd').html('<span class="fa fa-plus-square fa-lg"></span>&nbsp;Agregar Proveedor');
        }
        $('#searchList').prop('readonly', false);
    }
}

function getClearClient() {
    $('#Clientsid').val('');
    $('#Clientname').val('');
    $('#Clienttype').val('1');
    $('#Clientphone').val('');
    $('#Clientemail').val('');
    $('#Clientstatus').val('1');
    $('#Clientcontact').val('');
    $('#Clientaddress').val('');
    $('#Clientaddress').text('');
    $('#Clientdocument').val('');
    $('#Clientbirthdate').val('');
    $('#Clientanniversary').val('');
    $('#Clientobservations').val('');
    $('#Clientobservations').text('');
    $('#celphone').val('');
    $('#idDepa').val('');
    $('#idProv').val('');
    $('#idDist').val('');
    $("#btn-client").attr("disabled", false);
    // Modal
    $("body").removeAttr("style");
    $('.jquery-modal').fadeOut(500);
    $('#ListClientSearch tr').removeClass('highlighted');
    //var oTable = $('.ListClientRefres').dataTable();
    //oTable.fnDraw(false);
}

function provinciasList(argument, argument2) {
    $.ajax({
        type: "GET",
        url: "/provincias",
        data: {
            iddepa: argument
        },
        success: function(rpta_data) {
            var lista_provincias = '<option value="">Seleccionar</option>';
            $.each(rpta_data, function(k, v) {
                var id = rpta_data[k].idProv;
                var dep = rpta_data[k].provincia;
                lista_provincias += '<option value="' + id + '">' + dep + '</option>';
            });
            $('#idProv').html(lista_provincias);
            $('#idDist').html('<option value="">Seleccionar</option>');
            if (argument2 != '') {
                $('#idProv').val(argument2);
            }
        }
    });
}

function distritosList(argument, argument2) {
    $.ajax({
        type: "GET",
        url: "/distritos",
        data: {
            idProv: argument
        },
        success: function(rpta_data) {
            var lista_distrito = '<option value="">Seleccionar</option>';
            $.each(rpta_data, function(k, v) {
                var id = rpta_data[k].idDist;
                var dep = rpta_data[k].distrito;
                lista_distrito += '<option value="' + id + '">' + dep + '</option>';
            });
            $('#idDist').html(lista_distrito);
            if (argument2 != '') {
                $('#idDist').val(argument2);
            }
        }
    });
}