$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    /*
        $('#listSystem').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                data:{"bd":$('#DBactual').val()},
                url: "/listSystem",
                type: 'GET',
            },

            columns: [
                {data:'id',name:'id','visible':false},
                {data:'description'},
                {data:'abreviation'},
                {data:'status'},
                {data:'date','visible':false},
                {data:'action'},
            ],
            order: [[0, 'desc']]
        });
    */
    /*$.ajax({
        data:{"bd":$('#DBactual').val()},
        url: "/listSystem",
        type: 'GET',
        success:function(Rpta) {
            $('#listSystem1').html(Rpta);
        }
    });*/
    $('#DBactual').on('change', function(e) {
        $('#searchList').val('');
        listSystemAdd(this.value);
        if (this.value=='advisors_service') {

            $('#campo4').show();
            $('#campo4_1').hide();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('No. Celular:');

        }else if(this.value=='mechanics') {

            $('#campo4').hide();
            $('#campo4_1').show();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('Comision:');

        }else if(this.value=='models') {

            $('#campo4').hide();
            $('#campo4_1').show();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('Marca:');

        }else if(this.value=='coins') {

            $('#campo4').show();
            $('#campo4_1').hide();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('Simbolo:');

        }else if(this.value=='movements_cash') {

            $('#campo4').hide();
            $('#campo4_1').show();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('Tipo Movimiento:');

        }else if(this.value=='sublines_product') {

            $('#campo4').hide();
            $('#campo4_1').show();
            $('#div4campos').prop('hidden',false);
            $('#NameLabel').html('Producto:');

        }else{
           $('#div4campos').prop('hidden',true);
           $('#NameLabel').html('--'); 
        }
    });

    function listSystemAdd(tblAdd) {
        $.ajax({
            data: {
                "bd": tblAdd
            },
            url: "/listSystem",
            type: 'GET',
            success: function(Rpta) {
                $('#listPaginates').html(Rpta.html);
                $('#paginates').html(Rpta.pagenationAdd);

                if (Rpta.cantCampos==4) {
                    $('#Campo4th').html(Rpta.nombreCampo);
                    $('#Campo4th').prop('hidden', false);
                    if (Rpta.tableName!='') { MostrarCampo4(Rpta.tableName);}
                }else{
                    $('#Campo4th').prop('hidden', true);
                }
            }
            
        });
        $('#list_title').html($("#DBactual option:selected").html().toUpperCase());
    }

    function MostrarCampo4(name_table) {
        $.get("/list_tables_system", { db:name_table } ).done(function( data ) {
            var listOptions = '<option value="">Seleccionar...</option>';
            $.each(data.tablasList, function(k, v) {
                var id = data.tablasList[k].id;
                var Desc = data.tablasList[k].description;
                listOptions += '<option value="'+id+'">'+Desc+'</option>';
            });
            $('#campo4_1').html(listOptions);
        });
    }

    $(document).on('click', '#paginates a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        $.ajax({
            type: 'get',
            url: '/listSystem',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                "bd": $('#DBactual').val()
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList', function(e) {
        e.preventDefault();
        filtersystemreg();
    });

    function filtersystemreg() {
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        $.ajax({
            type: 'get',
            url: '/listSystem',
            data: {
                search: type_filter,
                valueInput: valueInput,
                "bd": $('#DBactual').val()
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    }
    // Editar system
    $('body').on('click', '.edit-system', function() {
        $('#tittle').text('EDICION');
        $('#tittleHorz').text('Editar');
        $("#btn-system").html("<li class='fa fa-edit'></li> Editar reg.");
        var system = $(this).data("id").split('/');
        var system_id = system[0];
        var system_name = system[1];
        $.get('listSystem/' + system_id + '/edit?id=' + system_id + '&bd=' + $('#DBactual').val(), function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-system').val('edit');
            // Combo de estatus
            $('#Systemstatus').val('');
            $('#Systemstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Systemstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#Systemid').val(data.system[0]['id']);
            $('#Systemstatus').val(data.system[0]['status']);
            $('#Systemdescription').val(data.system[0]['description']);
            $('#Systemabreviation').val(data.system[0]['abreviation']);
            if (data.cantCampos==4) {
                $('#campo4').val(data.system[0]['campo4']);
                $('#campo4_1').val(data.system[0]['campo4_1'])
            }
        })
    });
    // Add system
    $('body').on('click', '.add-system', function() {
        $('#tittle').text('REGISTRO');
        $('#tittleHorz').text('Registrar');
        $("#btn-system").text("Registrar");
        var system = $(this).data("id");
        $.get('listSystem/' + system + '/edit?id=' + system + '&bd=' + $('#DBactual').val(), function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-system').val('add');
                // Combo de estatus
                $('#Systemstatus').val('');
                $('#Systemstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Systemstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                //Register data
                $('#Systemid').val('');
                $('#Systemstatus').val('1');
                $('#Systemdescription').val('');
                $('#Systemabreviation').val('');
            }
        });
    });
    // Eliminar system
    $('body').on('click', '#delete-system', function() {
        var system = $(this).data("id").split('/');
        var system_id = system[0];
        var system_name = system[1];
        alertify.confirm("Está seguro que desea eliminar el registro: " + system_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    data: {
                        "id": system_id,
                        "bd": $('#DBactual').val()
                    },
                    url: "/listSystem/delete/" + system_id,
                    type: "GET",
                    success: function(data) {
                        //$('#mensSuccess').text('El registro '+system_name.toUpperCase()+' se eliminó correctamente...');
                        alertify.success('El registro ' + system_name.toUpperCase() + ' se eliminó correctamente...');
                        //$('#contentSuccess').show();
                        getClearSystem();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#mensDanger').text('El cliente '+client_name.toUpperCase()+' no se ha podido eliminar, intentelo nuevamente...');
                        alertify.error('El cliente ' + client_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        //$('#contentDanger').show();
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#systems-submit").on("submit", function(e) {
        if ($("#btn-system").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var bd = $('#DBactual').val();
        var btn = $("#btn-system").val();
        var Systemid = $('#Systemid').val();
        var Systemstatus = $('#Systemstatus').val();
        var Systemdescription = $('#Systemdescription').val();
        var Systemabreviation = $('#Systemabreviation').val();
        var yes = 1;
        if (!Systemdescription || !Systemabreviation || !Systemstatus) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-system").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("systems-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/listSystem/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    //$('#mensSuccess').text('El registro '+Systemdescription.toUpperCase()+' se '+content+' correctamente...');
                    alertify.success('El registro ' + Systemdescription.toUpperCase() + ' se ' + content + ' correctamente...');
                    //$('#contentSuccess').show();
                    listSystemAdd($('#DBactual').val());
                    getClearSystem();
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#mensSuccess').text('El registro '+Systemdescription.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    alertify.alert('#mensSuccess').text('El registro ' + Systemdescription.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    //$('#contentSuccess').show();
                }
            });
        }
    });
    /*$('body').on('click', '#btn-system', function () {
    
        if($("#btn-system").val()=='edit') {
            var action='edit';
            var content='editó';
        } else {
            var action='register';
            var content='Registro';
        } 
       
        var bd=$('#DBactual').val();
        var btn=$("#btn-system").val();
        var Systemid=$('#Systemid').val();
        var Systemstatus=$('#Systemstatus').val();
        var Systemdescription=$('#Systemdescription').val();
        var Systemabreviation=$('#Systemabreviation').val();

        var yes=1;
        if(!Systemdescription || !Systemabreviation || !Systemstatus) {
            yes=0;
        } 
    
        if(yes > 0) {

            $("#btn-system").attr("disabled", true);

            $.ajax({
                data:{"action":action,"Systemid":Systemid,"Systemstatus":Systemstatus,"Systemdescription":Systemdescription,"Systemdescription":Systemdescription,"Systemabreviation":Systemabreviation,"bd":bd,"btn":btn},
                url: "/listSystem/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#mensSuccess').text('El registro '+Systemdescription.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearSystem();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensSuccess').text('El registro '+Systemdescription.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentSuccess').show();
                }
            });
        }    
    });*/
});

function getClearSystem() {
    $('#Systemid').val('');
    $('#Systemstatus').val(1);
    $('#Systemdescription').val('');
    $('#Systemabreviation').val('');
    $("#btn-system").attr("disabled", false);
    //$('#tittleHorz').text('Editar');
    $("#btn-system").html("<li class='fa fa-save'></li> Registrar reg.");
    $('#btn-system').val('add');
    $('#tittle').text('REGISTRO');

    $('#campo4').val('');
    $('#campo4').val('');
    // Modal
    //$("body").removeAttr("style");
    //$('.jquery-modal').fadeOut(500);
    //var oTable = $('.ListSystemRefres').dataTable();
    //oTable.fnDraw(false);
}