$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready( function () {

    $('#listModels').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: SITEURL + "/listModels",
            type: 'GET',
        },

        columns: [
            { data: 'id', name: 'id', 'visible': false},
            { data: 'marks'},
            { data: 'description'},
            { data: 'abreviation' },
            { data: 'status' },
            { data: 'action' },
        ],
        order: [[0, 'desc']]
    });
        
    $('body').on('click', '.edit-models', function () {
       
        var Models = $(this).data("id").split('/');
        var Models_id = Models[0];
        var Models_name = Models[1];
        $.get('listModels/' + Models_id +'/edit', function (data) {
            
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-models').val('edit');
            $('#btn-models').text('Editar');

            // Combo de marca
            $('#Modelsidmark').val('');
            $('#Modelsidmark').empty();
            var marks=data.marks;
            var select=document.getElementsByName('Modelsidmark')[0];
            
            for (value in marks) {
                var option=document.createElement("option");
                    option.value=marks[value]['id'];
                    option.text=marks[value]['description'];
                    select.add(option);
            }

            // Combo de estatus
            $('#Modelsstatus').val('');
            $('#Modelsstatus').empty();
            var status=data.status;
            var select=document.getElementsByName('Modelsstatus')[0];
            
            for (value in status) {
                var option=document.createElement("option");
                    option.value=status[value]['id'];
                    option.text=status[value]['description'];
                    select.add(option);
            }

           //Register data
           $('#tittleModels').text('Editar');
           $('#tittleModelsHr').text('Editar');
           $('#Modelsid').val(data.models[0]['id']);
           $('#Modelsstatus').val(data.models[0]['status']);
           $('#Modelsidmark').val(data.models[0]['idmarks']);
           $('#Modelsdescription').val(data.models[0]['description']);
           $('#Modelsabreviation').val(data.models[0]['abreviation']);
        })
    });

    $('body').on('click', '.add-models', function () {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        $('#btn-models').val('Registrar');
        $('#btn-models').text('Registrar');
    
        var models = $(this).data("id");
        $.get('listModels/' + models +'/edit', function (data) {
                
            if(data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-models').val('add');
                
                // Combo de marca
                $('#Modelsidmark').val('');
                $('#Modelsidmark').empty();
                var marks=data.marks;
                var select=document.getElementsByName('Modelsidmark')[0];
                
                for (value in marks) {
                    var option=document.createElement("option");
                        option.value=marks[value]['id'];
                        option.text=marks[value]['description'];
                        select.add(option);
                }

                // Combo de estatus
                $('#Modelsstatus').val('');
                $('#Modelsstatus').empty();
                var status=data.status;
                var select=document.getElementsByName('Modelsstatus')[0];
                
                for (value in status) {
                    var option=document.createElement("option");
                        option.value=status[value]['id'];
                        option.text=status[value]['description'];
                        select.add(option);
                }
                
                //Register data
                $('#Modelsid').val('');
                $('#Modelsstatus').val('1');
                $('#Modelsidmark').val('1');
                $('#Modelsdescription').val('');
                $('#Modelsabreviation').val('');
                $('#tittleModels').text('Registrar');
                $('#tittleModelsHr').text('Registrar');
            }    
        });		
    });

    $('body').on('click', '#delete-models', function () {

        $('#contentDanger').hide();
        $('#contentSuccess').hide();

        var Models=$(this).data("id").split('/');
        var Models_id=Models[0];
        var Models_name=Models[1];
      
        if(confirm("Está seguro que desea eliminar el registro: "+Models_name.toUpperCase()+" !")) {
            $.ajax({
                type: "get",
                url: SITEURL + "/listModels/delete/"+Models_id,
                success: function (data) {
                    $('#mensSuccess').text('El registro '+Models_name.toUpperCase()+' se eliminó correctamente...');
                    $('#contentSuccess').show();
                    getClearModels();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensDanger').text('El registro '+Models_name.toUpperCase()+' no se ha podido eliminar, intentelo nuevamente...');
                    $('#contentDanger').show();
                }
            });
        }    
    });

    $("#models-submit").on("submit", function(e) {

        if($("#btn-models").val()=='edit') {
            var action='edit';
            var content='editó';
        } else {
            var action='register';
            var content='Registro';
        } 
       
        var btn=$("#btn-models").val();
        var Modelsid=$('#Modelsid').val();
        var Modelsstatus=$('#Modelsstatus').val();
        var Modelsidmark=$('#Modelsidmark').val();
        var Modelsdescription=$('#Modelsdescription').val();
        var Modelsabreviation=$('#Modelsabreviation').val();
      
        var yes=1;
        if(!Modelsidmark || !Modelsstatus || !Modelsdescription || !Modelsabreviation) {
            yes=0;
        } 
 
        if(yes > 0) {

            $("#btn-Models").attr("disabled", true);

            e.preventDefault();
            var f=$(this);
            var formData=new FormData(document.getElementById("models-submit"));
            formData.append("dato", "valor");

            $.ajax({
                url:SITEURL + "/listModels/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#mensSuccess').text('El registro '+Modelsdescription.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearModels();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensSuccess').text('El registro '+Modelsdescription.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentSuccess').show();
                }
            });
        }
    });
    
    /*$('body').on('click', '#btn-models', function () {
    
        if($("#btn-models").val()=='edit') {
            var action='edit';
            var content='editó';
        } else {
            var action='register';
            var content='Registro';
        } 
       
        var btn=$("#btn-models").val();
        var Modelsid=$('#Modelsid').val();
        var Modelsstatus=$('#Modelsstatus').val();
        var Modelsidmark=$('#Modelsidmark').val();
        var Modelsdescription=$('#Modelsdescription').val();
        var Modelsabreviation=$('#Modelsabreviation').val();
      
        var yes=1;
        if(!Modelsidmark || !Modelsstatus || !Modelsdescription || !Modelsabreviation) {
            yes=0;
        } 
 
        if(yes > 0) {

            $("#btn-Models").attr("disabled", true);

            $.ajax({
                data:{"action":action,"Modelsid":Modelsid,"Modelsstatus":Modelsstatus,"Modelsabreviation":Modelsabreviation,"Modelsidmark":Modelsidmark,"Modelsdescription":Modelsdescription},
                url: SITEURL + "/listModels/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#mensSuccess').text('El registro '+Modelsdescription.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearModels();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensSuccess').text('El registro '+Modelsdescription.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentSuccess').show();
                }
            });
        }    
    });*/  
});

function getClearModels() {

    $('#Modelsid').val('');
    $('#Modelsstatus').val('1');
    $('#Modelsidmark').val('1');
    $('#Modelsdescription').val('');
    $('#Modelsabreviation').val('');
    $("#btn-models").attr("disabled", false);
    $('.jquery-modal').fadeOut(500);

    $("body").removeAttr("style");
    var oTable = $('.listModelsRefres').dataTable();
    oTable.fnDraw(false);
}