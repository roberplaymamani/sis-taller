$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    $('#listEntryInventory').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: "/listEntryInventory",
            type: 'GET',
        },
        columns: [{
            data: 'id',
            name: 'id',
            'visible': false
        }, {
            data: 'description'
        }, {
            data: 'motive'
        }, {
            data: 'value_sale'
        }, {
            data: 'taxt'
        }, {
            data: 'total_net'
        }, {
            data: 'date_registration'
        }, {
            data: 'status'
        }, {
            data: 'action',
            'visible': false
        }, ],
        order: [
            [0, 'desc']
        ]
    });
    $('body').on('click', '.edit-entryedit ', function() {
        var entry = $(this).data("id").split('/');
        var entry_id = entry[0];
        var entry_name = entry[1];
        $.get('listEntryInventory/' + entry_id + '/edit', function(data) {
            //Register data
            $('#id_entry').val(data.entryNew[0]['id']);
            $('#EntryInventoryprice').val(data.entryNew[0]['price']);
            $('#EntryInventoryprice').val(data.entryNew[0]['price']);
            $('#EntryInventorytotal').val(data.entryNew[0]['total']);
            $('#EntryInventoryquantity').val(data.entryNew[0]['quantity']);
            $('#EntryInventoryid_products').val(data.entryNew[0]['codigo']);
            $('#EntryInventorydescription').val(data.entryNew[0]['id_products']);
            $('#id_entry_inventorys').val(data.entryNew[0]['id_entry_inventorys']);
            $('#id_entry_inventoryss').val(data.entryNew[0]['id_entry_inventorys']);
        })
    });
    $('body').on('click', '#delete-entrydel', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var entry = $(this).data("id").split('/');
        var entry_id = entry[0];
        var entry_name = entry[1];
        var id_entry_inventorys = $('#id_entry_inventorys').val();
        alertify.confirm("Esta seguro que desea eliminar el registro: " + entry_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "get",
                    url: "/listEntryInventory/delete/" + entry_id,
                    success: function(data) {
                        alertify.success('El registro ' + entry_name.toUpperCase() + ' se elimino correctamente...');
                        tableresfres(id_entry_inventorys);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        alertify.error('El registro ' + entry_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#entryinventory-submit").on("submit", function(e) {
        if ($("#btn-entryinventory").val() == 'edit') {
            var action = 'edit';
            var content = 'editÃ³';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var EntryInventoryid = $('#EntryInventoryid').val();
        var EntryInventorydate = $('#EntryInventorydate').val();
        var id_entry_inventoryss = $('#id_entry_inventoryss').val();
        var EntryInventorystatus = $('#EntryInventorystatus').val();
        var EntryInventoryid_store = $('#EntryInventoryid_store').val();
        var EntryInventoryid_coins = $('#EntryInventoryid_coins').val();
        var EntryInventorytotal_net = $('#EntryInventorytotal_net').val();
        var EntryInventoryid_reasons = $('#EntryInventoryid_reasons').val();
        var yes = 1;
        if (!EntryInventorydate || !EntryInventorystatus || !EntryInventoryid_store || !EntryInventoryid_coins) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-entryinventory").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("entryinventory-submit"));
            formData.append("dato", "valor");
            alertify.confirm("<b>Msj:</b> ¿Estas seguro de realizar esta operacion.? ", function(e) {
                if (e) {
                    $.ajax({
                        url: "/listEntryInventory/store",
                        type: "POST",
                        dataType: "json",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            alertify.success('El registro ' + EntryInventorydate.toUpperCase() + ' se ' + content + ' correctamente...');
                            filterClients();
                            AddInventarioDIV('list');
                            list_last_items($('#id_entry_inventorys').val());
                            getClearEntryInventory();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            alertify.error('El registro ' + EntryInventorydate.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                        }
                    });
                } else {
                    alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
                }
            });
        }
    });
    $("#entry-submit").on("submit", function(e) {
        if ($('#id_entry').val()) {
            var action = 'edit';
            var content = 'editÃ³';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var id_entry = $('#id_entry').val();
        var EntryInventoryprice = $('#EntryInventoryprice').val();
        var EntryInventorytotal = $('#EntryInventorytotal').val();
        var id_entry_inventorys = $('#id_entry_inventorys').val();
        var EntryInventoryquantity = $('#EntryInventoryquantity').val();
        var EntryInventorydescription = $('#EntryInventorydescription').val();
        var description = $('#EntryInventorydescription').find('option:selected').text();
        var yes = 1;
        if (!EntryInventoryprice || !EntryInventorytotal || !EntryInventoryquantity || !EntryInventorydescription || !id_entry_inventorys) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-newitems").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("entry-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/entryinventory/listEntryInventory/newitems",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    alertify.success('El registro1 ' + description.toUpperCase() + ' se ' + content + ' correctamente...');
                    tableresfres(id_entry_inventorys);
                },
                error: function(data) {
                    console.log('Error:', data);
                    alertify.error('El registro2 ' + description.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                }
            });
        }
    });
    $(document).on('click', '#paginates_inv a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && this.PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax = */
        $.ajax({
            type: 'get',
            url: '/entryinventory',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                cant_mostrar: $('#cant_mostrar').val(),
                typeClient: $('#typeClient').val()
            },
            success: function(rpta) {
                $('#listPaginates_inv').html(rpta.html);
                $('#paginates_inv').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList_inv', function(e) {
        e.preventDefault();
        filterClients();
    });
    $(document).on('change', '#filterInventory_x_date', function(e) {
        e.preventDefault();
        $('#searchList_inv').val('');
        filterClients();
    });
});

function filterClients() {
    var valuedateAdd = $('#filterInventory_x_date').val();
    if ($('#searchList_inv').val() != '' || $('#filterInventory_x_date').val() != '') {
        type_filter = '1';
        /*if ($('#searchList_inv').val()!= '') {

        }else{
            valuedateAdd=$('#filterInventory_x_date').val();
        }*/
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchList_inv').val();
    $.ajax({
        type: 'get',
        url: '/entryinventory',
        data: {
            search: type_filter,
            valueInput: valueInput,
            cant_mostrar: $('#cant_mostrar').val(),
            valuedate: valuedateAdd
        },
        success: function(rpta) {
            $('#listPaginates_inv').html(rpta.html);
            $('#paginates_inv').html(rpta.pagenationAdd);
        }
    });
}
/*
$('body').on('click', '.add-entryadd', function() {
        $('#contentDange').rhide();
        $('#contentSuccess').hide();
        $('#btn-entryinventory').val('add');
        $('#btn-entryinventory').text('Registrar');
        var entry = $(this).data("id");
        if (entry > 0) {
            var cond = 1;
        } else {
            var cond = 0;
        }
        $.get('listEntryInventory/0/edit', function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-entryinventory').val('add');
                if (cond > 0) {
                    $('#EntryExitInventory').val(cond);
                    $('.tittleEntryInventoryNew').text('Salida');
                    $('.tittleEntryInventoryHr').text('Registrar');
                    $('.tittleEntryInventory').text('Registrar Salida');
                    // Combo de salida
                    $('#EntryInventoryid_reasons').val('');
                    $('#EntryInventoryid_reasons').empty();
                    var exit = data.exit;
                    var select = document.getElementsByName('EntryInventoryid_reasons')[0];
                    var option = document.createElement("option");
                    option.value = '';
                    option.text = 'Seleccionar';
                    select.add(option);
                    for (value in exit) {
                        var option = document.createElement("option");
                        option.value = exit[value]['id'];
                        option.text = exit[value]['description'];
                        select.add(option);
                    }
                } else {
                    $('#EntryExitInventory').val(cond);
                    $('.tittleEntryInventoryNew').text('Ingreso');
                    $('.tittleEntryInventoryHr').text('Registrar');
                    $('.tittleEntryInventory').text('Registrar Ingreso');
                    // Combo de motivo de entrada
                    $('#EntryInventoryid_reasons').val('');
                    $('#EntryInventoryid_reasons').empty();
                    var entry = data.entry;
                    var select = document.getElementsByName('EntryInventoryid_reasons')[0];
                    var option = document.createElement("option");
                    option.value = '';
                    option.text = 'Seleccionar';
                    select.add(option);
                    for (value in entry) {
                        var option = document.createElement("option");
                        option.value = entry[value]['id'];
                        option.text = entry[value]['description'];
                        select.add(option);
                    }
                }
                // Combo de almacen
                $('#EntryInventoryid_store').val('');
                $('#EntryInventoryid_store').empty();
                var store = data.store;
                var select = document.getElementsByName('EntryInventoryid_store')[0];
                var option = document.createElement("option");
                option.value = '';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in store) {
                    var option = document.createElement("option");
                    option.value = store[value]['id'];
                    option.text = store[value]['description'];
                    select.add(option);
                }
                // Combo de moneda
                $('#EntryInventoryid_coins').val('');
                $('#EntryInventoryid_coins').empty();
                var coins = data.coins;
                var select = document.getElementsByName('EntryInventoryid_coins')[0];
                for (value in coins) {
                    var option = document.createElement("option");
                    option.value = coins[value]['id'];
                    option.text = coins[value]['description'];
                    select.add(option);
                }
                // Combo de estatus
                $('#EntryInventorystatus').val('');
                $('#EntryInventorystatus').empty();
                var status = data.status;
                var select = document.getElementsByName('EntryInventorystatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                // Combo de productos
                $('#EntryInventorydescription').val('');
                $('#EntryInventorydescription').empty();
                var products = data.products;
                var select = document.getElementsByName('EntryInventorydescription')[0];
                var option = document.createElement("option");
                option.value = '';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in products) {
                    var option = document.createElement("option");
                    option.value = products[value]['id'];
                    option.text = products[value]['description'];
                    select.add(option);
                }
                //Register data
                var date = new Date();
                $('#id_entry').val('');
                $('.textsymbol').text('S/.');
                $('#textNotification').show();
                $('#EntryInventoryid').val('');
                $('#EntryInventorystatus').val('1');
                $('#EntryInventoryid_store').val('');
                $('#EntryInventoryid_coins').val('1');
                $('#EntryInventoryidProducts').val('');
                $('#EntryInventoryid_reasons').val('');
                $('#EntryInventorydescription').val('');
                $("#btn-entryinventory").attr("disabled", true);
                $('#id_entry_inventorys').val(date.getSeconds() + '' + date.getMinutes() + '' + date.getMilliseconds());
                $('#id_entry_inventoryss').val(date.getSeconds() + '' + date.getMinutes() + '' + date.getMilliseconds());
                var table = $('#listEntry').DataTable();
                $('#listEntry').empty();
                //table.destroy();
            }
        });
    });
    */
function SelectAccion(valueClick) {
    $('#contentDanger').hide();
    $('#contentSuccess').hide();
    $('#btn-entryinventory').val('add');
    $('#btn-entryinventory').text('Registrar');
    var entry = valueClick;
    if (entry > 0) {
        var cond = 1;
    } else {
        var cond = 0;
    }
    $.get('listEntryInventory/0/edit', function(data) {
        if (data.status.length > 0) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-entryinventory').val('add');
            if (cond > 0) {
                $('#EntryExitInventory').val(cond);
                $('.tittleEntryInventoryNew').text('Salida');
                $('.tittleEntryInventoryHr').text('Registrar');
                $('.tittleEntryInventory').text('Registrar Salida');
                // Combo de salida
                $('#EntryInventoryid_reasons').val('');
                $('#EntryInventoryid_reasons').empty();
                var exit = data.exit;
                var select = document.getElementsByName('EntryInventoryid_reasons')[0];
                var option = document.createElement("option");
                option.value = '';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in exit) {
                    var option = document.createElement("option");
                    option.value = exit[value]['id'];
                    option.text = exit[value]['description'];
                    select.add(option);
                }
            } else {
                $('#EntryExitInventory').val(cond);
                $('.tittleEntryInventoryNew').text('Ingreso');
                $('.tittleEntryInventoryHr').text('Registrar');
                $('.tittleEntryInventory').text('Registrar Ingreso');
                // Combo de motivo de entrada
                $('#EntryInventoryid_reasons').val('');
                $('#EntryInventoryid_reasons').empty();
                var entry = data.entry;
                var select = document.getElementsByName('EntryInventoryid_reasons')[0];
                var option = document.createElement("option");
                option.value = '';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in entry) {
                    var option = document.createElement("option");
                    option.value = entry[value]['id'];
                    option.text = entry[value]['description'];
                    select.add(option);
                }
            }
            // Combo de almacen
            $('#EntryInventoryid_store').val('');
            $('#EntryInventoryid_store').empty();
            var store = data.store;
            var select = document.getElementsByName('EntryInventoryid_store')[0];
            var option = document.createElement("option");
            option.value = '';
            option.text = 'Seleccionar';
            select.add(option);
            for (value in store) {
                var option = document.createElement("option");
                option.value = store[value]['id'];
                option.text = store[value]['description'];
                select.add(option);
            }
            // Combo de moneda
            $('#EntryInventoryid_coins').val('');
            $('#EntryInventoryid_coins').empty();
            var coins = data.coins;
            var select = document.getElementsByName('EntryInventoryid_coins')[0];
            for (value in coins) {
                var option = document.createElement("option");
                option.value = coins[value]['id'];
                option.text = coins[value]['description'];
                select.add(option);
            }
            // Combo de estatus
            $('#EntryInventorystatus').val('');
            $('#EntryInventorystatus').empty();
            var status = data.status;
            var select = document.getElementsByName('EntryInventorystatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            // Combo de productos
            $('#EntryInventorydescription').val('');
            $('#EntryInventorydescription').empty();


            var products = data.products;
            var select = document.getElementsByName('EntryInventorydescription')[0];
            var option = document.createElement("option");
            option.value = '';
            option.text = 'Seleccionar';
            select.add(option);
            for (value in products) {
                var option = document.createElement("option");
                option.value = products[value]['id'];
                option.text = products[value]['description'];
                select.add(option);
            }

            /*var datalist = document.getElementsByName('lisproducto')[0];
            var option1 = document.createElement("option");
            option1.value = '';
            option1.text = 'Seleccionar';
            datalist.add(option1);
            for (value in products) {
                var option1 = document.createElement("option");
                option1.value = products[value]['id'];
                option1.text = products[value]['description'];
                datalist.add(option1);
            }*/
            var listOptions='';
            $.each(data.products, function(k, v) {
                var id = data.products[k].id_products;
                var Desc = data.products[k].description;
                listOptions += '<option value="'+id+'">'+Desc+'</option>';
            });
            $('#lisproducto').html(listOptions);


            //Register data
            var date = new Date();
            $('#id_entry').val('');
            $('.textsymbol').text('S/.');
            $('#textNotification').show();
            $('#EntryInventoryid').val('');
            $('#EntryInventorystatus').val('1');
            $('#EntryInventoryid_store').val('');
            $('#EntryInventoryid_coins').val('1');
            $('#EntryInventoryidProducts').val('');
            $('#EntryInventoryid_reasons').val('');
            $('#EntryInventorydescription').val('');
            $("#btn-entryinventory").attr("disabled", true);
            $('#id_entry_inventorys').val(date.getSeconds() + '' + date.getMinutes() + '' + date.getMilliseconds());
            $('#id_entry_inventoryss').val(date.getSeconds() + '' + date.getMinutes() + '' + date.getMilliseconds());
            var table = $('#listEntry').DataTable();
            $('#listEntry').empty();
            //table.destroy();
        }
    });
}

function AddInventarioDIV(valueAdd) {
    if (valueAdd == 'list') {
        $('#list').prop('disabled', true);
        $('#addInv').prop('disabled', false);
        $('#salInv').prop('disabled', false);
        $('#divSalidaIngresoInvList').prop('hidden', false);
        $('#divSalidaIngresoInv').prop('hidden', true);
        $('#searchList_inv').prop('readonly', false);
        $('#filterInventory_x_date').prop('readonly', false);
    } else if (valueAdd == 'addInv') {
        SelectAccion(0);
        $('#list').prop('disabled', false);
        $('#addInv').prop('disabled', true);
        $('#salInv').prop('disabled', false);
        $('#divSalidaIngresoInvList').prop('hidden', true);
        $('#divSalidaIngresoInv').prop('hidden', false);
        $('#tipoAdd').html('INGRESOS');
        $('#detallesAdd').html('INGRESO');
        $('#btn-entryinventory').html('<li class="fa fa-plus-square fa-lg"></li> REGISTRAR INGRESO.');
        $('#cancelarRegistro').html('<li class="fa fa-close fa-lg"></li> CANCELAR INGRESO.');
        $('#searchList_inv').prop('readonly', true);
        $('#filterInventory_x_date').prop('readonly', true);
    } else if (valueAdd == 'salInv') {
        SelectAccion(1);
        $('#list').prop('disabled', false);
        $('#addInv').prop('disabled', false);
        $('#salInv').prop('disabled', true);
        $('#divSalidaIngresoInvList').prop('hidden', true);
        $('#divSalidaIngresoInv').prop('hidden', false);
        $('#tipoAdd').html('SALIDAS');
        $('#detallesAdd').html('SALIDA');
        $('#btn-entryinventory').html('<li class="fa fa-plus-square fa-lg"></li> REGISTRAR SALIDA.');
        $('#cancelarRegistro').html('<li class="fa fa-close fa-lg"></li> CANCELAR SALIDA.');
        $('#searchList_inv').prop('readonly', true);
        $('#filterInventory_x_date').prop('readonly', true);
    }
}
function download_pdf_reporte() {
    if ($('#searchList_inv').val()!='') {
        seachAdd=$('#searchList_inv').val();
    }else{
        seachAdd='0';
    }
    if ($('#filterInventory_x_date').val()!='') {
        dateAdd=$('#filterInventory_x_date').val();
    }else{
        dateAdd='0';
    }
    window.open('/download_pdf_reporte/'+seachAdd+'/'+dateAdd);
}

function getClearEntryInventory() {
    $('#EntryInventoryid').val('');
    $('#EntryExitInventory').val('');
    $('#EntryInventorytaxt').val('');
    $('#EntryInventoryprice').val('');
    $('#EntryInventorytotal').val('');
    $('#id_entry_inventorys').val('');
    $('#id_entry_inventoryss').val('');
    $('#EntryInventorystatus').val('');
    $('#EntryInventoryquantity').val('');
    $('#EntryInventoryid_store').val('');
    $('#EntryInventoryid_coins').val('');
    $('#EntryInventorytotal_net').val('');
    $('#EntryInventorytotal_nets').val('');
    $('#EntryInventoryvalue_sale').val('');
    $('#EntryInventoryid_reasons').val('');
    $('#EntryInventorydescription').val('');
    $("#btn-entryinventory").attr("disabled", false);
    $('.jquery-modal').fadeOut(500);
    $("body").removeAttr("style");
    var oTable = $('.listEntryInventoryRefres').dataTable();
    oTable.fnDraw(false);
    var oTable = $('.listEntryRefres').dataTable();
    oTable.fnDraw(false);
    var table = $('#listEntry').DataTable();
    $('#listEntry').empty();
    table.destroy()
}

function getClearNewitems(val) {
    $('#id_entry').val('');
    $('#EntryInventoryprice').val('');
    $('#EntryInventorytotal').val('');
    $('#EntryInventoryquantity').val('');
    $('#EntryInventorydescription').val('');
    $('#EntryInventoryid_products').val('');
    $("#btn-newitems").attr("disabled", false);
    if (val > 0) {
        $("body").removeAttr("style");
        var oTable = $('.listEntryRefres').dataTable();
        oTable.fnDraw(false);
    }
}

function list_last_items(val) {
    $.ajax({
        data: {
            "id": val
        },
        url: "/entryinventory/listEntryInventory/search",
        type: 'POST',
        success: function(rptadata) {
            $('#listitemsAdd').html(rptadata.html);
        }
    });
}

function tableresfres(val) {
    $("#listEntry").dataTable().fnDestroy();
    /*$('#listEntry').DataTable({
        "bPaginate": false,
        "searching": false,
        ajax: {
            data:{"id":val},
            url:  "/entryinventory/listEntryInventory/search",
            type: 'POST',
        },
        
        columns: [
            {data:'description'},
            {data:'quantity'},
            {data:'price'},
            {data:'total',class:'total'},
            {data:'action'},
        ],
        order: [
            [0, 'desc']
        ] 
    });*/
    list_last_items(val);
    getClearNewitems(1);
    var I = 0;
    var QI = 0;
    var sum = 0;
    var total = 0;
    var tax = '18';
    $.post("/entryinventory/listEntryInventory/sum", {
        "id": val
    }, function(data) {
        var entryinventory = data.entryinventory;
        if (entryinventory.length > 0) {
            for (value in entryinventory) {
                sum = entryinventory[value]['total'];
                sum = sum.replace(',', '');
                total += parseFloat(sum);
                var I = parseFloat(total * tax / 100);
                var QI = (total - I);
            }
            $('#EntryInventorytaxt').val(formatMoney(I));
            $('#EntryInventoryvalue_sale').val(formatMoney(QI));
            $('#EntryInventorytotal_net').val(formatMoney(total));
            $('#EntryInventorytotal_nets').val(formatMoney(total));
            $('#textNotification').hide();
            $("#btn-entryinventory").attr("disabled", false);
        } else {
            $('#EntryInventorytaxt').val('');
            $('#EntryInventorytotal_net').val('');
            $('#EntryInventorytotal_nets').val('');
            $('#EntryInventoryvalue_sale').val('');
            $('#textNotification').show();
            $("#btn-entryinventory").attr("disabled", true);
        }
    });
}