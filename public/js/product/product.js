$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    $(document).on('click', '#paginates a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var Name = 'DataNulo';
        if ($('#name').val() != '') {
            var Name = $('#name').val();
        }
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && this.PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax = */
        $.ajax({
            type: 'get',
            url: '/products',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput,
                cant_mostrar: $('#cant_mostrar').val()
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList', function(e) {
        e.preventDefault();
        filterProducts();
    });
    $('body').on('click', '.edit-products', function() {
        var products = $(this).data("id").split('/');
        var products_id = products[0];
        var products_name = products[1];
        $.get('listProducts/' + products_id + '/edit', function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-products').val('edit');
            $('#btn-products').html('<li class="fa fa-edit"></li> Editar reg.');
            $('#frmprod').html('EDICION');
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-advisors').val('add');
                $('#tittleSubProduct').text('Editar');
                $('#tittleSubProductHr').text('Editar');
                // Combo de productos
                $('#Productsid_lines').val('');
                $('#Productsid_lines').empty();
                var linesproducts = data.linesproducts;
                var select = document.getElementsByName('Productsid_lines')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in linesproducts) {
                    var option = document.createElement("option");
                    option.value = linesproducts[value]['id'];
                    option.text = linesproducts[value]['description'];
                    select.add(option);
                }
                // Combo de sub productos
                $('#Productsid_sublines').val('');
                $('#Productsid_sublines').empty();
                var subproducts = data.subproducts;
                var select = document.getElementsByName('Productsid_sublines')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in subproducts) {
                    var option = document.createElement("option");
                    option.value = subproducts[value]['id'];
                    option.text = subproducts[value]['description'];
                    select.add(option);
                }
                // Combo de unidad de medicion
                $('#Productsid_measureunit').val('');
                $('#Productsid_measureunit').empty();
                var measurementunits = data.measurementunits;
                var select = document.getElementsByName('Productsid_measureunit')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in measurementunits) {
                    var option = document.createElement("option");
                    option.value = measurementunits[value]['id'];
                    option.text = measurementunits[value]['description'];
                    select.add(option);
                }
                // Combo marca de productos
                $('#Productsid_marks').val('');
                $('#Productsid_marks').empty();
                var brandsrepuestos = data.brandsrepuestos;
                var select = document.getElementsByName('Productsid_marks')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in brandsrepuestos) {
                    var option = document.createElement("option");
                    option.value = brandsrepuestos[value]['id'];
                    option.text = brandsrepuestos[value]['description'];
                    select.add(option);
                }
                // Combo ciudad de origen
                $('#Productsid_country').val('');
                $('#Productsid_country').empty();
                var countryorigin = data.countryorigin;
                var select = document.getElementsByName('Productsid_country')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in countryorigin) {
                    var option = document.createElement("option");
                    option.value = countryorigin[value]['id'];
                    option.text = countryorigin[value]['description'];
                    select.add(option);
                }
                // Combo de estatus
                $('#Productsstatus').val('');
                $('#Productsstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Productsstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                // Combo de moneda
                $('#Productsid_coins').val('');
                $('#Productsid_coins').empty();
                var coins = data.coins;
                var select = document.getElementsByName('Productsid_coins')[0];
                for (value in coins) {
                    var option = document.createElement("option");
                    option.value = coins[value]['id'];
                    option.text = coins[value]['description'];
                    select.add(option);
                }
                // Combo de proveedor
                $('#Productsid_provider').val('');
                $('#Productsid_provider').empty();
                var clients = data.clients;
                var select = document.getElementsByName('Productsid_provider')[0];
                for (value in clients) {
                    var option = document.createElement("option");
                    option.value = clients[value]['id'];
                    option.text = clients[value]['name'];
                    select.add(option);
                }
                //Register data
                if (data.products[0]['symbol']) {
                    $('.textsymbol').text(data.products[0]['symbol']);
                } else {
                    $('.textsymbol').text('S/.');
                }
                if (data.products[0]['imagen']) {
                    $("#file-info").text(data.products[0]['imagen']);
                    $("#ProductsiamgeAct").val(data.products[0]['imagen']);
                    $('#selectImagen').attr('src', '/img/products/' + data.products[0]['imagen']);
                } else {
                    $("#ProductsiamgeAct").val('');
                    $("#file-info").text('No hay archivo aún');
                    $('#selectImagen').attr('src', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=');
                }
                $('.textporcent').text('%');
                $('#Productsid').val(data.products[0]['id']);
                $('#ProductsPVP').val(data.products[0]['PVP']);
                $('#Productstax').val(data.products[0]['tax']);
                $('#Productscost').val(data.products[0]['cost']);
                $('#Productsgain').val(data.products[0]['gain']);
                $('#Productsstatus').val(data.products[0]['status']);
                $('#Productstaxporc').val(data.products[0]['by_tax']);
                $('#Productscode').val(data.products[0]['id_products']);
                $('#Productsgainporc').val(data.products[0]['by_profit']);
                $('#Productsid_coins').val(data.products[0]['id_coins']);
                $('#Productswarranty').val(data.products[0]['warranty']);
                $('#Productsid_lines').val(data.products[0]['id_lines']);
                $('#Productsid_marks').val(data.products[0]['id_marks']);
                $('#Productsid_country').val(data.products[0]['id_country']);
                $('#Productsid_provider').val(data.products[0]['id_provider']);
                $('#Productsdescription').val(data.products[0]['description']);
                $('#Productsabreviation').val(data.products[0]['abreviation']);
                $('#Productsid_sublines').val(data.products[0]['id_sublines']);
                $('#Productsid_measureunit').val(data.products[0]['id_measureunit']);
                $('#Productsprice_purchasec').val(data.products[0]['price_purchase']);
            }
        });
        AddproductoDIV('divListaproducto');
    });
    $('body').on('click', '.add-products', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        $('#btn-products').val('Registrar');
        $('#btn-products').html('<li class="fa fa-save"></li> Registrar reg.');
        var products = $(this).data("id");
        $.get('listProducts/' + products + '/edit', function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-advisors').val('add');
                // Combo de productos
                $('#Productsid_lines').val('');
                $('#Productsid_lines').empty();
                var linesproducts = data.linesproducts;
                var select = document.getElementsByName('Productsid_lines')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in linesproducts) {
                    var option = document.createElement("option");
                    option.value = linesproducts[value]['id'];
                    option.text = linesproducts[value]['description'];
                    select.add(option);
                }
                // Combo de sub productos
                $('#Productsid_sublines').val('');
                $('#Productsid_sublines').empty();
                var subproducts = data.subproducts;
                var select = document.getElementsByName('Productsid_sublines')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in subproducts) {
                    var option = document.createElement("option");
                    option.value = subproducts[value]['id'];
                    option.text = subproducts[value]['description'];
                    select.add(option);
                }
                // Combo de unidad de medicion
                $('#Productsid_measureunit').val('');
                $('#Productsid_measureunit').empty();
                var measurementunits = data.measurementunits;
                var select = document.getElementsByName('Productsid_measureunit')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in measurementunits) {
                    var option = document.createElement("option");
                    option.value = measurementunits[value]['id'];
                    option.text = measurementunits[value]['description'];
                    select.add(option);
                }
                // Combo marca de productos
                $('#Productsid_marks').val('');
                $('#Productsid_marks').empty();
                var brandsrepuestos = data.brandsrepuestos;
                var select = document.getElementsByName('Productsid_marks')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in brandsrepuestos) {
                    var option = document.createElement("option");
                    option.value = brandsrepuestos[value]['id'];
                    option.text = brandsrepuestos[value]['description'];
                    select.add(option);
                }
                // Combo ciudad de origen
                $('#Productsid_country').val('');
                $('#Productsid_country').empty();
                var countryorigin = data.countryorigin;
                var select = document.getElementsByName('Productsid_country')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in countryorigin) {
                    var option = document.createElement("option");
                    option.value = countryorigin[value]['id'];
                    option.text = countryorigin[value]['description'];
                    select.add(option);
                }
                // Combo de estatus
                $('#Productsstatus').val('');
                $('#Productsstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Productsstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                // Combo de moneda
                $('#Productsid_coins').val('');
                $('#Productsid_coins').empty();
                var coins = data.coins;
                var select = document.getElementsByName('Productsid_coins')[0];
                for (value in coins) {
                    var option = document.createElement("option");
                    option.value = coins[value]['id'];
                    option.text = coins[value]['description'];
                    select.add(option);
                }
                // Combo de proveedor
                $('#Productsid_provider').val('');
                $('#Productsid_provider').empty();
                var clients = data.clients;
                var select = document.getElementsByName('Productsid_provider')[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'Seleccionar';
                select.add(option);
                for (value in clients) {
                    var option = document.createElement("option");
                    option.value = clients[value]['id'];
                    option.text = clients[value]['name'];
                    select.add(option);
                }
                //Register data
                $("#file").val('');
                $('#file-info').text('');
                $('#Productsid').val('');
                $('#ProductsPVP').val('');
                $('#Productstax').val('');
                $('#Productscost').val('');
                $('#Productsgain').val('');
                $('#Productscode').val('');
                $('.textporcent').text('%');
                $('.textsymbol').text('S/.');
                $('#Productsstatus').val('1');
                $('#Productstaxporc').val('');
                $('#Productstaxporc').val('');
                $('#Productsgainporc').val('');
                $("#ProductsiamgeAct").val('');
                $('#Productsid_coins').val('1');
                $('#Productswarranty').val('0');
                $('#Productsid_lines').val('0');
                $('#Productsid_marks').val('0');
                $('#Productsid_country').val('0');
                $('#Productsid_provider').val('0');
                $('#Productsdescription').val('');
                $('#Productsabreviation').val('');
                $('#Productsid_sublines').val('0');
                $('#Productsid_measureunit').val('0');
                $('#Productsprice_purchasec').val('');
                $("#file-info").text('No hay archivo aún');
                $('#selectImagen').attr('src', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=');
                $('#tittleProducts').text('Registrar');
                $('#tittleProductsHr').text('Registrar');
            }
        });
    });
    $('body').on('click', '#delete-products', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var products = $(this).data("id").split('/');
        var products_id = products[0];
        var products_name = products[1];
        alertify.confirm("Está seguro que desea eliminar el producto: " + products_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "get",
                    url: "/listProducts/delete/" + products_id,
                    success: function(data) {
                        //$('#mensSuccess').text('El producto ' + products_name.toUpperCase() + ' se eliminó correctamente...');
                        alertify.success('El producto ' + products_name.toUpperCase() + ' se eliminó correctamente...');
                        //$('#contentSuccess').show();
                        filterProducts();
                        getClearProducts();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#mensDanger').text('El producto ' + products_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        alertify.error('El producto ' + products_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        //$('#contentDanger').show();
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#products-submit").on("submit", function(e) {
        alert('AQUII');
        if ($("#btn-products").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
        } else {
            var action = 'register';
            var content = 'Registro';
        }
        var yes = 1;
        var Productscode = $('#Productscode').val();
        var Productsstatus = $('#Productsstatus').val();
        var Productsdescription = $('#Productsdescription').val();
        var Productsabreviation = $('#Productsabreviation').val();
        if (!Productsdescription || !Productsabreviation || !Productsstatus || !Productscode) {
            yes = 0;
        }
        if (yes > 0) {
            $("#btn-products").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("products-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/listProducts/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    //$('#mensSuccess').text('El producto ' + Productsdescription.toUpperCase() + ' se ' + content + ' correctamente...');
                    //$('#contentSuccess').show();
                    alertify.success('El producto ' + Productsdescription.toUpperCase() + ' se ' + content + ' correctamente...');
                    filterProducts();
                    AddproductoDIV($('#btnAdd').val());
                    getClearProducts();
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#mensSuccess').text('El producto ' + Productsdescription.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    //$('#contentSuccess').show();
                    alertify.error('El producto ' + Productsdescription.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                }
            });
        }
    });
    /*$('body').on('click', '#btn-products', function () {
        
        if($("#btn-products").val()=='edit') {
            var action='edit';
            var content='editó';
        } else {
            var action='register';
            var content='Registro';
        } 
       
        var btn=$("#btn-products").val();
        var Productsid=$('#Productsid').val();
        var ProductsPVP=$('#ProductsPVP').val();
        var Productstax=$('#Productstax').val();
        var Productscost=$('#Productscost').val();
        var Productsgain=$('#Productsgain').val();
        var Productscode=$('#Productscode').val();
        var Productsstatus=$('#Productsstatus').val();
        var Productstaxporc=$('#Productstaxporc').val();
        var Productsgainporc=$('#Productsgainporc').val();
        var Productsid_coins=$('#Productsid_coins').val();
        var Productswarranty=$('#Productswarranty').val();
        var Productsid_lines=$('#Productsid_lines').val();
        var Productsid_marks=$('#Productsid_marks').val();
        var Productsid_country=$('#Productsid_country').val();
        var Productsid_provider=$('#Productsid_provider').val();
        var Productsdescription=$('#Productsdescription').val();
        var Productsabreviation=$('#Productsabreviation').val();
        var Productsid_sublines=$('#Productsid_sublines').val();
        var Productsdate_purchase=$('#Productsdate_purchase').val();
        var Productsid_measureunit=$('#Productsid_measureunit').val();
        var Productsprice_purchasec=$('#Productsprice_purchasec').val();
        
        var yes=1;
        if(!Productsdescription || !Productsabreviation || !Productsstatus || !Productscode) {
            yes=0;
        } 

        if(yes > 0) {

            $("#btn-products").attr("disabled", true);

            $.ajax({
                data:{"action":action,"Productsid":Productsid,"ProductsPVP":ProductsPVP,"Productstax":Productstax,"Productscost":Productscost,"Productsgain":Productsgain ,"Productscode":Productscode,"Productsstatus":Productsstatus,"Productstaxporc":Productstaxporc,"Productsgainporc":Productsgainporc,"Productsid_coins":Productsid_coins,"Productswarranty":Productswarranty,"Productsid_lines":Productsid_lines,"Productsid_marks":Productsid_marks,"Productsid_country":Productsid_country,"Productsid_provider":Productsid_provider,"Productsdescription":Productsdescription,"Productsabreviation":Productsabreviation,"Productsid_sublines":Productsid_sublines,"Productsid_measureunit":Productsid_measureunit,"Productsdate_purchase":Productsdate_purchase,"Productsprice_purchasec":Productsprice_purchasec},
                url: "/listProducts/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#mensSuccess').text('El producto '+Productsdescription.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearProducts();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensSuccess').text('El producto '+Productsdescription.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentSuccess').show();
                }
            });
        }    
    }); */
});

function filterProducts() {
    if ($('#searchList').val() != '') {
        type_filter = '1';
    } else {
        type_filter = '0';
    }
    valueInput = $('#searchList').val();
    $.ajax({
        type: 'get',
        url: '/products',
        data: {
            search: type_filter,
            valueInput: valueInput,
            cant_mostrar: $('#cant_mostrar').val()
        },
        success: function(rpta) {
            $('#listPaginates').html(rpta.html);
            $('#paginates').html(rpta.pagenationAdd);
        }
    });
}

function sublines(subline, line) {
    $('#' + subline).val('');
    $('#' + subline).empty();
    $("#" + line + " option:selected").each(function() {
        id = $(this).val();
        $.post("/products/listProducts/subline", {
            "id": id
        }, function(data) {
            if (data.length > 0) {
                var select = document.getElementsByName(subline)[0];
                for (value in data) {
                    var option = document.createElement("option");
                    option.value = data[value]['id'];
                    option.text = data[value]['description'];
                    select.add(option);
                }
            } else {
                var select = document.getElementsByName(subline)[0];
                var option = document.createElement("option");
                option.value = '0';
                option.text = 'No hay registro';
                select.add(option);
            }
        });
    });
}

function AddproductoDIV(valueAdd) {
    if (valueAdd == 'divListaproducto') {
        $('#divListaproducto').prop('hidden', true);
        $('#divRegistrarproducto').prop('hidden', false);
        $('#btnAdd').val('divRegistrarproducto');
        $('#btnAdd').html('<span class="fa fa-list"></span>&nbsp;Lista productos');
        $('#searchList').prop('readonly', true);
    } else {
        $('#divListaproducto').prop('hidden', false);
        $('#divRegistrarproducto').prop('hidden', true);
        $('#btnAdd').val('divListaproducto');
        $('#btnAdd').html('<span class="fa fa-pencil"></span>&nbsp;Agregar producto');
        $('#searchList').prop('readonly', false);
        $('#frmprod').html('REGISTRO');
    }
}

function getClearProducts() {
    $("#file").val('');
    $("#file").empty('');
    $('#Productsid').val('');
    $("#file-info").text('');
    $('#file-info').text('');
    $('#ProductsPVP').val('');
    $('#Productstax').val('');
    $('#Productscost').val('');
    $('#Productsgain').val('');
    $('#Productscode').val('');
    $('.textporcent').text('%');
    $('.textsymbol').text('S/.');
    $('#Productsstatus').val('1');
    $('#Productstaxporc').val('');
    $('#Productsgainporc').val('');
    $("#ProductsiamgeAct").val('');
    $('#Productsid_coins').val('1');
    $('#Productswarranty').val('0');
    $('#Productsid_lines').val('0');
    $('#Productsid_marks').val('0');
    $('#Productsid_country').val('0');
    $('#Productsid_provider').val('0');
    $('#Productsdescription').val('');
    $('#Productsabreviation').val('');
    $('#Productsid_sublines').val('0');
    $('#Productsid_measureunit').val('0');
    $('#Productsprice_purchasec').val('');
    $('#selectImagen').attr('src', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=');
    $("#btn-products").attr("disabled", false);
    $('.jquery-modal').fadeOut(500);
    $("body").removeAttr("style");
    /*var oTable = $('.listsProductsRefres').dataTable();
    oTable.fnDraw(false);*/
    $('#btn-products').val('Registrar');
    $('#btn-products').html('<li class="fa fa-save"></li> Registrar reg.');
    $('#frmprod').html('REGISTRO');
}