$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    var PeticionAjax = 'null';
    $(document).on('click', '#paginates a', function(e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && this.PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax = */
        $.ajax({
            type: 'get',
            url: '/listUser',
            data: {
                page: page,
                search: type_filter,
                valueInput: valueInput
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    });
    $(document).on('keyup', '#searchList', function(e) {
        e.preventDefault();
        filterusers();
    });

    function filterusers() {
        if ($('#searchList').val() != '') {
            type_filter = '1';
        } else {
            type_filter = '0';
        }
        valueInput = $('#searchList').val();
        /*if (PeticionAjax && PeticionAjax.readyState != 4) {
            PeticionAjax.abort();
        }
        PeticionAjax =*/
        $.ajax({
            type: 'get',
            url: '/listUser',
            data: {
                search: type_filter,
                valueInput: valueInput
            },
            success: function(rpta) {
                $('#listPaginates').html(rpta.html);
                $('#paginates').html(rpta.pagenationAdd);
            }
        });
    }
    $('body').on('click', '.edit-user', function() {
        var user = $(this).data("id").split('/');
        var user_id = user[0];
        var user_name = user[1];
        $.get('listUser/' + user_id + '/edit', function(data) {
            $('#contentDanger').hide();
            $('#contentSuccess').hide();
            $('#btn-user').val('edit');
            $('#btn-user').html('<li class="fa fa-edit"></li> Editar us.');
            $('#tittle').html('EDICION');
            $("#btn-user").attr("disabled", false);
            $('#Userpassword').prop("required", false);
            $('#Userpassword2').prop("required", false);
            // Combo de perfiles
            $('#Userprofile').val('');
            $('#Userprofile').empty();
            var privileges = data.privileges;
            var select = document.getElementsByName('Userprofile')[0];
            for (value in privileges) {
                var option = document.createElement("option");
                option.value = privileges[value]['id'];
                option.text = privileges[value]['description'];
                select.add(option);
            }
            // Combo de estatus
            $('#Userstatus').val('');
            $('#Userstatus').empty();
            var status = data.status;
            var select = document.getElementsByName('Userstatus')[0];
            for (value in status) {
                var option = document.createElement("option");
                option.value = status[value]['id'];
                option.text = status[value]['description'];
                select.add(option);
            }
            //Register data
            $('#Userpassword').val('');
            $('#Userpassword2').val('');
            $('#Userid').val(data.user['id']);
            $('#Username').val(data.user['name']);
            $('#Useruser').val(data.user['email']);
            $('#Userstatus').val(data.user['status']);
            $('#UserpasswordAct').val(data.user['password']);
            $('#Userprofile').val(data.user['id_privileges']);
            $('#tittleUsers').text('Editar');
            $('#tittleUsersHr').text('Editar');
        })
    });
    $('body').on('click', '.add-user', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        $('#btn-user').val('Registrar');
        $('#btn-user').text('Registrar');
        $("#btn-user").attr("disabled", false);
        $('#Userpassword').prop("required", true);
        $('#Userpassword2').prop("required", true);
        var users = $(this).data("id");
        $.get('listUser/' + users + '/edit', function(data) {
            if (data.status.length > 0) {
                $('#contentDanger').hide();
                $('#contentSuccess').hide();
                $('#btn-advisors').val('add');
                // Combo de perfiles
                $('#Userprofile').val('');
                $('#Userprofile').empty();
                var privileges = data.privileges;
                var select = document.getElementsByName('Userprofile')[0];
                for (value in privileges) {
                    var option = document.createElement("option");
                    option.value = privileges[value]['id'];
                    option.text = privileges[value]['description'];
                    select.add(option);
                }
                // Combo de estatus
                $('#Userstatus').val('');
                $('#Userstatus').empty();
                var status = data.status;
                var select = document.getElementsByName('Userstatus')[0];
                for (value in status) {
                    var option = document.createElement("option");
                    option.value = status[value]['id'];
                    option.text = status[value]['description'];
                    select.add(option);
                }
                //Register data
                $('#Userid').val('');
                $('#Username').val('');
                $('#Useruser').val('');
                $('#Userstatus').val('1');
                $('#Userprofile').val('2');
                $('#Userpassword').val('');
                $('#Userpassword2').val('');
                $('#UserpasswordAct').val('');
                $('#tittleUsers').text('Registrar');
                $('#tittleUsersHr').text('Registrar');
            }
        });
    });
    $('body').on('click', '#delete-user', function() {
        $('#contentDanger').hide();
        $('#contentSuccess').hide();
        var user = $(this).data("id").split('/');
        var user_id = user[0];
        var user_name = user[1];
        alertify.confirm("Está seguro que desea eliminar el usuario con el NOMBRE: " + user_name.toUpperCase() + " !", function(e) {
            if (e) {
                $.ajax({
                    type: "get",
                    url: "/listUser/delete/" + user_id,
                    success: function(data) {
                        //$('#mensSuccess').text('El usuario con el nombre ' + user_name.toUpperCase() + ' se eliminó correctamente...');
                        alertify.success('El usuario con el nombre ' + user_name.toUpperCase() + ' se eliminó correctamente...');
                        filterusers();
                        //$('#contentSuccess').show();
                        getClearUsers();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#mensDanger').text('El usuario con el nombre ' + user_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        alertify.error('El usuario con el nombre ' + user_name.toUpperCase() + ' no se ha podido eliminar, intentelo nuevamente...');
                        //$('#contentDanger').show();
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
    });
    $("#users-submit").on("submit", function(e) {
        if ($("#btn-user").val() == 'edit') {
            var action = 'edit';
            var content = 'editó';
            var Username = $("#Username").val();
            var Useruser = $("#Useruser").val();
            var Userstatus = $("#Userstatus").val();
            var Userprofile = $("#Userprofile").val();
            var Userpassword = $("#Userpassword").val();
            var Userpassword2 = $("#Userpassword2").val();
            var yes = 1;
            if (!Username || !Useruser || !Userprofile || !Userstatus) {
                yes = 0;
            }
            if (Userpassword) {
                if (Userpassword != Userpassword2) {
                    alertify.alert('confirmación de la Contraseña no coincide con la Contraseña !');
                    return false;
                }
            } else {
                var Userpassword = $("#UserpasswordAct").val();
            }
        } else {
            var action = 'register';
            var content = 'Registro';
            var Username = $("#Username").val();
            var Useruser = $("#Useruser").val();
            var Userstatus = $("#Userstatus").val();
            var Userprofile = $("#Userprofile").val();
            var Userpassword = $("#Userpassword").val();
            var Userpassword2 = $("#Userpassword2").val();
            var yes = 1;
            if (!Username || !Useruser || !Userprofile || !Userpassword || !Userstatus) {
                yes = 0;
            }
            if (Userpassword != Userpassword2) {
                alertify.alert('confirmación de la Contraseña no coincide con la Contraseña !');
                return false;
            }
        }
        if (yes > 0) {
            $("#btn-user").attr("disabled", true);
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("users-submit"));
            formData.append("dato", "valor");
            $.ajax({
                url: "/listUser/store",
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    //$('#mensSuccess').text('El usuario con el nombre ' + Username.toUpperCase() + ' se ' + content + ' correctamente...');
                    alertify.success('El usuario con el nombre ' + Username.toUpperCase() + ' se ' + content + ' correctamente...');
                    //$('#contentSuccess').show();
                    filterusers();
                    getClearUsers();
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#mensDanger').text('El usuario con el nombre ' + Username.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    alertify.error('El usuario con el nombre ' + Username.toUpperCase() + ' no se ha podido ' + content + ', intentelo nuevamente...');
                    //$('#contentDanger').show();
                }
            });
        }
    });
    /*$('body').on('click', '#btn-user', function () {
     
        if($("#btn-user").val()=='edit') {

            var action='edit';
            var content='editó';
            var Userid=$("#Userid").val();
            var Username=$("#Username").val();
            var Useruser=$("#Useruser").val();
            var Userstatus=$("#Userstatus").val();
            var Userprofile=$("#Userprofile").val();
            var Userpassword=$("#Userpassword").val();
            var Userpassword2=$("#Userpassword2").val();

            var yes=1;
            if(!Username || !Useruser || !Userprofile || !Userstatus) {
                yes=0;
            }

            if(Userpassword) {
                if(Userpassword!=Userpassword2) {
                    alert('confirmación de la Contraseña no coincide con la Contraseña !');return;
                }
            } else {
                var Userpassword=$("#UserpasswordAct").val();
            }

        } else {
            
            var action='register';
            var content='Registro';
            var Userid=$("#Userids").val();
            var Username=$("#Usernames").val();
            var Useruser=$("#Userusers").val();
            var Userstatus=$("#Userstatuss").val();
            var Userprofile=$("#Userprofiles").val();
            var Userpassword=$("#Userpasswords").val();
            var Userpassword2=$("#Userpassword2s").val();

            var yes=1;
            if(!Username || !Useruser || !Userprofile || !Userpassword || !Userstatus) {
                yes=0;
            }

            if(Userpassword!=Userpassword2) {
                alert('confirmación de la Contraseña no coincide con la Contraseña !');return;
            }
        }
        
        if(yes > 0) {

            $("#btn-user").attr("disabled", true);

            $.ajax({
                data:{"action":action,"Userid":Userid,"Username":Username,"Useruser":Useruser,"Userprofile":Userprofile,"Userpassword":Userpassword,"Userstatus":Userstatus},
                url: "/listUser/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#mensSuccess').text('El usuario con el nombre '+Username.toUpperCase()+' se '+content+' correctamente...');
                    $('#contentSuccess').show();
                    getClearUsers();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#mensDanger').text('El usuario con el nombre '+Username.toUpperCase()+' no se ha podido '+content+', intentelo nuevamente...');
                    $('#contentDanger').show();
                }
            });
        }  
    });*/
    /*Editar perfil usuario*/
    $('#nombreapel').on('keyup', function(e) {
        $("#nombrelabel").html($('#nombreapel').val());
    });
    $("#frm_profile_users").on('submit', function(e) {
        e.preventDefault();
        if ($('#clavesuaurio1').val() != '') {
            if ($('#clavesuaurio1').val() != $('#clavesuaurio2').val()) {
                alertify.error('<b>Mensaje:</b> Las claves nuevas son diferentes.!');
                $('#clavesuaurio2').focus();
                return false;
            }
        }
        alertify.confirm("¿Está seguro de actualizar tu información personal.?", function(e) {
            if (e) {
                $.ajax({
                    type:'post',
                    url:'/save_profile_user',
                    data:$('#frm_profile_users').serialize(),
                    success:function(rpta) {
                        if (rpta.status=='error') {
                            alertify.error('<b>Mensaje:</b> Clave usuario es incorreccta.');
                            $('#clavesuaurio').val('');
                            $('#clavesuaurio').focus();
                        }else{
                            alertify.success('<b>Mensaje:</b> Datos actualizados correctamente.');
                        }
                    }
                });
            } else {
                alertify.log("<b>Mensaje: </b>Proceso no completado..!'");
            }
        });
        // body...
    });
});

function getClearUsers() {
    $("#Userid").val('');
    $("#Username").val('');
    $("#Useruser").val('');
    $("#Userstatus").val(1);
    $("#Userprofile").val(1);
    $("#Userpassword").val('');
    $("#Userpassword2").val('');
    $('#UserpasswordAct').val('');
    $('.jquery-modal').fadeOut(500);
    $("#btn-user").attr("disabled", false);
    $("body").removeAttr("style");
    $('#btn-user').val('Registrar');
    $('#btn-user').html('<li class="fa fa-save"></li> Registrar us.');
    $('#tittle').html('REGISTRO');
    /*var oTable = $('.listUserRefres').dataTable();
    oTable.fnDraw(false);*/
}