var id=0;
$(document).on('submit', '#fcls', function (e) {
	if(id===0){
		$('#fcls').attr("action","sclients");
		$('#fcls').attr("method","post");
	}else{
		$('#fcls').attr("action","uclients/"+id);
		$('#fcls').attr("method","post");
	}
});

function bcls(i){
	$.get("fclients/"+i,function(r){
		$("#no").val(r.name);
		$("#ty").val(r.type);
		$("#do").val(r.document);
		$("#ad").val(r.address);
		$("#ph").val(r.phone);
		$("#em").val(r.email);
		$("#co").val(r.contact);
		$("#bi").val(r.birthdate);
		$("#an").val(r.anniversary);
		$("#es").val(r.status);
		$("#ob").val(r.observations);		
		id=r.id;
	});
}
function ccls(){
	$("#no").val("");
	$("#ty").val(1);
	$("#do").val("");
	$("#ad").val("");
	$("#ph").val("");
	$("#em").val("");
	$("#co").val("");
	$("#bi").val("");
	$("#an").val("");
	$("#es").val(1);
	$("#ob").val("");			
	id=0;
}