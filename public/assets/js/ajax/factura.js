var ls=[];
var gra=0.00;
var iva=0.00;
var tot=0.00;
var des=0.00;

function bsp(){
	var a=$("#cop").val();
	$("#data").load("bsproducto/"+a);
}

function addproducto(pro){
	var ob=JSON.parse(pro);
	ls.push(ob);
	$.notify("+1 | "+ob.description,{
	    className:"success",
	    globalPosition: "top center"
	});
	$("#tbproducto tr").slice(1).remove();
	for (var i = ls.length - 1; i >= 0; i--) {
		var imp=ls[i].PVP;
		imp = imp.replace(",","");
		var igv=eval(imp+"*0.18");
		var sub=eval(imp+"*0.82");
		$("#tbproducto").append("<tr><td>"+ls[i].description+"</td><td>Incluido</td><td>"+ls[i].measurement_units.description+"</td><td>"+imp
			+"</td><td>1</td><td>"+sub+"</td><td>"+igv+"</td><td>"+imp+"</td></tr>");
	}
	ctotal();
}

function ctotal(){
	gra=0.00;
	iva=0.00;
	tot=0.00;
	des=0.00;
	var s=$("#descto").val()
	if(s!=""){
		for (var i = ls.length - 1; i >= 0; i--) {
			var imp=ls[i].PVP;
			imp = imp.replace(",","");
			var igv=eval(imp+"*0.18");
			var sub=eval(imp+"*0.82");
			gra+=eval(imp);
			iva+=igv;
		}
		s=eval(s);
		$("#monto").val(gra);
		$("#igv").val(iva);
		var d=gra*(s/100);
		$("#dsct").val(d);
		tot=gra-d;
		$("#total").val(tot);
	}else{
		$("#descto").val("0.00")
		ctotal();
	}

}
$(document).on('click', '#bform', function (e) {
	var a=$("#no").val();
	if(a!=""&&ls.length>0) {
		alertify.confirm("Enviar Factura Electronica","¿Esta Seguro Con Grabar el Documento?",
		  function() {
		  	$("#lista").val(JSON.stringify(ls));
		    $.post("sfactura",$("#fform").serialize(),function(r){
		    	//location.href="sfactura";
		    });
		  },
		  function() {
		    
		  }
		);

	}else{
		$.notify("Faltan Datos",{
	    className:"error",
	    globalPosition: "top center"
	  });
	}
});

$(document).on('click', '#check', function (e) {
	if( $('#ck').is(':checked') ) {
	    $("#cor").prop("hidden",true);
	    $('#ck').prop("checked", true);
	}else{
		$("#cor").prop("hidden",false);
		$('#ck').prop("checked", false);
	}
});
