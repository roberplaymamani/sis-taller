var pi=0;
function com() {
    if(pi===0){
        $('#fpro').attr("action","sproduct");
    }else{
        $('#fpro').attr("action","uproduct/"+pi);
    }
    var a=$("#co").val();
    var b=$("#de").val();
    var c=$("#ab").val();
    if(a!==""&&b!==""&&c!==""){
        $('#fpro').submit();
    }else{
        alert("Los Campos en Rojo son Obligatorios");
    }
}

$(document).on('change', '#li', function (e) {
	subl();
});

function cpro(){    
    $("#co").val("");
    $("#de").val("");
    $("#ab").val("");
    $("#ga").val(0);
    $("#es").val(1);
    $("#li").val(1);
    $("#um").val(2);
    $("#ma").val(1);
    $("#pa").val(1);
    $("#pr").val(1);
    $("#Productsid_coins").val(1);
    $("#Productscost").val(0);
    $("#Productsgain").val(0);
    $("#Productsgainporc").val(0);
    $("#Productstax").val(0);
    $("#Productstaxporc").val(0);
    $("#ProductsPVP").val(0);
    $("#Productsprice_purchasec").val(0);
    $("#Productsdate_purchase").val(0);
    $("#fo").attr("src","http://placehold.it/100x100/E5E5E5&amp;text=Image");
    pi=0;
    subl();
    coins('textsymbol','Productsid_coins');
    $('#mytab a[href="#lista"]').tab('show');
}

function bpro(i){
/*    $("#lista").removeClass("in active"); 
    $("#registra").addClass("in active");*/
    $('#mytab a[href="#registra"]').tab('show');
    $.get("fproduct/"+i,function(r){
        $("#co").val(r.id_products);
        $("#de").val(r.description);
        $("#ab").val(r.abreviation);
        $("#ga").val(r.warranty);
        $("#es").val(r.status);
        $("#li").val(r.id_lines);
        $("#um").val(r.id_measureunit);
        $("#ma").val(r.id_marks);
        $("#pa").val(r.id_country);
        $("#pr").val(r.id_provider);
        $("#Productsid_coins").val(r.id_coins);
        $("#Productscost").val(r.cost);
        $("#Productsgain").val(r.gain);
        $("#Productsgainporc").val(r.by_profit);
        $("#Productstax").val(r.tax);
        $("#Productstaxporc").val(r.by_tax);
        $("#ProductsPVP").val(r.PVP);
        $("#Productsprice_purchasec").val(r.price_purchase);
        $("#Productsdate_purchase").val(r.date_purchase);
        $("#fo").attr("src","assets/pro/"+r.imagen);
        pi=r.id;

        $("#sl option").remove();

        function sln(){
            return r.id_sublines;
        }
        var a=$("#li").val();
        $.get("lssublines_product/"+a,function(r){
            for (var i = r.length - 1; i >= 0; i--) {
                $("#sl").append('<option value="'+r[i].id+'">'+r[i].description+'</option>');           
            }
            $("#sl").val(sln());
            coins('textsymbol','Productsid_coins');
        });
        
    });
}
$(document).ready(function(){
	subl();
	coins('textsymbol','Productsid_coins');
});

function subl(){
	$("#sl option").remove();
	var a=$("#li").val();
	$.get("lssublines_product/"+a,function(r){
		for (var i = r.length - 1; i >= 0; i--) {
			$("#sl").append('<option value="'+r[i].id+'">'+r[i].description+'</option>');			
		}
	});
}

function format(input) {
    var num=input.value.replace(/\,/g,'');
    if(!isNaN(num)){
        num=num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        num=num.split('').reverse().join('').replace(/^[\,]/,'');
        input.value=num;
    } else { 
        input.value=input.value.replace(/[^\d\.]*/g,'');
    }
}

function formatMoney(num) {
    if(!isNaN(num)){
        num=num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
        num=num.split('').reverse().join('').replace(/^[\,]/,'');
       return num;
    } else { 
        return num.replace(/[^\d\.]*/g,'');
    }
}

function formatDecimal(num) {
    if(num % 1 == 0) {
        return parseInt(num);
    } else {
        return num.toFixed(1);
    }
}

function prices(val) {

    var tax=$('#Productstax').val();
    var gain=$('#Productsgain').val();
    var price=$('#Productscost').val();
    var taxporc=parseInt($('#Productstaxporc').val());
    var gainporc=parseInt($('#Productsgainporc').val());
    
    
    tax=tax.replace(',','');
    gain=gain.replace(',','');
    price=price.replace(',','');

    switch (val) {

        case 'P':
            $('#Productstax').val('');
            $('#Productsgain').val('');
            $('#Productstaxporc').val('');
            $('#Productsgainporc').val('');
        break;

        case 'G':
            if(price && gain){
                var GP=parseFloat(gain/price*100);
                $('#Productsgainporc').val(formatDecimal(GP));
            } else if(!gain) {
                $('#Productsgain').val('');
                $('#Productsgainporc').val('');
            }
        break;

        case 'GP':
            if(price && gainporc){
                var G=(price*gainporc/100);
                $('#Productsgain').val(formatMoney(G));
            } else if(!gainporc) {
                $('#Productsgain').val('');
                $('#Productsgainporc').val('');
            }
        break;

        case 'I':
            if(price && tax){
                var I=parseFloat(tax/price*100);
                $('#Productstaxporc').val(formatDecimal(I));
            } else if(!tax) {
                $('#Productstax').val('');
                $('#Productstaxporc').val('');
            }
        break;
    
        case 'IP':
            if(price && taxporc){
                var IP=(price*taxporc/100);
                $('#Productstax').val(formatMoney(IP));
            } else if(!taxporc) {
                $('#Productstax').val('');
                $('#Productstaxporc').val('');
            }
        break;
    }

    if(price && gain && tax) {
        var PVP=(parseInt(price)+parseInt(gain)+parseInt(tax));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price && gain) {
        var PVP=(parseInt(price)+parseInt(gain));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price && tax) {
        var PVP=(parseInt(price)+parseInt(tax));
        $('#ProductsPVP').val(formatMoney(PVP));
    } else if(price) {
        $('#ProductsPVP').val(formatMoney(price));
    }else if(!price) {
        $('#ProductsPVP').val('');
    }
}
function coins(coins,type) {
    $('.'+coins).val('');
    $('.'+coins).empty();
    
    $("#"+type+" option:selected").each(function () {
        id=$(this).val();
        $.get("lscoins/"+id, function(data){
            if(data.length > 0) {
                $('.'+coins).text(data[0]['symbol']);
            }   
        });		
    });
}